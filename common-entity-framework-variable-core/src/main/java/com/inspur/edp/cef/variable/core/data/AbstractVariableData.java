/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.variable.core.data;


import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.variable.api.data.IVariableData;

public abstract class AbstractVariableData implements IVariableData {

  //C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
  ///#region IValueObjData Impl
  public final ICefData Copy() {
    throw new RuntimeException("AbstractVariableData未实现IVariableData.Copy()");
  }

  public final ICefData CopySelf() {
    try {
      return (ICefData) super.clone();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  public abstract java.util.List<String> GetPropertyNames();

  public abstract Object GetValue(String propName);

  public abstract void SetValue(String propName, Object value);
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
  ///#endregion
}