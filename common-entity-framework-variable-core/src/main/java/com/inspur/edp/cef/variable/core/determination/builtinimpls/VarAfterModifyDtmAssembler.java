/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.variable.core.determination.builtinimpls;

import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObjContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.core.determination.builtinimpls.ValueObjAfterModifyDtmAssembler;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.variable.api.variable.IVariableContext;
import com.inspur.edp.cef.variable.core.determination.VarDeterminationContext;

public class VarAfterModifyDtmAssembler extends ValueObjAfterModifyDtmAssembler {

  @Override
  public ICefDeterminationContext getDeterminationContext(ICefValueObjContext iCefValueObjContext) {
    return new VarDeterminationContext(iCefValueObjContext);
  }

  @Override
  public IChangeDetail getChangeset(ICefDataTypeContext iCefDataTypeContext) {
    return ((IVariableContext)iCefDataTypeContext).getInnerChange();
  }
}
