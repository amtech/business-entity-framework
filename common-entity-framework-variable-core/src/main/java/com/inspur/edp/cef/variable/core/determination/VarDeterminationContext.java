/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.variable.core.determination;

import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObjContext;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.spi.determination.AbstractDeterminationAction;
import com.inspur.edp.cef.variable.api.data.IVariableData;
import com.inspur.edp.cef.variable.api.determination.IVarDeterminationContext;

public class VarDeterminationContext implements IVarDeterminationContext
{
	private IVariableData varData;

	public VarDeterminationContext(ICefValueObjContext nodeContext)
	{
		this.varData = (IVariableData)nodeContext.getData();
	}
//	private ICefData ICefDeterminationContext.data => data;

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
		///#region i18n
	public final String GetEntityI18nName()
	{
		throw new RuntimeException("变量无国际化相关。");
	}

	public final String GetPropertyI18nName(String labelID)
	{
		throw new RuntimeException("变量无国际化相关。");
	}

	public final String GetRefPropertyI18nName(String labelID, String refLabelID)
	{
		throw new RuntimeException("变量无国际化相关。");
	}

	public final String GetEnumValueI18nDisplayName(String labelID, String enumKey)
	{
		throw new RuntimeException("变量无国际化相关。");
	}

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
		///#endregion

	public IVariableData getVarData(){return  varData;}

	public IVariableData getData() {
		return varData;
	}

	public String getEntityI18nName() {
		return null;
	}

	public String getPropertyI18nName(String s) {
		return null;
	}

	public String getRefPropertyI18nName(String s, String s1) {
		return null;
	}

	public String getEnumValueI18nDisplayName(String s, String s1) {
		return null;
	}
}