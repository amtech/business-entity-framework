/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.variable.core.data;


public final class DataUtil
{

    public static String getSesssionValue(String key)
    {
        throw new UnsupportedOperationException();
    }

    public static void setSessionValue(String key,String value)
    {
        throw new UnsupportedOperationException();
    }
//	public static String GetSessionValue(String key) => CafContext.Current.Session[key];
//
////C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
//	public static void SetSessionValue(String key, String value) => CafContext.Current.Session[key] = value;
////C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
//		///#endregion
}