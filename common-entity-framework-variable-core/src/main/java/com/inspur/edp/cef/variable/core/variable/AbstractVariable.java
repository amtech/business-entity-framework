/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.variable.core.variable;

import com.inspur.edp.cef.api.changeListener.EntityDataChangeListener;
import com.inspur.edp.cef.core.datatype.CefValueObjCacheInfo;
import com.inspur.edp.cef.core.datatype.CefValueObject;
import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import com.inspur.edp.cef.spi.determination.IValueObjRTDtmAssembler;
import com.inspur.edp.cef.variable.api.variable.IVariable;
import com.inspur.edp.cef.variable.api.variable.IVariableContext;
import com.inspur.edp.cef.variable.core.determination.builtinimpls.VarAfterModifyDtmAssembler;
import com.inspur.edp.cef.variable.core.variable.builtinimpls.VariableEntityCacheInfo;

public abstract class AbstractVariable extends CefValueObject implements IVariable {

    protected AbstractVariable(IVariableContext context) {
        super();
        super.setContext(context);
    }

    public IVariableContext getVariableContext() {
        return (IVariableContext) super.getContext();
    }

    @Override
    public void modify(ValueObjModifyChangeDetail change) {
        if (getVariableContext().getData() == null) {
            throw new UnsupportedOperationException();
        }

        IAccessor data = (IAccessor) getContext().getData();
        data.acceptChange(change);

        EntityDataChangeListener listener = EntityDataChangeListener.createInstance();
        listener.registListener(data);
        try {
            afterModifyDeterminate(change);
            ((VariableContext) getVariableContext()).setInnerChange(listener.getChange());
        } finally {
            listener.unregistListener(data);
        }
    }

    @Override
    protected final CefValueObjCacheInfo getValueObjCacheInfo() {
        return getVariableCacheInfo();
    }

    protected VariableEntityCacheInfo getVariableCacheInfo() {
        return null;
    }

    @Override
    protected IValueObjRTDtmAssembler createAfterModifyDtmAssembler() {
        return new VarAfterModifyDtmAssembler();
    }
}
