/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.spi.event.retrievedefault;

import com.inspur.edp.bef.api.be.IBEManager;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.determination.AbstractDeterminationAction;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BefRetrieveDefaultBroker {
    private static List<IBefRetrieveDefaultListener> eventLs = new ArrayList<IBefRetrieveDefaultListener>() {{
        //add(getBefQueryEventListener("com.inspur.edp.bef.debugtool.listener.impl.retrievedefault.BefRetrieveDefaultListener"));
    }};

    private static IBefRetrieveDefaultListener getBefQueryEventListener(String className){
        try {
            Class cls=Class.forName(className);
            IBefRetrieveDefaultListener listener= (IBefRetrieveDefaultListener) cls.newInstance();
            return listener;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("新增listener实例初始化失败",e);
        }

    }

    public static  final void fireBeforeRetrieveDefaultManagerAction(IBEManagerContext context,boolean useId,String dataId,
        Map<String,Object> defaultValues){
        for (IBefRetrieveDefaultListener listener:eventLs){
            listener.beforeRetrieveDefaultManagerAction(context,useId,dataId,defaultValues);
        }
    }
    public static  final void fireAfterRetrieveDefaultManagerAction( IBEManagerContext context,IEntityData data){
        for (IBefRetrieveDefaultListener listener:eventLs){
            listener.afterRetrieveDefaultManagerAction(context,data);
        }
    }

    public static  final void fireBeforeDealDefaultValue( IBusinessEntity be, Map<String, Object> defaultValues,IEntityData data){
        for (IBefRetrieveDefaultListener listener:eventLs){
            listener.beforeDealDefaultValue(be,defaultValues,data);
        }
    }

    public static  final void fireAfterDealDefaultValue(IBusinessEntity be, Map<String, Object> defaultValues,IEntityData data){
        for (IBefRetrieveDefaultListener listener:eventLs){
            listener.afterDealDefaultValue(be,defaultValues,data);
        }
    }


    public static  final void fireBeforeRetrieveDefaultDetermination( AbstractDeterminationAction action){
        for (IBefRetrieveDefaultListener listener:eventLs){
            listener.beforeRetrieveDefaultDetermination(action);
        }
    }

    public static  final void fireAfterRetrieveDefaultDetermination( IEntityData data){
        for (IBefRetrieveDefaultListener listener:eventLs){
            listener.afterRetrieveDefaultDetermination(data);
        }
    }
    public static final void fireExceptionStop(Exception e){
        for (IBefRetrieveDefaultListener listener : eventLs) {
            listener.exceptionStop(e);
        }
    }
}
