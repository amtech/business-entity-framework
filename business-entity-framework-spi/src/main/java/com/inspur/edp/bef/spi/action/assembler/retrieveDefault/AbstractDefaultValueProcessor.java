/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.spi.action.assembler.retrieveDefault;

import com.inspur.edp.bef.api.action.assembler.IDefaultValueProcessor;
import com.inspur.edp.bef.api.action.assembler.INodeDefaultValueProcessor;
import com.inspur.edp.bef.entity.entityData.defaultValue.IDefaultValue;
import com.inspur.edp.cef.entity.entity.IEntityData;
import java.util.Map;

public abstract class AbstractDefaultValueProcessor implements IDefaultValueProcessor {
  // <nodeCode, <labelId, value>>
  protected final java.util.Map<String, java.util.Map<String, IDefaultValue>> values =
      new java.util.HashMap<String, java.util.Map<String, IDefaultValue>>();

  public java.util.Map<String, java.util.Map<String, IDefaultValue>> getDefaultValues() {
    return values;
  }

  public abstract INodeDefaultValueProcessor getNodeDefaultValueProcessor(String nodeCode);
  // {
  //    throw new NotImplementedException();
  // }
  public void merge(IDefaultValueProcessor source) {
    merge((AbstractDefaultValueProcessor)source);
  }
  /** 将target中包含但当前类不包含的默认值合并到当前类的默认值中 */
  public void merge(AbstractDefaultValueProcessor source) {
    if (source == null || source.values.isEmpty()) {
      return;
    }
for (Map.Entry<String, java.util.Map<String, IDefaultValue>> src : source.values.entrySet()) {
      if (src.getValue() == null || src.getValue().isEmpty()) {
        continue;
      }
      java.util.Map<String, IDefaultValue> nodeDict = values.get(src.getKey());
      if (nodeDict == null) {
        values.put(src.getKey(), (nodeDict = new java.util.HashMap<String, IDefaultValue>()));
      }
for (Map.Entry<String, IDefaultValue> srcNodeField : src.getValue().entrySet()) {
        if (!nodeDict.containsKey(srcNodeField.getKey())) {
          nodeDict.put(srcNodeField.getKey(), srcNodeField.getValue());
        }
      }
    }
  }

  /** 执行赋默认值 */
  public void setValue(String nodeCode, IEntityData entityData) {
    if (getDefaultValues() == null) {
      return;
    }
    java.util.Map<String, IDefaultValue> nodeDefaultValues = getDefaultValues().get(nodeCode);
    if (nodeDefaultValues == null || nodeDefaultValues.isEmpty()) {
      return;
    }
    INodeDefaultValueProcessor processor = getNodeDefaultValueProcessor(nodeCode);
    if (processor == null) {
      return;
    }
    processor.setDefaultValues(nodeDefaultValues);
    processor.setValue(entityData);
  }

//  private void merge(IDefaultValueProcessor source) {
//    merge((AbstractDefaultValueProcessor) source);
//  }
}
