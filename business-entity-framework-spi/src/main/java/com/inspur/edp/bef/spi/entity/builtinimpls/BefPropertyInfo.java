/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.spi.entity.builtinimpls;

import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.entity.entity.FieldType;
import com.inspur.edp.cef.spi.entity.PropertyDefaultValue;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.BasePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.ObjectType;

public class BefPropertyInfo extends DataTypePropertyInfo {

    private boolean defaultNull;

    public BefPropertyInfo(
            String propertyName,
            String displayValueKey,
            boolean required,
            boolean enableRtrim,
            int length,
            int precision,
            FieldType fieldType,
            ObjectType objectType,
            BasePropertyInfo objectInfo,
            PropertyDefaultValue defaultValue,
            boolean defaultNull) {
        super(propertyName, displayValueKey, required, enableRtrim, length, precision, fieldType, objectType, objectInfo, defaultValue);

        this.defaultNull = defaultNull;
    }

    public BefPropertyInfo(
            String propertyName,
            String displayValueKey,
            boolean required,
            boolean enableRtrim,
            int length,
            int precision,
            FieldType fieldType,
            ObjectType objectType,
            BasePropertyInfo objectInfo,
            PropertyDefaultValue defaultValue,
            boolean isMulti,
            boolean defaultNull) {
        super(propertyName, displayValueKey, required, enableRtrim, length, precision, fieldType, objectType, objectInfo, defaultValue, isMulti);

        this.defaultNull = defaultNull;
    }

    public BefPropertyInfo(
        String propertyName,
        String displayValueKey,
        boolean required,
        boolean enableRtrim,
        int length,
        int precision,
        FieldType fieldType,
        ObjectType objectType,
        BasePropertyInfo objectInfo,
        PropertyDefaultValue defaultValue,
        boolean isMulti,
        boolean defaultNull, GspDbDataType dbDataType,String dbColumnName) {
        super(propertyName, displayValueKey, required, enableRtrim, length, precision, fieldType, objectType, objectInfo, defaultValue, isMulti);

        this.defaultNull = defaultNull;
        this.setDbDataType(dbDataType);
        this.setDbColumnName(dbColumnName);
    }

    public static BefPropertyInfo createBySimple(String propertyName,String displayValueKey,int length,int precision,FieldType fieldType)
    {
        return new BefPropertyInfo(propertyName,displayValueKey,false,true,length,precision,fieldType,ObjectType.Normal,null,null,false, false);
    }

//    public static BefPropertyInfo createSimpleAssociation(String propertyName,String displayValueKey,)

    public boolean isDefaultNull() {
        return defaultNull;
    }
}
