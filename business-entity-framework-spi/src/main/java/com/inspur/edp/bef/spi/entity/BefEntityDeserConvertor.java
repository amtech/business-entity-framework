/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.spi.entity;

import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntitySerializerItem;
import java.util.ArrayList;

/**
 * BE序列化类
 */
public class BefEntityDeserConvertor extends AbstractBizEntityDeSerializer {
    private final String type;
    private final CefEntityResInfoImpl info;

    public BefEntityDeserConvertor(String objectCode, boolean isRoot, String beType, CefEntityResInfoImpl resourceInfo) {
        super(objectCode, isRoot, beType, getTypes(resourceInfo));
        type = beType;
        this.info = resourceInfo;
    }

    @Override
    protected AbstractBizEntityDeSerializer getChildConvertor(String childCode) {
        if (info.getChildEntityResInfos() != null && BeMapIgnoreKeysUtil.containsIgnoreKey(info.getChildEntityResInfos().keySet(), childCode)) {
            return new BefEntityDeserConvertor(BeMapIgnoreKeysUtil.getRelKey(info.getChildEntityResInfos().keySet(), childCode), false, type, (CefEntityResInfoImpl) BeMapIgnoreKeysUtil.getValueByIgnoreKey(info.getChildEntityResInfos(), childCode));
        }
        throw new RuntimeException("无法找到" + childCode + "节点");
    }

    private static ArrayList<AbstractEntitySerializerItem> getTypes(CefEntityResInfoImpl resourceInfo) {
        ArrayList<AbstractEntitySerializerItem> list = new ArrayList<>();
        list.add(new com.inspur.edp.cef.spi.jsonser.builtinimpls.CefEntityDataSerializerItem(resourceInfo));
        return list;
    }
}
