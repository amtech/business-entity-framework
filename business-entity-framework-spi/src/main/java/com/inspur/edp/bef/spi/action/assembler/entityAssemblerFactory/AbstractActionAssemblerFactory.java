/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.spi.action.assembler.entityAssemblerFactory;

import com.inspur.edp.bef.api.action.assembler.IBEActionAssembler;
import com.inspur.edp.bef.api.action.assembler.IDefaultValueProcessor;
import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.spi.action.assembler.BEActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.delete.DeleteActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.delete.DeleteChildActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.modify.ModifyActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.retrieve.AddLockActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.retrieve.RetrieveActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.retrieve.RetrieveWithScopeActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.retrieveDefault.RetrieveDefaultActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.retrieveDefault.RetrieveDefaultActionChildAssembler;
import com.inspur.edp.bef.spi.action.assembler.save.SaveActionAssembler;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

/**
 自定义动作组装器工厂的默认实现，继承自IAbstractActionAssemblerFactory接口
 
*/
public class AbstractActionAssemblerFactory implements IDefaultEntityActionAssFactory
{
	/** 
	 实例化自定义动作的默认组装器的虚方法
	 
	 @param beContext 实体上下文
	 @return 默认的实体动作组装器
	*/
	public IBEActionAssembler getDefaultAssembler(IBENodeEntityContext beContext)
	{
		return new BEActionAssembler(beContext);
	}

	/** 
	 实例化检索操作的默认组装器的虚方法
	 
	 @param beContext 实体上下文
	 @param para 检索参数
	 @return 检索操作的默认组装器
	*/
	public RetrieveActionAssembler getRetrieveActionAssembler(IBEContext beContext, RetrieveParam para)
	{
		return new RetrieveActionAssembler(beContext, para);
	}

	/** 
	 实例化批量检索操作的默认组装器的虚方法
	 
	 @param beContext 实体上下文
	 @param para 检索参数
	 @return 批量检索操作的默认组装器
	*/
	public RetrieveWithScopeActionAssembler getRetrieveWithScopeActionAssembler(IBEContext beContext, RetrieveParam para)
	{
		return new RetrieveWithScopeActionAssembler(beContext, para);
	}

	/** 
	 实例化加锁操作的默认组装器的虚方法
	 
	 @param beContext 实体上下文
	 @return 加锁操作的默认组装器
	*/
	public AddLockActionAssembler getAddLockActionAssembler(IBEContext beContext)
	{
		return new AddLockActionAssembler(beContext);
	}

	/** 
	 实例化修改操作的默认组装器的虚方法
	 
	 @param beContext 实体上下文
	 @param changeDetail 实体变更集
	 @return 修改操作的默认组装器
	*/
	public ModifyActionAssembler getModifyActionAssembler(IBEContext beContext, IChangeDetail changeDetail)
	{
		return new ModifyActionAssembler(beContext, changeDetail);
	}

	/** 
	 实例化保存操作的默认组装器的虚方法
	 
	 @param beContext 实体上下文
	 @return 保存操作的默认组装器
	*/
	public SaveActionAssembler getSaveActionAssembler(IBEContext beContext)
	{
		return new SaveActionAssembler(beContext);
	}

	/** 
	 实例化新增操作的默认组装器的虚方法
	 
	 @param beContext 实体上下文
	 @return 新增操作的默认组装器
	*/
	public RetrieveDefaultActionAssembler getRetrieveDefaultActionAssembler(IBEContext beContext)
	{
		return new RetrieveDefaultActionAssembler(beContext);
	}

	/** 
	 实例化新增从(从)表数据操作的默认组装器的虚方法
	 
	 @param beContext 实体上下文
	 @param nodeCodes 要新增数据的从(从)表的实体编号
	 @param hierachyIdList 新增从(从)表数据的所属实体数据的唯一标识
	 @return 新增从(从)表数据操作的默认组装器
	*/
	public RetrieveDefaultActionChildAssembler getRetrieveDefaultChildActionAssembler(IBEContext beContext, java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList)
	{
		return new RetrieveDefaultActionChildAssembler(beContext, nodeCodes, hierachyIdList);
	}

	/** 
	 实例化删除操作的默认组装器的虚方法
	 
	 @param beContext 实体上下文
	 @return 删除操作的默认组装器
	*/
	public DeleteActionAssembler getDeleteActionAssembler(IBEContext beContext)
	{
		return new DeleteActionAssembler(beContext);
	}

	/** 
	 实例化删除子表数据操作的默认组装器的虚方法
	 
	 @param beContext 实体上下文
	 @param nodeCodes 要删除数据的从(从)表的实体编号
	 @param hierachyIdList 要删除从(从)表数据的所属实体数据的唯一标识
	 @param ids 要删除的从(从)表数据的唯一标识集合
	 @return 删除子表数据操作的默认组装器
	*/
	public DeleteChildActionAssembler getDeleteChildActionAssembler(IBEContext beContext, java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, java.util.List<String> ids)
	{
		return new DeleteChildActionAssembler(beContext);
	}

	/** 
	 实例化默认值处理器的虚方法
	 
	 @return 默认值处理器
	*/
	public IDefaultValueProcessor getDefaultValueProcessor(IBEContext beContext)
	{
		return null;
	}
}
