/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.spi.action.determination;

import com.inspur.edp.bef.api.action.determination.IQueryDeterminationContext;
import com.inspur.edp.cef.entity.condition.EntityFilter;

/** 业务实体上定义联动规则的抽象基类 */
public abstract class AbstractQueryDetermination extends AbstractDetermination {
  /**
   * 新实例初始化AbstractQueryDetermination具有给定的查询联动规则上下文。
   *
   * @param context 查询联动规则上下文
   */
  protected AbstractQueryDetermination(IQueryDeterminationContext context) {
    super(null, null);
    privateContext = context;
  }

  /**
   * 获取查询联动规则上下文，由构造函数传入。
   */
  private IQueryDeterminationContext privateContext;

  public IQueryDeterminationContext getQueryContext() {
    return privateContext;
  }

  /**
   * 查询联动规则过滤条件
   */
  protected EntityFilter getFilter() {
    return getQueryContext().getFilter();
  }
}
