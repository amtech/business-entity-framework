/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.spi.entity;

import com.inspur.edp.bef.api.BefRtBeanUtil;
import com.inspur.edp.bef.api.lcp.ILcpFactory;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntitySerializerItem;
import com.inspur.edp.commonmodel.api.ICMManager;
import com.inspur.edp.commonmodel.spi.AbstractEntityDataDeSerializer;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.List;

public abstract class AbstractBizEntityDeSerializer extends AbstractEntityDataDeSerializer {
  private String beType;
  private ICMManager mgr;

  protected AbstractBizEntityDeSerializer(
      String objectCode, boolean isRoot, String beType, List<AbstractEntitySerializerItem> serializers) {
    super(objectCode, isRoot, serializers);
    this.beType = beType;
  }

  @Override
  protected final ICMManager getCMManager() {
    if(mgr == null) {
      mgr = BefRtBeanUtil.getLcpFactory().createCMManager(beType);
    }
    return mgr;
  }

  public void setManger(ICMManager mgr) {
    this.mgr = mgr;
  }
}
