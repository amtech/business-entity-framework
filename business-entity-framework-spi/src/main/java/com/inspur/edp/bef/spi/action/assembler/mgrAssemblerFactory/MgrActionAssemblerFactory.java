/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.spi.action.assembler.mgrAssemblerFactory;

import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.spi.action.assembler.MgrActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.delete.DeleteChildMgrActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.delete.DeleteMgrActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.delete.MultiDeleteMgrActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.modify.ModifyByFilterMgrActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.modify.ModifyMgrActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.modify.MultiModifyMgrActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.query.QueryMgrActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.retrieve.MultiRetrieveMgrActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.retrieve.RetrieveMgrActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.retrieveDefault.RetrieveDefaultChildMgrActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.retrieveDefault.RetrieveDefaultMgrActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.save.SaveMgrActionAssembler;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.condition.EntityFilter;

/** 管理类动作组装器工厂的默认实现，继承自IMgrActionAssemblerFactory接口 */
public class MgrActionAssemblerFactory implements IDefaultMgrActionAssFactory {
  /**
   * 实例化管理类动作的默认组装器的虚方法
   *
   * @param beMgrContext 实体管理器上下文
   * @return 默认的管理类动作组装器
   */
  public IMgrActionAssembler getMgrActionAssembler(IBEManagerContext beMgrContext) {
    return new MgrActionAssembler(beMgrContext);
  }

  /**
   * 实例化批量检索管理类操作的默认组装器的虚方法
   *
   * @param IBEManagerContext 实体管理器上下文
   * @param para 检索参数
   * @return 批量检索管理类操作的默认组装器
   */
  public MultiRetrieveMgrActionAssembler getMultiRetrieveMgrActionAssembler(
      IBEManagerContext IBEManagerContext, RetrieveParam para) {
    return new MultiRetrieveMgrActionAssembler(IBEManagerContext, para);
  }

  /**
   * 实例化检索管理类操作的默认组装器的虚方法
   *
   * @param IBEManagerContext 实体管理器上下文
   * @param para 检索参数
   * @return 检索管理类操作的默认组装器
   */
  public RetrieveMgrActionAssembler getRetrieveMgrActionAssembler(
      IBEManagerContext IBEManagerContext, RetrieveParam para) {
    return new RetrieveMgrActionAssembler(IBEManagerContext, para);
  }

  /**
   * 实例化新增管理类操作的默认组装器的虚方法
   *
   * @param IBEManagerContext 实体管理器上下文
   * @return 新增管理类操作的默认组装器
   */
  public RetrieveDefaultMgrActionAssembler getRetrieveDefaultMgrActionAssembler(
      IBEManagerContext IBEManagerContext) {
    return new RetrieveDefaultMgrActionAssembler(IBEManagerContext);
  }

  /**
   * 实例化新增从(从)表数据管理类操作的默认组装器的虚方法
   *
   * @param IBEManagerContext 实体管理器上下文
   * @param nodeCodes 要新增数据的从(从)表的实体编号
   * @param hierachyIdList 新增从(从)表数据的所属实体数据的唯一标识
   * @return 新增从(从)表数据管理类操作的默认组装器
   */
  public RetrieveDefaultChildMgrActionAssembler getRetrieveDefaultChildMgrActionAssembler(
      IBEManagerContext IBEManagerContext,
      java.util.List<String> nodeCodes,
      java.util.List<String> hierachyIdList) {
    return new RetrieveDefaultChildMgrActionAssembler(IBEManagerContext, nodeCodes, hierachyIdList);
  }

  /**
   * 实例化保存管理类操作的默认组装器的虚方法
   *
   * @param IBEManagerContext 实体管理器上下文
   * @return 保存管理类操作的默认组装器
   */
  public SaveMgrActionAssembler getSaveMgrActionAssembler(IBEManagerContext IBEManagerContext) {
    return new SaveMgrActionAssembler(IBEManagerContext);
  }

  /**
   * 实例化修改管理类操作的默认组装器的虚方法
   *
   * @param IBEManagerContext 实体管理器上下文
   * @param changeDetail 变更集
   * @return 修改管理类操作的默认组装器
   */
  public ModifyMgrActionAssembler getModifyMgrActionAssembler(
      IBEManagerContext IBEManagerContext, IChangeDetail changeDetail) {
    return new ModifyMgrActionAssembler(IBEManagerContext, changeDetail);
  }

  /**
   * 实例化批量修改管理类操作的默认组装器的虚方法
   *
   * @param IBEManagerContext 实体管理器上下文
   * @param changeList 变更集集合
   * @return 批量修改管理类操作的默认组装器
   */
  public MultiModifyMgrActionAssembler getMultiModifyMgrActionAssembler(
      IBEManagerContext IBEManagerContext, java.util.ArrayList<IChangeDetail> changeList) {
    return new MultiModifyMgrActionAssembler(IBEManagerContext, changeList);
  }

  /**
   * 实例化安过滤条件批量修改管理类操作的默认组装器的虚方法
   *
   * @param IBEManagerContext 实体管理器上下文
   * @param changeDetail 变更集集合
   * @return 批量修改管理类操作的默认组装器
   */
  public ModifyByFilterMgrActionAssembler getModifyByFilterMgrActionAssembler(
      IBEManagerContext IBEManagerContext, EntityFilter filter, ModifyChangeDetail changeDetail) {
    return new ModifyByFilterMgrActionAssembler(IBEManagerContext, filter, changeDetail);
  }

  /**
   * 实例化按过滤条件检索实体数据的查询管理类操作的默认组装器的虚方法
   *
   * @param IBEManagerContext 实体管理器上下文
   * @param entityFilter 过滤条件
   * @return 查询管理类操作的默认组装器
   */
  public QueryMgrActionAssembler getQueryMgrActionAssembler(
      IBEManagerContext IBEManagerContext, EntityFilter entityFilter) {
    return new QueryMgrActionAssembler(IBEManagerContext, entityFilter);
  }

  /**
   * 实例化删除管理类操作的默认组装器的虚方法
   *
   * @param IBEManagerContext 实体管理器上下文
   * @return 删除管理类操作的默认组装器
   */
  public DeleteMgrActionAssembler getDeleteMgrActionAssembler(IBEManagerContext IBEManagerContext) {
    return new DeleteMgrActionAssembler(IBEManagerContext);
  }

  /**
   * 实例化删除从(从)表数据管理类操作的默认组装器的虚方法
   *
   * @param IBEManagerContext 实体管理器上下文
   * @param nodeCodes 要删除数据的从(从)表的实体编号
   * @param hierachyIdList 要删除数据的从(从)表的所属实体数据的唯一标识
   * @param ids 要删除的从(从)表数据的唯一标识集合
   * @return 删除从(从)表数据管理类操作的默认组装器
   */
  public DeleteChildMgrActionAssembler getDeleteChildMgrActionAssembler(
      IBEManagerContext IBEManagerContext,
      java.util.List<String> nodeCodes,
      java.util.List<String> hierachyIdList,
      java.util.List<String> ids) {
    return new DeleteChildMgrActionAssembler(IBEManagerContext, nodeCodes, hierachyIdList, ids);
  }

  /**
   * 实例化批量删除管理类操作的默认组装器的虚方法
   *
   * @param IBEManagerContext 实体管理器上下文
   * @return 批量删除管理类操作的默认组装器
   */
  public MultiDeleteMgrActionAssembler getMultiDeleteMgrActionAssembler(
      IBEManagerContext IBEManagerContext) {
    return new MultiDeleteMgrActionAssembler(IBEManagerContext);
  }
}
