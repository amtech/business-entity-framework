/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.spi.action.determination;

import com.inspur.edp.bef.api.action.determination.IBeforeRetrieveDtmContext;
import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public abstract class AbstractB4RetrieveDetermination extends AbstractDetermination {
    private IBeforeRetrieveDtmContext b4QueryDtmContext;

    /**
     * 新实例初始化AbstractDetermination具有给定的联动规则上下文，以及实体数据变更集。
     *
     * @param context 联动规则上下文
     */
    protected AbstractB4RetrieveDetermination(IBeforeRetrieveDtmContext context) {
        super(context, null);
        b4QueryDtmContext = context;
    }


    protected IBeforeRetrieveDtmContext getB4RetrieveDtmContext() {
        return b4QueryDtmContext;
    }


}
