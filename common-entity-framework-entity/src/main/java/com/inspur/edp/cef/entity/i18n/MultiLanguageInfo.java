/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.entity.i18n;

import java.util.HashMap;
import java.util.Map;

public class MultiLanguageInfo {

  public final static String MULTILANGUAGETOKEN = "_MULTILANGUAGE";
  private String propName;
  /**
   * <Key:语言，Value:值>
   */
  private Map<String, Object> propValueMap;

  public String getPropName() {
    return propName;
  }

  public void setPropName(String propName) {
    this.propName = propName;
  }

  public Map<String, Object> getPropValueMap() {
    if (this.propValueMap == null) {
      this.propValueMap = new HashMap<>();
    }
    return propValueMap;
  }

  public void setPropValueMap(Map<String, Object> propValueMap) {
    this.propValueMap = propValueMap;
  }

  public MultiLanguageInfo clone() {
    MultiLanguageInfo targetInfo = new MultiLanguageInfo();
    targetInfo.setPropName(this.getPropName());
    if (this.getPropValueMap() != null) {
      this.getPropValueMap().entrySet().forEach(entrySet ->
          targetInfo.getPropValueMap().put(entrySet.getKey(), entrySet.getValue()));
    }
    return targetInfo;
  }
}
