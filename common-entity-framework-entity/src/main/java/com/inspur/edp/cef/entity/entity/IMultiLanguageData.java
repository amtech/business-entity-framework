/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.entity.entity;

import com.inspur.edp.cef.entity.i18n.MultiLanguageInfo;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

public interface IMultiLanguageData {

  @Deprecated
  Map<String, MultiLanguageInfo> getMultiLanguageInfos();

  default Object getMultiLang(String propName, String langCode) {
		Objects.requireNonNull(propName, "propName");
		Objects.requireNonNull(langCode, "langCode");

  	if(getMultiLanguageInfos() == null){
  		return null;
		}
  	MultiLanguageInfo info = getMultiLanguageInfos().get(propName.concat(MultiLanguageInfo.MULTILANGUAGETOKEN));
  	if(info == null) {
  		return null;
		}
  	return info.getPropValueMap().get(langCode);
  }

  default void setMultiLang(String propName, String langCode, Object value) {
	 Objects.requireNonNull(propName, "propName");
	 Objects.requireNonNull(langCode, "langCode");

	 if(getMultiLanguageInfos() == null){
		 throw new UnsupportedOperationException();
	 }
	 MultiLanguageInfo info = getMultiLanguageInfos().computeIfAbsent(propName.concat(MultiLanguageInfo.MULTILANGUAGETOKEN), (key) -> new MultiLanguageInfo(){{setPropName(key);}});
	 info.getPropValueMap().put(langCode, value);
 }

  default Iterable<Entry<String, Object>> iterableMultiLang(String propName) {
		Objects.requireNonNull(propName, "propName");
		if(getMultiLanguageInfos() == null){
			return Collections.emptyList();
		}
		MultiLanguageInfo info = getMultiLanguageInfos().get(propName.concat(MultiLanguageInfo.MULTILANGUAGETOKEN));
		if(info == null || info.getPropValueMap().isEmpty()){
			return Collections.emptyList();
		}
		return Collections.unmodifiableMap(info.getPropValueMap()).entrySet();
	}
}
