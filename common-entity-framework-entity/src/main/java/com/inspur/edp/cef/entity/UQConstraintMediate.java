/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.entity;
import java.util.ArrayList;
import java.util.HashMap;

/**
 持久化层用于构造唯一性约束SQL语句的中介类
 
*/
public class UQConstraintMediate
{
	/** 
	 需要排除的修改ID字段集合
	 
	*/
	private java.util.ArrayList<String> privateExceptModifyIds=new ArrayList<>();
	public final java.util.ArrayList<String> getExceptModifyIds()
	{
		return privateExceptModifyIds;
	}

	/** 
	 需要排除的删除ID字段集合
	 
	*/
	private java.util.ArrayList<String> privateExceptDeleteIds=new ArrayList<>();
	public final java.util.ArrayList<String> getExceptDeleteIds()
	{
		return privateExceptDeleteIds;
	}

	/**
	 string dataId,Dictionary string field,object value
	 
	*/
	private java.util.HashMap<String, java.util.HashMap<String, Object>> privateParametersInfo=new HashMap<>();
	public final java.util.HashMap<String, java.util.HashMap<String, Object>> getParametersInfo()
	{
		return privateParametersInfo;
	}

	/** 
	 节点编号
	 
	*/
	private String privateNodeCode;
	public final String getNodeCode()
	{
		return privateNodeCode;
	}
	public final void setNodeCode(String value)
	{
		privateNodeCode = value;
	}

	/** 
	 唯一性约束提示信息
	 
	*/
	private String privateMessage;
	public final String getMessage()
	{
		return privateMessage;
	}
	public final void setMessage(String value)
	{
		privateMessage = value;
	}

	private boolean checkFailed;
	public final boolean getCheckFailed(){return checkFailed;}
	public final void setCheckFailed(boolean value){checkFailed = value;}
}
