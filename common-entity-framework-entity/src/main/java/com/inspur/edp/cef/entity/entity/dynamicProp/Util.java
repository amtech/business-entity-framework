
/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.entity.entity.dynamicProp;


import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;

public final class Util
{
	public static void copyTo(IDynamicPropSet src, IDynamicPropSet target)
	{
		DataValidator.checkForNullReference(src, "src");
		DataValidator.checkForNullReference(target, "target");
		if (src == target)
		{
			throw new RuntimeException("Could not copy same object");
		}

		target.clear();
		for (IDynamicPropPair pair : src)
		{
			target.setValue(pair.getPropName(), pair.getPropValue());
		}
	}
}
