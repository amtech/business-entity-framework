/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.entity.repository;

import java.util.HashMap;
import java.util.Map;

public class DataSaveResult {

  private int affectedRowCount;

  public int getAffectedRowCount() {
    return affectedRowCount;
  }

  public void setAffectedRowCount(int value) {
    affectedRowCount = value;
  }

  //         <nodeCode,  <dataId, result>>
  private Map<String, Map<String, DataSaveResult>> childResult = new HashMap<>();

  public void addChildResult(String code, String id, DataSaveResult child) {
    Map<String, DataSaveResult> childs = childResult.computeIfAbsent(code, (k) -> new HashMap<>());
    childs.put(id, child);
  }

  public Map<String, DataSaveResult> getChildResult(String childCode) {
    return childResult.get(childCode);
  }

  public DataSaveResult getChildResult(String childCode, String dataId) {
    Map<String, DataSaveResult> childRezs = childResult.get(childCode);
    return childRezs == null ? null : childRezs.get(dataId);
  }
}
