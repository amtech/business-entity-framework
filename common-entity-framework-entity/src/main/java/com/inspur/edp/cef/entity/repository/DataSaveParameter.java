/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.entity.repository;

import com.inspur.edp.cef.entity.condition.FilterCondition;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DataSaveParameter {

  private Map<String, NodeSaveParameter> childFilters = new HashMap<>();

  //<nodeCode, >
  public Map<String, NodeSaveParameter> getNodeFilters() {
    return childFilters;
  }

  public NodeSaveParameter getFilter(String nodeCode) {
    return childFilters.get(nodeCode);
  }

  public List<FilterCondition> getFilterCondition(String code, String id) {
    Objects.requireNonNull(code);
    Objects.requireNonNull(id);

    NodeSaveParameter nodePar = getNodeFilters().get(code);
    return nodePar == null ? null : nodePar.getDataFilter(id);
  }

  public void setFilterCondition(String code, String id, List<FilterCondition> condition) {
    NodeSaveParameter nodePar = getNodeFilters()
        .computeIfAbsent(code, (key) -> new NodeSaveParameter());
    nodePar.getDataFilters().compute(id, (key, value) -> {
      if (value == null) {
        return condition;
      } else {
        throw new IllegalArgumentException("重复设置过滤条件");
      }
    });
  }

  public void clear(){
    getNodeFilters().clear();
  }

  static {
    DataSaveParameter rez = new DataSaveParameter();
    rez.childFilters = Collections.unmodifiableMap(rez.childFilters);
    Empty = rez;
  }

  public static DataSaveParameter Empty;
}
