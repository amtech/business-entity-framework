/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.entity.condition;

/** 
 查询条件中排序信息的排序方式
 
*/
public enum SortType
{
	/** 
	 升序
	 
	*/
	Asc(0),

	/** 
	 降序
	 
	*/
	Desc(1);

	private int intValue;
	private static java.util.HashMap<Integer, SortType> mappings;
	private synchronized static java.util.HashMap<Integer, SortType> getMappings()
	{
		if (mappings == null)
		{
			mappings = new java.util.HashMap<Integer, SortType>();
		}
		return mappings;
	}

	private SortType(int value)
	{
		intValue = value;
		SortType.getMappings().put(value, this);
	}

	public int getValue()
	{
		return intValue;
	}

	public static SortType forValue(int value)
	{
		return getMappings().get(value);
	}
}
