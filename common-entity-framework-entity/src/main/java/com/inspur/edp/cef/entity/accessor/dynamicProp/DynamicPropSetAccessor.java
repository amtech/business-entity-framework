/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.entity.accessor.dynamicProp;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.entity.accessor.base.AccessorBase;
import com.inspur.edp.cef.entity.accessor.dataType.ValueObjAccessor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.entity.dynamicProp.DynamicPropSetJsonDeserializer;
import com.inspur.edp.cef.entity.entity.dynamicProp.DynamicPropSetJsonSerializer;
import com.inspur.edp.cef.entity.entity.dynamicProp.IDynamicPropPair;
import com.inspur.edp.cef.entity.entity.dynamicProp.IDynamicPropSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

@JsonDeserialize( using = DynamicPropSetJsonDeserializer.class)
@JsonSerialize(using = DynamicPropSetJsonSerializer.class)
public class DynamicPropSetAccessor extends ValueObjAccessor implements IDynamicPropSet {
  public DynamicPropSetAccessor() {}

  public DynamicPropSetAccessor(IDynamicPropSet inner) {
    super(inner);
  }

  public boolean getIsReadonly() {
    return false;
  }

  // C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing via the 'new'
  // keyword:
  // ORIGINAL LINE: private new IDynamicPropSet InnerData =>(IDynamicPropSet)super.InnerData;
  public IDynamicPropSet getInnerData() {
    return (IDynamicPropSet) super.getInnerData();
  }

  public Object getValue(String propName) {
    if (getInnerData() == null) {
      throw new RuntimeException();
    }
    return getInnerData().getValue(propName);
  }

  public void setValue(String propName, Object value) {
    Objects.requireNonNull(propName);

    tryCopy();
    Object orgValue =
        getInnerData().contains(propName) ? getInnerData().getValue(propName) : new Object();
    raisePropertyChanging(propName, value, orgValue);
    getInnerData().setValue(propName, value);
  }

  @Override
  protected AccessorBase createNewObject() {
    return new DynamicPropSetAccessor();
  }

  public final void clear() {
    tryCopy();
    getInnerData().clear();
  }

  @Override
  public boolean contains(String propName) {
    return getInnerData().contains(propName);
  }

  protected void acceptChangeCore(IChangeDetail change) {
    DataValidator.checkForNullReference(change, "change");
    if (!(change instanceof ValueObjModifyChangeDetail)) {
      throw new RuntimeException("not supported changeType: " + change.getChangeType());
    }
    ValueObjModifyChangeDetail modifyChange = (ValueObjModifyChangeDetail) change;
    if (modifyChange.getPropertyChanges() == null) {
      return;
    }

    for (Map.Entry<String, Object> pair : modifyChange.getPropertyChanges().entrySet()) {
      setValue(pair.getKey(), pair.getValue());
    }
  }
//
//  public final java.util.Iterator<IDynamicPropPair> GetEnumerator() {
//    if (getInnerData() == null) {
//      throw new RuntimeException();
//    }
//    return getInnerData().iterator();
//  }

  @Override
  public Iterator<IDynamicPropPair> iterator() {
    return getInnerData().iterator();
  }
}
