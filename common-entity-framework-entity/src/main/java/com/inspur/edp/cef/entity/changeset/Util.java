/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.entity.changeset;

import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import com.inspur.edp.cef.entity.entity.IKey;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class Util
{
	public static boolean containsAny(IChangeDetail change, List<String> elements) {
		return containsAny(change, elements.toArray(new String[0]));
	}

	public static boolean containsAny(IChangeDetail change, String[] elements) {
		DataValidator.checkForNullReference(change, "change");
		DataValidator.checkForNullReference(elements, "elements");

		AbstractModifyChangeDetail modifyChange = (AbstractModifyChangeDetail)((change instanceof AbstractModifyChangeDetail) ? change : null);
		if (modifyChange != null)
		{
			for (String ele:elements)
			{
				if (modifyChange.getPropertyChanges().containsKey(ele))
					return true;
			}
			return false;
		}
		return true;
	}

	public static boolean containsChildAny(IChangeDetail change, String nodeCode, String... elements) {
		if(change instanceof  ModifyChangeDetail) {
			return containsChildAnyForModifyChange((ModifyChangeDetail)change,nodeCode,elements);
		} else if(change instanceof AddChangeDetail) {
			return containsChildAnyForAddChange((AddChangeDetail) change,nodeCode,elements);
		}
		return false;
	}

	private  static  boolean containsChildAnyForModifyChange(ModifyChangeDetail modifyChange, String nodeCode, String... elements) {
		if(modifyChange==null)
			return false;

		Map<String, java.util.Map<String, IChangeDetail>> childChanges=modifyChange.getChildChanges();
			if(childChanges== null) {
				return false;
			}

		java.util.Map<String, IChangeDetail> nodeChildChanges  = childChanges.get(nodeCode);
			if(nodeChildChanges==null)
				return false;
			for (Map.Entry<String,IChangeDetail> entry: nodeChildChanges.entrySet())
			{
				if(containsAny(entry.getValue(),elements))
					return true;
			}
			return false;
	}

	private  static  boolean containsChildAnyForAddChange(AddChangeDetail addChange, String nodeCode, String... elements)
	{
		if(addChange==null)
			return false;
		if(addChange.getEntityData().getChilds()==null||addChange.getEntityData().getChilds().containsKey(nodeCode)==false)
			return false;
		IEntityDataCollection nodeChildChanges =addChange.getEntityData().getChilds().get(nodeCode);
		if(nodeChildChanges!=null&&nodeChildChanges.isEmpty()==false)
			return true;
		return false;
	}

	public static boolean containsChild(IChangeDetail change, String nodeCode)
	{

		ModifyChangeDetail modifyChange=(ModifyChangeDetail)((change instanceof ModifyChangeDetail) ? change : null);
		if (modifyChange != null)
		{
			java.util.Map<String, java.util.Map<String, IChangeDetail>> childChanges=modifyChange.getChildChanges();
			if (childChanges== null) {
				return false;
			}
			return childChanges.containsKey(nodeCode)&&childChanges.get(nodeCode)!=null;
		}

		AddChangeDetail addChange = (AddChangeDetail)((change instanceof AddChangeDetail) ? change : null);
		if (addChange != null)
		{
			java.util.HashMap<String, IEntityDataCollection> childDatas=addChange.getEntityData().getChilds();
			if(childDatas==null)
				return false;
			if(childDatas.containsKey(nodeCode)==false)
				return false;
			IEntityDataCollection nodeChildDatas =childDatas.get(nodeCode);
			return nodeChildDatas!=null&&nodeChildDatas.isEmpty()==false;
		}
		return true;
	}

	public static String getID(IChangeDetail change)
	{
		DataValidator.checkForNullReference(change, "change");
		IKey keyContainer = (IKey)((change instanceof IKey) ? change : null);
		if (keyContainer == null)
		{
			throw new IllegalArgumentException();
		}
		return keyContainer.getID();
	}

	public static Iterable<IChangeDetail> getChildChanges(IChangeDetail change, String childCode)
	{
		DataValidator.checkForNullReference(change, "change");
		DataValidator.checkForEmptyString(childCode, "childCode");

		switch (change.getChangeType())
		{
			case Deleted:
				return Collections.emptyList();
			case Modify:
				if (((ModifyChangeDetail)change).getChildChanges() == null) {
					return Collections.emptyList();
				}
				Map<String, IChangeDetail> childChange = ((ModifyChangeDetail)change).getChildChanges().get(childCode);
				if(childChange == null){
					return Collections.emptyList();
				}
				return childChange.values();
			case Added:
				return new AddedChildChangeCollection((AddChangeDetail)change, childCode);
			default:
				throw new UnsupportedOperationException("change.ChangeType");
		}
	}

	public static IChangeDetail getChildChange(IChangeDetail change, String childCode, String childId)
	{
		DataValidator.checkForNullReference(change, "change");
		DataValidator.checkForEmptyString(childCode, "childCode");
		DataValidator.checkForEmptyString(childId, "childId");

		switch (change.getChangeType())
		{
			case Deleted:
				return new DeleteChangeDetail(childId);
			case Modify:
				if (((ModifyChangeDetail)change).getChildChanges()== null) {
					return null;
				}
				java.util.Map<String, IChangeDetail> childChange = ((ModifyChangeDetail)change).getChildChanges().get(childCode);
				return childChange != null ? childChange.get(childId) : null ;
			case Added:
				IEntityData childData = ((AddChangeDetail)change).getEntityData().getChilds().get(childCode).tryGet(childId);
				return childData == null ? null : new AddChangeDetail(childData);
			default:
				throw new  UnsupportedOperationException("change.ChangeType"+change.getChangeType().toString());
		}
	}

	public static boolean contains(IChangeDetail change, String propName)
	{
		AbstractModifyChangeDetail modifyChange = (AbstractModifyChangeDetail)((change instanceof AbstractModifyChangeDetail) ? change : null);
		return modifyChange != null && modifyChange.getPropertyChanges().containsKey(propName);
	}

	public static Object getPropChange(IChangeDetail change, String propName)
	{
		AbstractModifyChangeDetail modifyChange = (AbstractModifyChangeDetail)((change instanceof AbstractModifyChangeDetail) ? change : null);
		if (modifyChange == null)
		{
			return null;
		}
		return modifyChange.getItem(propName);
	}

	public static ValueObjModifyChangeDetail getNestedPropChange(IChangeDetail change, String propName)
	{
		AbstractModifyChangeDetail modifyChange = (AbstractModifyChangeDetail)((change instanceof AbstractModifyChangeDetail) ? change : null);
		if (modifyChange != null)
		{
			return modifyChange.getNestedChange(propName);
		}

		AddChangeDetail addChange = (AddChangeDetail)((change instanceof AddChangeDetail) ? change : null);
		if (addChange != null)
		{
			return addChange.getNestedChange(propName);
		}

		return null;
	}

	public static <TKey, T> T tryGetValue(java.util.Map<TKey, T> dic, TKey key)
	{
		return dic.get(key);
	}

	public static final List<ChangeType> ChgTypes_Add = Collections.unmodifiableList(Arrays
			.asList(ChangeType.Added));
	public static final List<ChangeType> ChgTypes_Modify = Collections.unmodifiableList(Arrays
			.asList(ChangeType.Modify));
	public static final List<ChangeType> ChgTypes_Delete = Collections.unmodifiableList(Arrays
			.asList(ChangeType.Deleted));
	public static final List<ChangeType> ChgTypes_AddModify = Collections.unmodifiableList(Arrays
			.asList(ChangeType.Added, ChangeType.Modify));

	public static final List<ChangeType> ChgTypes_ModifyDelete = Collections.unmodifiableList(Arrays
			.asList(ChangeType.Deleted, ChangeType.Modify));

	public static final List<ChangeType> ChgTypes_AddDelete = Collections.unmodifiableList(Arrays
			.asList(ChangeType.Added, ChangeType.Deleted));

	public static final List<ChangeType> ChgTypes_All = Collections.unmodifiableList(Arrays
			.asList(ChangeType.Added, ChangeType.Modify, ChangeType.Deleted));
}
