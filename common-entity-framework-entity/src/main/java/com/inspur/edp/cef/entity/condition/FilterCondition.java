/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.entity.condition;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inspur.edp.cef.entity.condition.advanceconditions.AbsConditionValue;
import lombok.var;

import java.util.List;

/**
 * 检索实体数据时，查询条件中过滤条件描述类
 * <p>关于FilterCondition的使用可以访问<a href="https://open.inspuronline.com/iGIX/#/document/mddoc/igix-2103%2Fdev-guide-beta%2Fspecial-subject%2Fbusiness-object%2FFilterConditions_Example.md">这里</a>查看
 */
public class FilterCondition
{
	/** 
	 新实例初始化AddChangeDetail
	 
	*/
	public FilterCondition()
	{

	}

	/** 
	 新实例初始化AddChangeDetail具有给定的表达式内容
	 
	 @param lBracketCount 左括号
	 @param fieldName 属性名称
	 @param compare 比较符
	 @param value 值
	 @param rBracketCount 右括号
	 @param relation 关系符
	 @param expresstype 表达式值类型
	*/
	public FilterCondition(int lBracketCount, String fieldName, ExpressCompareType compare, String value
			, int rBracketCount, ExpressRelationType relation, ExpressValueType expresstype)
	{
		setlBracketCount(lBracketCount);
		setFilterField(fieldName);
		setCompare(compare);
		setValue(value);
		setRBracketCount(rBracketCount);
		setRelation(relation);
		setExpresstype(expresstype);
	}

	@Deprecated
	public FilterCondition(int lBracketCount, String fieldName, ExpressCompareType compare, List<String> inValues
			, int rBracketCount, ExpressRelationType relation, ExpressValueType expresstype)
	{
		setlBracketCount(lBracketCount);
		setFilterField(fieldName);
		setCompare(compare);
		if(inValues == null){
			setValue(null);
		}
		else {
			setInValues(inValues);
		}
		setRBracketCount(rBracketCount);
		setRelation(relation);
		setExpresstype(expresstype);
	}

	/** 
	 获取或设置进行过滤的属性名称
	 <see cref="string"/>
	 
	*/
	private String filterField;
	public final String getFilterField() {
		return filterField;
	}
	public final void setFilterField(String value)
	{
		filterField = value;
	}

	private String filterNode;
	public String getFilterNode() {
		return filterNode;
	}

	public void setFilterNode(String filterNode) {
		this.filterNode = filterNode;
	}

	//TODO Value能否用Object
	/** 
	 获取或设置过滤条件的值
	 <see cref="string"/>
	 
	*/
	private String value;
	public final String getValue()
	{
		return value;
	}
	public final void setValue(String value)
	{
		this.value = value;
	}

	public List getInValues() {
		return inValues;
	}

	public void setInValues(List inValues) {
		this.inValues = inValues;
	}

	/**
	 * 针对in类型 value范围值集合
	 */
	private List inValues;


	/** 
	 获取或设置过滤条件的左括号
	 <see cref="string"/>
	 
	*/

	public final String getLbracket()
	{
		if (lBracketCount <= 0) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < lBracketCount; i++) {
			sb.append("(");
		}
		return sb.toString();
	}

	public final void setLbracket(String value)
	{
		if (value == null || "".equals(value)) {
			setlBracketCount(0);
			return;
		}
		else if ("null".equals(value.toLowerCase())) {
			setlBracketCount(0);
			return;
		}
		int count = 0;
		for (var item : value.toCharArray()) {
			if (item == '(') {
				count++;
				continue;
			}
			if (item == ' ')
				continue;
			throw new RuntimeException("条件左括号中不能包含："+item);
		}
		setlBracketCount(count);
	}

	private int lBracketCount = 0;

	@JsonIgnore
	public final int getLBracketCount()
	{
		return lBracketCount;
	}
	public final  void setlBracketCount(int count)
	{lBracketCount=count;}

	/** 
	 获取或设置过滤条件的右括号
	 <see cref="string"/>
	 
	*/
	public final String getRbracket()
	{
		if (rBracketCount <= 0)
		{
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < rBracketCount; i++)
		{
			sb.append(")");
		}
		return sb.toString();
	}

	public final void setRbracket(String value){
		if (value == null || "".equals(value)) {
			setRBracketCount(0);
			return;
		}

		else if ("null".equals(value.toLowerCase())) {
			setRBracketCount(0);
			return;
		}
		int count = 0;
		for(var item : value.toCharArray())
		{
			if (item == ')') {
				count++;
				continue;
			}
			if (item == ' ')
				continue;
			throw new RuntimeException("条件右括号中不能包含:"+item);
		}
		setRBracketCount(count);
	}
	private int rBracketCount = 0;

	@JsonIgnore
	public final int getRBracketCount()
	{
		return rBracketCount;
	}
	public final void setRBracketCount(int value)
	{
		if (value < 0)
		{
			throw new RuntimeException("右括号数量不能小于0.");
		}
		rBracketCount = value;
	}
	/** 
	 获取或设置过滤条件的关系符
	 <see cref="ExpressRelationType"/>
	 
	*/
	private ExpressRelationType relation = ExpressRelationType.forValue(0);
	public final ExpressRelationType getRelation()
	{
		return relation;
	}
	public final void setRelation(ExpressRelationType value)
	{
		relation = value;
	}

	/** 
	 获取或设置过滤条件的值类型
	 <see cref="ExpressValueType"/>
	 
	*/
	private ExpressValueType expresstype = ExpressValueType.forValue(0);
	public final ExpressValueType getExpresstype()
	{
		return expresstype;
	}
	public final void setExpresstype(ExpressValueType value)
	{
		expresstype = value;
	}

	/** 
	 获取或设置过滤条件的比较符类型
	 <see cref="ExpressCompareType"/>
	 
	*/
	private ExpressCompareType compare = ExpressCompareType.forValue(0);
	public final ExpressCompareType getCompare()
	{
		return compare;
	}
	public final void setCompare(ExpressCompareType value)
	{
		compare = value;
	}

	private AbsConditionValue  customConditionValue=null;

  public AbsConditionValue getCustomConditionValue() {
    return customConditionValue;
  }

  public void setCustomConditionValue(
      AbsConditionValue customConditionValue) {
    this.customConditionValue = customConditionValue;
  }

	public boolean isParsed() {
		return isParsed;
	}

	public void setParsed(boolean parsed) {
		isParsed = parsed;
	}

	//针对表达式类型，表达式值是否被解析过
	private boolean isParsed = false;
}
