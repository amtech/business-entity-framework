/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.api.manager;

import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.api.dataType.base.IAccessorCreator;

public interface ICefDataTypeManager {

  ICefManagerContext getContext();

  ICefData createDataType();

  IAccessorCreator getAccessorCreator();

  // AccessorBase createAccessor(IDataType datatype);

  // AccessorBase createReadonlyAccessor(IDataType datatype);

  ICefData deserialize(String content);

  String serialize(ICefData data);

  String serializeChange(IChangeDetail change);

  IChangeDetail deserializeChange(String strChange);
}
