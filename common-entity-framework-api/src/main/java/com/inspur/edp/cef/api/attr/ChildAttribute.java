/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.api.attr;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ChildAttribute
{
	/** 
	 获取或设置有从属关系的实体接口的节点编号
	 
	*/
	String objectCode();

	/**
	 获取或设置节点的ID
	 
	*/
	String id();

	/** 
	 获取或设置有从属关系的实体接口上主键字段属性的名称
	 
	*/
	String keyFieldName();

	/** 
	 获取或设置有从属关系的实体接口上外键字段属性的名称
	 
	*/
	String parentFieldName();

	//废弃, 挪到子表Entity实现类上, 为了兼容暂时保留, 过一段时间考虑删掉
//	java.lang.Class serializerType();

	//废弃, 挪到子表Entity实现类上, 为了兼容暂时保留, 过一段时间考虑删掉
//	java.lang.Class targetInterfaceType();
}
