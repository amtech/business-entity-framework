/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.api.exceptions;

public final class ErrorCodes
{
	public static final String SUCode = "pf_common";

	public static final String Required = "Cef1001";
	public static final String Length = "Cef1002";
	public static final String ForeignKey = "Cef1003";
	public static final String DeleteCheck = "Cef1004";
	public static final String UniquenessCheck = "Cef1005";

	public static final String Scope_IncompleteConfig = "Cef2001";
	public static final String Scope_ExtensionConfigNotFound= "Cef2002";
	public static final String Scope_ExtensionTypeNotFound = "Cef2003";
	public static final String Scope_InvalidExtensionType = "Cef2004";
	public static final String Scope_JavaReflectionException = "Cef2005";
	public static final String Scope_ConvertBase64ToBinary = "Cef2006";
	public static final String Scope_BizContextDestroyFailed = "Cef2007";
	public static final String Scope_BizContextNotFound = "Cef2008";

	public static final String Changeset_CrossLevelMerging = "Cef3001";
}
