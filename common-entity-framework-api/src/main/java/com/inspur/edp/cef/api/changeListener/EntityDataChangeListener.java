/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.api.changeListener;

import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.accessor.entity.IRootAccessor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IMultiLanguageAcc;
import java.util.Objects;

public class EntityDataChangeListener {

  private IChangeHandlerContext context;
  private EntityDataChangeHandler handler;
  private boolean _isEquivalentValueEffective;

  public EntityDataChangeListener(IChangeHandlerContext context) {
    //DataValidator.CheckForNullReference(context, "context");

    this.context = context;
  }

  public static EntityDataChangeListener createInstance() {
    return new EntityDataChangeListener(new InnerChangeHandlerContext());
  }

  public void registListener(ICefData data) {
    innerRegistListener((IAccessor) data);
  }

  public void unregistListener(ICefData data) {
    innerUnregistListener((IAccessor) data);
  }

  private void innerRegistListener(IAccessor accessor) {
    Objects.requireNonNull(accessor, "accessor参数不能为空");

    //context = new InnerChangeHandlerContext();
    EntityDataChangeHandler tempVar = new EntityDataChangeHandler(context);
    tempVar.setIsEquivalentValueEffective(getIsEquivalentValueEffective());
    handler = tempVar;
    accessor.addPropertyChangingListener(handler);
    if (accessor instanceof IRootAccessor) {
      ((IRootAccessor) accessor).addItemRemovedListener(handler);
      ((IRootAccessor) accessor).addItemAddedListener(handler);
    }
    if(accessor instanceof IMultiLanguageAcc){
      ((IMultiLanguageAcc) accessor).addMultiLangChangingListener(handler);
    }
  }

  private void innerUnregistListener(IAccessor accessor) {
    Objects.requireNonNull(accessor, "accessor参数不能为空");

    accessor.removePropertyChangingListener(handler);
    if (accessor instanceof IRootAccessor)
    {
      ((IRootAccessor)accessor).removeItemRemovedListener(handler);
      ((IRootAccessor)accessor).removeItemAddedListener(handler);
    }
    if(accessor instanceof IMultiLanguageAcc){
      ((IMultiLanguageAcc) accessor).removeMultiLangChangingListener(handler);

    }
  }

  public IChangeDetail getChange() {
    return context.getChangeDetail();
  }

  public final void clear() {
    context.setChangeDetail(null);
  }

  public final void suspend() {
    if(handler != null) {
      handler.suspend();
    }
  }

  public final void resume() {
    if(handler != null) {
      handler.resume();
    }
  }

  /**
   * 是否监听相等的赋值, 默认为否
   */
  public final boolean getIsEquivalentValueEffective() {
    return _isEquivalentValueEffective;
  }

  public final void setIsEquivalentValueEffective(boolean value) {
    if (handler != null) {
      handler.setIsEquivalentValueEffective(value);
    }
    _isEquivalentValueEffective = value;
  }
}
