/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.api.message;

public enum MessageLevel 
{
	Success(0),
	Info(1),
	Warning(2),
	Error(3);

	private int intValue;
	private static java.util.HashMap<Integer, MessageLevel> mappings;
	private synchronized static java.util.HashMap<Integer, MessageLevel> getMappings()
	{
		if (mappings == null)
		{
			mappings = new java.util.HashMap<Integer, MessageLevel>();
		}
		return mappings;
	}

	private MessageLevel(int value)
	{
		intValue = value;
		MessageLevel.getMappings().put(value, this);
	}

	public int getValue()
	{
		return intValue;
	}

	public static MessageLevel forValue(int value)
	{
		return getMappings().get(value);
	}
}
