/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptoritem;

import io.iec.edp.caf.commons.event.config.CompositePropertySourceFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Configuration
@PropertySource(
        value = {"application.yml"},
        factory = CompositePropertySourceFactory.class
)
@ConfigurationProperties(
        prefix = "befdbextend-settings"
)
public class DBExtendInfo {
    private String dbName = "";
    private String sqlGenerateClass = "";
    private String dbProcessorClass;

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getSqlGenerateClass() {
        return sqlGenerateClass;
    }

    public void setSqlGenerateClass(String sqlGenerateClass) {
        this.sqlGenerateClass = sqlGenerateClass;
    }

    public String getDbProcessorClass() {
        return dbProcessorClass;
    }

    public void setDbProcessorClass(String dbProcessorClass) {
        this.dbProcessorClass = dbProcessorClass;
    }
}
