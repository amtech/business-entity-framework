//package Inspur.Gsp.Cef.Repository.datacache;
//
//import Inspur.Ecp.Caf.Caching.*;
//import Inspur.Gsp.Cef.Entity.*;
//
//public class EntityMemoryCacheAdapter
//{
//	private static EntityMemoryCacheAdapter intance;
//	public static EntityMemoryCacheAdapter getInstance()
//	{
//		if (intance == null)
//		{
//			intance = new EntityMemoryCacheAdapter();
//		}
//		return intance;
//	}
//
//	private IMemoryCache GetCache(String cacheConfigID)
//	{
//		return CacheFactory.GetMemoryCache(cacheConfigID);
//	}
//
//	public final void SetDataToCache(String cacheConfigID, IEntityData data)
//	{
//		GetCache(cacheConfigID).Set(data.getID(), data);
//	}
//
//	public final void RemoveCache(String cacheConfigID, String dataID)
//	{
//		GetCache(cacheConfigID).Remove(dataID);
//	}
//
//	public final IEntityData GetData(String cacheConfigID, String dataID)
//	{
//		Object tempVar = GetCache(cacheConfigID).Get(dataID);
//		return (IEntityData)((tempVar instanceof IEntityData) ? tempVar : null);
//	}
//}