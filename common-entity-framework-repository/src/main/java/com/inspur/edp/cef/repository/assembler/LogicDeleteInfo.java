/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler;

public class LogicDeleteInfo {
    /**
     * 逻辑删除
     */
    private boolean enableLogicDelete = false;
    /**
     * 字段编号
     */
    private String labelId;

    public LogicDeleteInfo(boolean enableLogicDelete, String labelId) {
        this.enableLogicDelete = enableLogicDelete;
        this.labelId = labelId;
    }

    public boolean isEnableLogicDelete() {
        return enableLogicDelete;
    }

    public void setEnableLogicDelete(boolean enableLogicDelete) {
        this.enableLogicDelete = enableLogicDelete;
    }

    public String getLabelId() {
        return labelId;
    }

    public void setLabelId(String labelId) {
        this.labelId = labelId;
    }
}
