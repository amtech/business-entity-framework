/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.repository.readerwriter;

import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;

public class CefMappingReader implements ICefReader {

  //Key:udtElementLabel, Value:DbColumnName
  private java.util.HashMap<String, String> mapping;
  private ICefReader reader;

  public CefMappingReader(java.util.HashMap<String, String> mapping, ICefReader reader) {
    DataValidator.checkForNullReference(mapping, "mapping");
    DataValidator.checkForNullReference(reader, "reader");

    this.mapping = mapping;
    this.reader = reader;
  }

//  @Override
//  public final Object getString(String propName) {
//    DataValidator.checkForEmptyString(propName, "propName");
//    if (!mapping.containsKey(propName)) {
//      return null;
//    }
//    return reader.getString(mapping.get(propName));
//  }

  @Override
  public final Object readValue(String propName) {
    DataValidator.checkForEmptyString(propName, "propName");
    String mappingPropName = "";
    if (mapping.containsKey(propName)) {
      mappingPropName = mapping.get(propName);
    }
    return reader.readValue(mappingPropName.equals("") ? propName: mappingPropName);
  }

  @Override
  public boolean hasProperty(String propName) {
    return mapping.containsKey(propName);
  }
}
