/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptoritem.dbprocessor;

import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.entity.entity.EntityDataPropertyValueUtils;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.dataaccess.DbType;
import org.hibernate.jpa.TypedParameterValue;
import org.hibernate.type.StandardBasicTypes;

import javax.persistence.Query;
import javax.persistence.TemporalType;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Date;

public class DB2DbProcessor extends DbProcessor {

    @Override
    protected final void buildNClobParameter(Query query, int i, DbParameter param) {
        query.setParameter(i, new TypedParameterValue(StandardBasicTypes.TEXT, param.getValue()));
    }

    @Override
    protected final void buildDateTimeParameter(Query query, int i, DbParameter param) {
        query.setParameter(i, (Date) param.getValue(), TemporalType.DATE);
    }

    @Override
    public final String getStringValue(ICefReader reader, String propName) {
        return super.getStringValue(reader, propName);
    }

    @Override
    public final byte[] getBlobValue(ICefReader reader, String propName) {
        Object obj = reader.readValue(propName);
        if (!(obj instanceof Blob) && obj != null) {
            throw new RuntimeException("DB2数据库取出数据类型异常！");
        } else {
            try {
                return obj == null ? EntityDataPropertyValueUtils.getBinaryPropertyDefaultValue() : ((Blob) obj).getBytes(1L, (int) ((Blob) obj).length());
            } catch (SQLException e) {
                throw new RuntimeException("DB2数据库解析备注类型数据：" + obj + "失败");
            }
        }
    }
}
