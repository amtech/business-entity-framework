/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptoritem;

import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfoCollection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdaptorItemSqlCache {
    //缓存ColumnInfo信息

    private DbColumnInfoCollection containColumns = new DbColumnInfoCollection();
    private boolean columnInit = false;
    private Map<String, String> queryFieldsMap = new HashMap<>();
    private HashMap<String, HashMap<String, Integer>> queryMappingMap = new HashMap<>();//列和索引映射
    private HashMap<String, HashMap<Integer, String>> insertMultiIndexMap = new HashMap<>();
    private Map<String, String> queryFieldsWithMultiMap = new HashMap<>();
    private HashMap<String, HashMap<String, Integer>> queryWithMultiMappingMap = new HashMap<>();//多语列和索引映射
    private Map<String, String> insertFieldsMap = new HashMap<>();
    private Map<String, String> insertFieldsWithMultiMap = new HashMap<>();
    private Map<String, String> insertFieldValueMap = new HashMap<>();
    private Map<String, String> insertFieldValueWithMultiMap = new HashMap<>();
    private Map<String, RefObject<Integer>> multiValusCount = new HashMap<>();
    private Map<String, RefObject<Integer>> valusCount = new HashMap<>();

    DbColumnInfoCollection  getContainColumns(){ return containColumns; }

    public boolean isColumnInit() {
        return columnInit;
    }

    public void setColumnInit(boolean columnInit) {
        this.columnInit = columnInit;
    }

    Map<String, String> getQueryFieldsMap() {
        return queryFieldsMap;
    }

    HashMap<String, HashMap<String, Integer>> getQueryMappingMap(){
        return queryMappingMap;
    }

    HashMap<String, HashMap<Integer, String>> getInsertMultiIndexMap(){
        return insertMultiIndexMap;
    }


    Map<String, String> getQueryFieldsWithMultiMap() {
        return queryFieldsWithMultiMap;
    }

    HashMap<String, HashMap<String, Integer>> getQueryWithMultiMappingMap(){
        return queryWithMultiMappingMap;
    }
    Map<String, String> getInsertFieldsMap() {
        return insertFieldsMap;
    }

    Map<String, String> getInsertFieldsWithMultiMap() {
        return insertFieldsWithMultiMap;
    }

    Map<String, String> getInsertFieldValueMap() {
        return insertFieldValueMap;
    }

    Map<String, String> getInsertFieldValueWithMultiMap() {
        return insertFieldValueWithMultiMap;
    }

    Map<String, RefObject<Integer>> getMultiValusCount() {
        return multiValusCount;
    }

    Map<String, RefObject<Integer>> getValusCount() {
        return valusCount;
    }
}
