/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.repository.typetransprocesser;

import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.FilterCondition;

import java.sql.Connection;

public class BinaryTransProcesser implements ITypeTransProcesser
{
	private static BinaryTransProcesser instance;
	public static BinaryTransProcesser getInstacne()
	{
		if (instance == null)
		{
			instance = new BinaryTransProcesser();
		}
		return instance;
	}
	public final Object transType(FilterCondition filter, Connection db)
	{
		throw new UnsupportedOperationException("不支持对二进制字段进行过滤。");
	}

	public final Object transType(Object value)
	{
		return value;
	}

	@Override
	public Object transType(Object value, boolean isNull) {
		return transType(value);
	}
}
