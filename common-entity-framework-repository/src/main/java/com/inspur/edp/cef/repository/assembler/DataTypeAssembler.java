/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler;

import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.entity.entity.ICefData;

import java.util.ArrayList;

public abstract class DataTypeAssembler
{
	protected java.util.ArrayList<AssociationInfo> associationInfos = new java.util.ArrayList<AssociationInfo>();
	public java.util.ArrayList<AssociationInfo> getAssociationInfos (){return  associationInfos;}

	public DataTypeAssembler()
	{
		initAssociationInfo();
	}
	/** 
	 初始化关联信息
	 使用<see cref="addAssociationInfo"/>AddAssociationInfo方法添加关联信息
	 
	 使用<see cref="addAssociationInfo"/>方法添加关联信息
	*/
	public void initAssociationInfo()
	{
	}

	/**
	 * 获取逻辑删除信息
	 * @return
	 */
	public LogicDeleteInfo getLogicDeleteInfo(){
		return new LogicDeleteInfo(false, "");
	}


	public void readData(ICefData data, ICefReader reader)
	{
	}

	public void writeData()
	{
	}

	/** 
	 添加关联信息
	 
	 @param nodeCode 关联节点编号
	 @param sourceColumn 源字段
	 @param targetColumn 目标字段
	 @param refRepository 引用实体Repository
	 @param refColumns 引用字段集合
	*/
	protected final void addAssociationInfo(String configId, String nodeCode, String sourceColumn, String targetColumn, java.lang.Class associationType
			, java.util.HashMap<String, String> refColumns)
	{
		AssociationInfo tempVar = new AssociationInfo();
		tempVar.setConfigId(configId);
		tempVar.setNodeCode(nodeCode);
		tempVar.setSourceColumn(sourceColumn);
		tempVar.setTargetColumn(targetColumn);
		tempVar.setAssociationType(associationType);
		tempVar.setRefColumns(refColumns);
		AssociationInfo info = tempVar;
		associationInfos.add(info);
	}

	protected final void addAssociationInfo(String configId, String nodeCode, String sourceColumn, String targetColumn, java.lang.Class associationType
			, java.util.HashMap<String, String> refColumns, ArrayList<AssoCondition> assoConditions)
	{
		AssociationInfo tempVar = new AssociationInfo();
		tempVar.setConfigId(configId);
		tempVar.setNodeCode(nodeCode);
		tempVar.setSourceColumn(sourceColumn);
		tempVar.setTargetColumn(targetColumn);
		tempVar.setAssociationType(associationType);
		tempVar.setRefColumns(refColumns);
		tempVar.setAssoConditions(assoConditions);
		AssociationInfo info = tempVar;
		associationInfos.add(info);
	}
}
