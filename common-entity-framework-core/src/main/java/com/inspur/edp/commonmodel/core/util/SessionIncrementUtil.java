/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.commonmodel.core.util;

import com.inspur.edp.cef.api.session.ICefSession;
import com.inspur.edp.commonmodel.core.session.serviceinterface.SessionConfigService;
import com.inspur.edp.commonmodel.core.session.tableentity.ConnectType;
import com.inspur.edp.commonmodel.core.session.tableentity.SessionCacheInfo;
import com.inspur.edp.commonmodel.core.session.tableentity.SessionConfig;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class SessionIncrementUtil {

    private SessionConfig config;

    private SessionIncInterface util;

    private boolean isUseAvailabilityFeature;

    private boolean isCreateTable;

    public SessionIncrementUtil() {
        this(true);
    }

    public SessionIncrementUtil(boolean isCreateTable){
        SessionConfigService service = SpringBeanUtils.getBean(SessionConfigService.class);
        this.config = service.getSessionConfig();
        if(this.config == null){
            this.isUseAvailabilityFeature = false;
        }else {
            if (this.config.getConnectInfo().isUseCache() && this.config.getConnectInfo().getConnectType() == ConnectType.DataBase) {
                this.isUseAvailabilityFeature = true;
                this.isCreateTable = isCreateTable;
                this.util = new SessionIncByDbUtil(this.config,isCreateTable);
            } else {
                this.isUseAvailabilityFeature = false;
                this.util = null;
            }
        }
    }

    public boolean isStorageInited(){
        Objects.requireNonNull(util);
        return util.isStorageInited();
//        return isCreateTable;
    }

    public boolean isUseAvailabilityFeature() {
        return isUseAvailabilityFeature;
    }

    public List<SessionCacheInfo> getSessionCacheInfos(String sessionId, int version) {
        if(!config.getConnectInfo().isUseCache())
            return null;
        if(config.getConnectInfo().getConnectType() == ConnectType.DataBase){
            return this.util.getSessionCacheInfos(sessionId,version);
        }else {
            return null;
        }
    }

    public void saveSessionCache(SessionCacheInfo info, ICefSession items){
        if(!config.getConnectInfo().isUseCache())
            return;
        if(config.getConnectInfo().getConnectType() == ConnectType.DataBase){
            this.util.saveSessionCache(info,items);
        }
    }

    public void clearSessionCache(String sessionId){
        if(!config.getConnectInfo().isUseCache())
            return;
        if(config.getConnectInfo().getConnectType() == ConnectType.DataBase){
            this.util.clearSessionCache(sessionId);
        }
    }

    public boolean isLatestVersion(String sessionId,int version){
        if(!config.getConnectInfo().isUseCache())
            return true;
        if(config.getConnectInfo().getConnectType() == ConnectType.DataBase) {
            return this.util.isLatestVersion(sessionId, version);
        }
        return true;
    }

    public List<List<SessionCacheInfo>> traversalCache(List<Date> dates){
        if(config.getConnectInfo().getConnectType() == ConnectType.DataBase){
            return this.util.traversalSessionCacheInfos(dates);
        }
        return null;
    }

    public void batchClear(List<List<String>> sessionIds){
        if(config.getConnectInfo().getConnectType() == ConnectType.DataBase) {
            this.util.batchClear(sessionIds);
        }
    }

    public int getDBTableNumber(){
        if(config.getConnectInfo().getConnectType() == ConnectType.DataBase) {
            return config.getConnectInfo().getDBConnectInfo().getTableCount();
        }else {
            throw new RuntimeException("配置信息错误，请配置为数据库存储");
        }
    }
}
