/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.commonmodel.core.session.tableentity;

import com.inspur.edp.cef.api.repository.GspDbType;

public class DBConnectInfo {

    private int tableCount = 0;

    private boolean isUseDefConnect = true;

    private String tableName = "BefSessionCacheInfo";

    private GspDbType dbType;

    private String serverIp;

    private String port;

    private String dbInstance;

    private String userId;

    private String password;

    private int maxPoolSize;

    private boolean Enlist;

    public int getTableCount() {
        return tableCount;
    }

    public void setTableCount(int connectCount) {
        this.tableCount = connectCount;
    }

    public boolean isUseDefConnect() {
        return isUseDefConnect;
    }

    public void setUseDefConnect(boolean useDefConnect) {
        isUseDefConnect = useDefConnect;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public GspDbType getDbType() {
        return dbType;
    }

    public void setDbType(GspDbType dbType) {
        this.dbType = dbType;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String server) {
        this.serverIp = server;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getDbInstance() {
        return dbInstance;
    }

    public void setDbInstance(String database) {
        this.dbInstance = database;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public boolean isEnlist() {
        return Enlist;
    }

    public void setEnlist(boolean enlist) {
        Enlist = enlist;
    }
}
