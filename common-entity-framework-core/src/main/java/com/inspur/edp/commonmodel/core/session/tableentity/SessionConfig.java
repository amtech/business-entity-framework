/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.commonmodel.core.session.tableentity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Table
@Entity
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionConfig {

    @Id
    //租户_应用实例_SU  LC10000_Ins001_SCM
    private String id;

    private String belongTenant;

    private String belongAppInstance;

    private String belongSU;

    private Date lastChangedOn;

    private String configInfo;

    //防止每次获取后反序列化导致执行updata语句，增加一个transient对象记录连接信息
    @Transient
    private transient ConnectInfo connectInfo;

    //增加一个transient对象记录每个su下是否已经自动建表
    @Transient
    private transient boolean isSessionTableExist;

    public boolean isSessionTableExist() {
        return isSessionTableExist;
    }

    public void setSessionTableExist(boolean sessionTableExist) {
        isSessionTableExist = sessionTableExist;
    }

    public ConnectInfo getConnectInfo() {
        return connectInfo;
    }

    public void setConnectInfo(ConnectInfo info) {
        this.connectInfo = info;
    }

    public String getConfigInfo() {
        return configInfo;
    }

    public void setConfigInfo(String configInfo) {
        this.configInfo = configInfo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBelongTenant() {
        return belongTenant;
    }

    public void setBelongTenant(String belongTenant) {
        this.belongTenant = belongTenant;
    }

    public String getBelongAppInstance() {
        return belongAppInstance;
    }

    public void setBelongAppInstance(String belongInstance) {
        this.belongAppInstance = belongInstance;
    }

    public String getBelongSU(){
        return this.belongSU;
    }

    public void setBelongSU(String su){
        this.belongSU = su;
    }

    public Date getLastChangedOn() {
        return lastChangedOn;
    }

    public void setLastChangedOn(Date lastChangedOn) {
        this.lastChangedOn = lastChangedOn;
    }
}
