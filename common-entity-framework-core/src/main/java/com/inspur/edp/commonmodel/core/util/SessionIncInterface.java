/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.commonmodel.core.util;

import com.inspur.edp.cef.api.session.ICefSession;
import com.inspur.edp.commonmodel.core.session.tableentity.SessionCacheInfo;

import java.util.Date;
import java.util.List;

public interface SessionIncInterface {

    List<SessionCacheInfo> getSessionCacheInfos(String sessionId, int version);

    void saveSessionCache(SessionCacheInfo info, ICefSession items);

    void clearSessionCache(String sessionId);

    boolean isLatestVersion(String id, int ver);

    List<List<SessionCacheInfo>> traversalSessionCacheInfos(List<Date> dates);

    void batchClear(List<List<String>> sessionIds);

    boolean isStorageInited();
}
