/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.commonmodel.core.session.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.api.session.ICefSessionItem;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.entity.changeset.Tuple;
import com.inspur.edp.commonmodel.core.session.distributed.SessionItemDistributedFactory;
import com.inspur.edp.commonmodel.core.session.distributed.dac.FuncSessionItemIncrement;
import com.inspur.edp.commonmodel.core.util.SessionIncSerializerUtil;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.List;
import java.util.Map.Entry;

public class SessionItemSerializer extends JsonSerializer<FuncSessionItemIncrement> {

  private ICefSessionItem item;

  List<Tuple<String, String>> itemTypes;

  public SessionItemSerializer(ICefSessionItem item, List<Tuple<String, String>> itemTypes) {
    this.item = item;
    this.itemTypes = itemTypes;
  }

  @Override
  public void serialize(FuncSessionItemIncrement increment, JsonGenerator writer,
      SerializerProvider serializers) {
    SerializerUtils.writeStartObject(writer);
    if (increment.getIndex() != 0) {
      SerializerUtils.writePropertyValue(writer, "index", increment.getIndex());
    }
    if (increment.isReset()) {
      SerializerUtils.writePropertyValue(writer, "reset", increment.isReset());
    }
    if (increment.getLockIds() != null && increment.getLockIds().size() > 0) {
      SerializerUtils.writePropertyValue(writer, "lockIds", increment.getLockIds());
    }
    if (increment.getVariable() != null) {
      writeVariable(writer, increment);
    }
    if (increment.getChangeDetails() != null && increment.getChangeDetails().size() > 0) {
      writeChangeDetails(writer, increment);
    }
    if(increment.getCustoms() != null && !increment.getCustoms().isEmpty()) {
      writeCustoms(writer, increment);
    }
    SerializerUtils.writeEndObject(writer);
  }

  private void writeCustoms(JsonGenerator writer, FuncSessionItemIncrement increment) {
    SerializerUtils.writePropertyName(writer, "cust");
//    SerializerUtils.writeStartObject(writer);
    SerializerUtils.writePropertyValue_Object(writer, increment.getCustoms());
//    for (Entry<String, String> pair : increment.getCustoms().entrySet()) {
//      SerializerUtils.writePropertyValue(writer, pair.getKey(), pair.getValue());
//    }
//    SerializerUtils.writeEndObject(writer);
  }

  private void writeVariable(JsonGenerator writer, FuncSessionItemIncrement increment) {
    //TODO: 相同代码提取方法
    SessionIncSerializerUtil service = SpringBeanUtils
        .getBean(item.getCategory(), SessionItemDistributedFactory.class).getSerializer();
    service.serializerVar(writer, increment, this.item);
  }

  private void writeChangeDetails(JsonGenerator writer, FuncSessionItemIncrement increment) {
    SessionIncSerializerUtil service = SpringBeanUtils
        .getBean(item.getCategory(), SessionItemDistributedFactory.class).getSerializer();
    service.serializerChanges(writer, increment, this.item);
  }
}
