/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.core.rootmanager;

import com.inspur.edp.cef.api.changeset.IChangesetManager;
import com.inspur.edp.cef.api.manager.IBufferManager;
import com.inspur.edp.cef.api.rootManager.ICefRootManager;
import com.inspur.edp.cef.api.rootManager.IRootManagerContext;

public abstract class BaseRootManagerContext implements IRootManagerContext
{
	private ICefRootManager _rootMgr;
	public ICefRootManager getRootManager(){return _rootMgr;}
	public final void setRootManager(ICefRootManager value)
	{
		if (_rootMgr != null)
		{
			throw new UnsupportedOperationException();
		}
		_rootMgr = value;
	}

	private IChangesetManager privateChangesetManager;
	public final IChangesetManager getChangesetManager()
	{
		return privateChangesetManager;
	}
	public final void setChangesetManager(IChangesetManager value)
	{
		privateChangesetManager = value;
	}

	private IBufferManager privateBufferManager;
	public final IBufferManager getBufferManager()
	{
		return privateBufferManager;
	}
	public final void setBufferManager(IBufferManager value)
	{
		privateBufferManager = value;
	}
}
