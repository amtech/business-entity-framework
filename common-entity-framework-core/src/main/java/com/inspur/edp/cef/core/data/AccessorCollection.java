/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.core.data;

import com.inspur.edp.cef.entity.accessor.entity.IChildAccessor;
import com.inspur.edp.cef.entity.accessor.entity.IEntityAccessor;
import com.inspur.edp.cef.entity.entity.IChildEntityData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import java.util.Iterator;
import java.util.function.Function;
import java.util.function.Supplier;

public class AccessorCollection extends
    com.inspur.edp.cef.entity.accessor.entity.AccessorCollection {

  protected final Function<IChildEntityData, IChildAccessor> accCreator;
  protected final Supplier<IChildEntityData> dataCreator;
  protected final CefEntityResInfoImpl resInfo;

  public AccessorCollection(IEntityAccessor belongEntity, CefEntityResInfoImpl resInfo,
      Supplier<IChildEntityData> dataCreator,
      Function<IChildEntityData, IChildAccessor> accCreator) {
    super(belongEntity);
    this.resInfo = resInfo;
    this.dataCreator = dataCreator;
    this.accCreator = accCreator;

    initialize(belongEntity);
  }

  private void initialize(IEntityAccessor belongEntity) {
    if (belongEntity == null || belongEntity.getInnerData() == null) {
      return;
    }
    IEntityDataCollection collection = ((IEntityData) belongEntity.getInnerData()).getChilds()
        .get(resInfo.getEntityCode());
    if (collection == null) {
      return;
    }

    for (IEntityData item : collection) {
      this.innerAdd(accCreator.apply((IChildEntityData) item));
    }
  }

  @Override
  public IChildAccessor createInstance() {
    IChildEntityData childData = dataCreator.get();
    childData.setParentID(getParent() != null ? getParent().getID() : "");
    return accCreator.apply(childData);
  }

  @Override
  protected com.inspur.edp.cef.entity.accessor.entity.AccessorCollection createNewObject() {
    return new AccessorCollection(null, resInfo, dataCreator, accCreator);
  }

  @Override
  public Iterator<IEntityData> iterator() {
    if (resInfo == null || resInfo.getSorts() == null || resInfo.getSorts().isEmpty()) {
      return super.iterator();
    }
    return super.innerGetAll().stream()
        .sorted(new EntityDataCollectionComparator(resInfo.getSorts())).iterator();
  }
}
