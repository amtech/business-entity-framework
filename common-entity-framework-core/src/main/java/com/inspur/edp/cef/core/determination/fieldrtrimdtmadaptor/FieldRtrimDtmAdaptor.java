/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.core.determination.fieldrtrimdtmadaptor;

import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.AbstractModifyChangeDetail;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.spi.determination.IDetermination;
import java.util.List;

public class FieldRtrimDtmAdaptor implements IDetermination {

  protected List<String> propNames;
  private ICefDeterminationContext context;

  public FieldRtrimDtmAdaptor(List<String> propNames) {
    this.propNames = propNames;
  }

  @Override
  public String getName() {
    return null;
  }

  @Override
  public boolean canExecute(IChangeDetail change) {
    return change != null && change.getChangeType() != ChangeType.Deleted;
  }

  @Override
  public void execute(ICefDeterminationContext context, IChangeDetail change) {
    if (context.getData() == null) {
      return;
    }
    executeFieldRtrim(context, change);
  }

  protected void executeFieldRtrim(ICefDeterminationContext context, IChangeDetail change) {
    for (String propName : this.propNames) {
      if(change.getChangeType()==ChangeType.Modify&&((AbstractModifyChangeDetail)change).getPropertyChanges().containsKey(propName)==false)
        continue;
      Object propValue = context.getData().getValue(propName);
      if (!(propValue instanceof String)) {
        return;
      }
      context.getData().setValue(propName, trimEnd((String) propValue));
    }
  }

  private String trimEnd(String str) {
    int len = str.length();
    int st = 0;
    char[] val = str.toCharArray();

    while ((st < len) && (val[len - 1] <= ' ')) {
      len--;
    }
    return str.substring(st, len);
  }
}
