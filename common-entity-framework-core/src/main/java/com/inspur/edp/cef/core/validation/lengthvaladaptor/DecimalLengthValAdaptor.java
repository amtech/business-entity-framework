/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.core.validation.lengthvaladaptor;


import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.api.validation.IValueObjValidationContext;
import com.inspur.edp.cef.core.i18n.I18nResourceUtil;
import java.math.BigDecimal;
import org.springframework.util.StringUtils;

public
class DecimalLengthValAdaptor extends LengthValAdaptor<java.math.BigDecimal> {

    private int cefVersion;

    public
    DecimalLengthValAdaptor(String propName, int length, int prec) {
        super(propName, length, prec);
    }

    public
    DecimalLengthValAdaptor(String propName, int length, int prec,int cefVersion) {
        this(propName, length, prec);
        this.cefVersion = cefVersion;
    }

    @Override
    protected
    String getDisplayMessage(ICefValidationContext context) {
        //国际化异常调用资源文件
        String exceptionCode = I18nResourceUtil.getResourceItemValue("pfcommon", "cef_exception.properties", "Gsp_Cef_DigDeciamlLenDecVal_0001");
        String lengthExceptionCode = I18nResourceUtil.getResourceItemValue("pfcommon", "cef_exception.properties", "Gsp_Cef_DigDeciamlLenDecVal_0002");
        String precisionExceptionCode = I18nResourceUtil.getResourceItemValue("pfcommon", "cef_exception.properties", "Gsp_Cef_DigDeciamlLenDecVal_0003");
        String i18nMessage = getI18nMessage(context);
        String lengthMessage = String.format(lengthExceptionCode, i18nMessage, curLength, this.length - prec);
        String precisionMessage = String.format(precisionExceptionCode, i18nMessage, curPrec, prec);

        // 分场景进行提示
        if (curLength > length - prec && curPrec > prec) {
            // 精度和长度均超出
            return lengthMessage + precisionMessage;
        }
        if (curLength > length - prec) {
            // 长度超出
            return lengthMessage;
        }
        if (curPrec > prec) {
            // 精度超出
            return precisionMessage;
        }
        return String.format(exceptionCode,i18nMessage,Integer.toString(this.length),Integer.toString(this.prec));
    }

    private String getI18nMessage(ICefValidationContext context) {
        if (context instanceof IValueObjValidationContext) {
            if (((IValueObjValidationContext) context).getParentContext() == null || ((IValueObjValidationContext) context).getPropertyName() == null || "".equals(((IValueObjValidationContext) context).getPropertyName())) {
                // 兼容之前的业务字段校验信息提示
                return context.getPropertyI18nName(propName);
            } else {
                // 业务字段必填提示信息提示BE的字段名称
                return (((IValueObjValidationContext) context).getParentContext()).getPropertyI18nName(((IValueObjValidationContext) context).getPropertyName());
            }
        } else {
            // BE上的必填校验提示信息
            return context.getPropertyI18nName(propName);
        }
    }

    @Override
    protected
    boolean isValid(BigDecimal value) {
//		throw new UnsupportedOperationException();
        //if(value == null || value.equals(BigDecimal.ZERO)){
        //电科院兼容
        if(cefVersion==0) {
            if (value == null || value.equals(BigDecimal.ZERO) || this.length == 1) {
                return true;
            }
        }
        else
        {
            if(value==null||value.equals(BigDecimal.ZERO))
                return true;
        }

        String decimalStr = value.abs().toPlainString();
        if (decimalStr.contains(".")) {
            int indexOfPoint = decimalStr.indexOf(".");
            this.curLength = decimalStr.substring(0, indexOfPoint).length();
            this.curPrec = StringUtils
                .trimTrailingCharacter(decimalStr.substring(indexOfPoint + 1), '0').length();
            return curLength <= length - prec && curPrec <= prec;
        } else {
            this.curLength = decimalStr.length();
            this.curPrec = 0;
            return (curLength <= length - prec);
        }
    }
 }
