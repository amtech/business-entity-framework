/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.core.validation.requiredvaladaptor;

import com.inspur.edp.cef.api.exceptions.ErrorCodes;
import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.ISupportBizMessage;
import com.inspur.edp.cef.api.message.MessageLevel;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.api.validation.IValueObjValidationContext;
import com.inspur.edp.cef.core.i18n.I18nResourceUtil;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.spi.validation.IValidation;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import java.util.Arrays;

public class RequiredValAdaptor<T> implements IValidation
{
	protected String propName;
    public RequiredValAdaptor(String propName)
	{
		DataValidator.checkForEmptyString(propName, "propName");
		this.propName = propName;
	}

	public boolean canExecute(IChangeDetail change)
	{return change == null || change.getChangeType() != ChangeType.Deleted;}

	public final void execute(ICefValidationContext context, IChangeDetail change) {
		try {
			if (context.getData() != null && isValid((T) context.getData().getValue(propName))) {
				return;
			}
		} catch (Exception e) {
			throw new RuntimeException("字段[" + propName + "]检查必填时发生异常", e);
		}
		onInvalid(context, change);
	}

	private void onInvalid(ICefValidationContext context, IChangeDetail change)
	{
		if(context instanceof ISupportBizMessage){
			IBizMessage msg = ((ISupportBizMessage) context).createMessageWithLocation(
					MessageLevel.Error,
					Arrays.asList(propName),
					getDisplayMessage(context));
			((ISupportBizMessage) context).addMessage(msg);
		} else {
			throw new CAFRuntimeException(
					"pf_common",
					ErrorCodes.Required,
					getDisplayMessage(context),
					null,
					ExceptionLevel.Warning,
					true);
		}
	}

	private String getDisplayMessage(ICefValidationContext context) {
		String exceptionCode=I18nResourceUtil.getResourceItemValue("pfcommon","cef_exception.properties","Gsp_Cef_RequireVal_0001");
		//判断context是udt上的UdtValidationContext校验上下文还是BE上的校验上下文ValidationContext
		if(context instanceof IValueObjValidationContext){

//			IUnifiedDataType dataType=(IUnifiedDataType)(((UdtValidationContext)context).getUdtContext().getValueObjDataType());

				if(((IValueObjValidationContext)context).getParentContext()==null || ((IValueObjValidationContext)context).getPropertyName()==null || "".equals(((IValueObjValidationContext)context).getPropertyName())) {

					exceptionCode=String.format(exceptionCode,context.getPropertyI18nName(propName));//兼容之前的业务字段校验信息提示

				}
				else {
					//业务字段必填提示信息提示BE的字段名称

					exceptionCode = String.format(exceptionCode, (((IValueObjValidationContext)context).
							getParentContext()).
							getPropertyI18nName(((IValueObjValidationContext)context).getPropertyName()));

				}
		}
		else {
			//BE上的必填校验提示信息
			exceptionCode=String.format(exceptionCode,context.getPropertyI18nName(propName));

		}
		return	exceptionCode;
	}

	protected boolean isValid(T value) {return  value != null;}
}
