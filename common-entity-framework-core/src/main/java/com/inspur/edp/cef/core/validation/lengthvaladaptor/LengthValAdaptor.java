/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.core.validation.lengthvaladaptor;

import com.inspur.edp.cef.api.exceptions.ErrorCodes;
import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.ISupportBizMessage;
import com.inspur.edp.cef.api.message.MessageLevel;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.entity.changeset.AbstractModifyChangeDetail;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.spi.validation.IValidation;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import java.util.Arrays;

public abstract class LengthValAdaptor<T> implements IValidation
{
	protected String propName;
	protected int length;
	protected int prec;
	protected int curLength;
	protected int curPrec;

	protected LengthValAdaptor(String propName, int length, int prec)
	{
		DataValidator.checkForEmptyString(propName, "propName");

		this.propName = propName;
		this.length = length;
		this.prec = prec;
	}

	@Override
	public final boolean canExecute(IChangeDetail change)
	{
		if (change == null) {
			return true;
		}
		AbstractModifyChangeDetail modifyChange = (AbstractModifyChangeDetail)((change instanceof AbstractModifyChangeDetail) ? change : null);
		if (modifyChange != null)
		{
			return modifyChange.getPropertyChanges() != null && modifyChange.getPropertyChanges().containsKey(propName);
		}
		return change.getChangeType() == ChangeType.Added;
	}

	@Override
	public final void execute(ICefValidationContext context, IChangeDetail change)
	{
		try {
			if (context.getData() == null || isValid((T) context.getData().getValue(propName)))
				return;
		}
		catch(Exception e) {
			throw new RuntimeException("字段[" + propName + "]检查长度精度时发生异常", e);
		}
		onInvalid(context);
	}

    /**
     *
     * throw length or precision exceptions
     */
	private void onInvalid(ICefValidationContext context) {
		if(context instanceof ISupportBizMessage){
			IBizMessage msg = ((ISupportBizMessage) context)
					.createMessageWithLocation(MessageLevel.Error, Arrays.asList(propName),
							getDisplayMessage(context));
			((ISupportBizMessage) context).addMessage(msg);
		}else {
			throw new CAFRuntimeException("", ErrorCodes.Length, getDisplayMessage(context), null,
					ExceptionLevel.Warning, true);
		}
	}

	protected abstract String getDisplayMessage(ICefValidationContext context);

	protected abstract boolean isValid(T value);
}
