/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package  com.inspur.edp.cef.core.determination;

import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

//TODO: 与EntityDeterminationExecutor有重复
//AfterLoading 不依赖变更集
public class NoChangesetEntityDtmExecutor
{
	private IEntityRTDtmAssembler privateAssembler;
	protected final IEntityRTDtmAssembler getAssembler()
	{
		return privateAssembler;
	}

	private ICefEntityContext privateEntityContext;
	protected final ICefEntityContext getEntityContext()
	{
		return privateEntityContext;
	}

	public NoChangesetEntityDtmExecutor(IEntityRTDtmAssembler assembler, ICefEntityContext entityCtx)
	{
		DataValidator.checkForNullReference(assembler, "assembler");
		DataValidator.checkForNullReference(entityCtx, "entityCtx");

		privateAssembler = assembler;
		privateEntityContext= entityCtx;
	}

	private IChangeDetail privateChange;
	protected final IChangeDetail getChange()
	{
		return privateChange;
	}
	protected final void setChange(IChangeDetail value)
	{
		privateChange = value;
	}
	private ICefDeterminationContext privateContext;
	public final ICefDeterminationContext getContext()
	{
		return privateContext;
	}
	public final void setContext(ICefDeterminationContext value)
	{
		privateContext = value;
	}
	public void execute()
	{
		setContext(GetContext());

		executeBelongingDeterminations();

		executeDeterminations();

		executeChildDeterminations();
	}

	protected final void executeBelongingDeterminations()
	{
		////新增的数据行不需要执行业务字段的determination
		////因为udt是在初始化属性的时候执行新增时determinations
		//if (Change.ChangeType == ChangeType.Added)
		//    return;
		java.util.List<IDetermination> dtms = getAssembler().getBelongingDeterminations();
		if (dtms == null || dtms.isEmpty())
		{
			return;
		}


		for (IDetermination dtm : dtms)
		{
			executeDtmCore(dtm);
		}
	}

	protected final void executeDeterminations()
	{
		java.util.List<IDetermination> dtms = getAssembler().getDeterminations();
		if (dtms == null || dtms.isEmpty())
		{
			return;
		}


		for (IDetermination dtm : dtms)
		{
			executeDtmCore(dtm);
		}
	}

	protected final void executeChildDeterminations()
	{
		java.util.List<IDetermination> dtms = getAssembler().getChildAssemblers();
		if (dtms == null || dtms.isEmpty())
		{
			return;
		}


		for (IDetermination dtm : dtms)
		{
			executeDtmCore(dtm);
		}
	}

	protected void executeDtmCore(IDetermination dtm)
	{
		if (!dtm.canExecute(getChange()))
		{
			return;
		}
		dtm.execute(getContext(), getChange());
	}

	protected ICefDeterminationContext GetContext() {return  getAssembler().getDeterminationContext(getEntityContext());}

	protected IChangeDetail getChangeset() {return getAssembler().getChangeset(getEntityContext());}
}
