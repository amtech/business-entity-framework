/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.core.validation.deletecheckadaptor;

import com.fasterxml.jackson.databind.util.BeanUtil;
import com.inspur.edp.cef.api.exceptions.ErrorCodes;
import com.inspur.edp.cef.api.message.CefException;
import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.ISupportBizMessage;
import com.inspur.edp.cef.api.message.MessageLevel;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.core.i18n.I18nResourceUtil;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.DeleteChangeDetail;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.validation.IValidation;
import com.inspur.edp.svc.reference.check.api.CheckResult;
import com.inspur.edp.svc.reference.check.api.IReferenceCheckService;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.runtime.config.CefBeanUtil;
import java.util.List;
import org.springframework.beans.BeanUtils;

public class DeleteCheckValidationAdaptor implements IValidation
{
	private String befConfig;
	private String nodeCode;

	public DeleteCheckValidationAdaptor(String befConfig, String nodeCode)
	{
		this.befConfig = befConfig;
		this.nodeCode = nodeCode;
	}
	public final boolean canExecute(IChangeDetail change)
	{
		if (change == null)
		{
			return false;
		}
		return change.getChangeType() == ChangeType.Deleted;
	}

	public final void execute(ICefValidationContext context, IChangeDetail change)
	{

		DeleteChangeDetail deleteChange = (DeleteChangeDetail)((change instanceof DeleteChangeDetail) ? change : null);
		if (deleteChange == null)
		{
			return;
		}
		String dataId = deleteChange.getID();
		IReferenceCheckService service=SpringBeanUtils.getBean(IReferenceCheckService.class);
		String dbCode = befConfig + "/" + nodeCode;

		CheckResult result=service.checkReference(dbCode,dataId);
		//国际化获取 读取gsprefconfiginfo表中的关联信息
		// TODO: 2019/11/16
		if (result!=null && result.isReferenced()) {
			String message = buildMessage(result);
			if(context instanceof ISupportBizMessage){
				IBizMessage bizMsg = ((ISupportBizMessage)context).createMessageWithLocation(MessageLevel.Error, null, message, null);
				((ISupportBizMessage)context).addMessage(bizMsg);
			} else {
				throw new CAFRuntimeException("", ErrorCodes.DeleteCheck, message, null, ExceptionLevel.Warning, true);
			}
		}
	}

	private static String buildMessage(CheckResult result){
		String warningMessage= I18nResourceUtil.getResourceItemValue("pfcommon","cef_exception.properties","Gsp_Cef_DelVal_0001");
		String warningMessage1= I18nResourceUtil.getResourceItemValue("pfcommon","cef_exception.properties","Gsp_Cef_DelVal_0002");
		StringBuilder stringBuilder=new StringBuilder();
		List<String> resultWarningMessages=result.getWarningMessages();
		//
		if(resultWarningMessages==null){
			return warningMessage1;
		}
		for(String warningMessageTemp:resultWarningMessages){
			if(warningMessageTemp!=null){
				stringBuilder.append(warningMessageTemp);
			}
		}
		if(stringBuilder.toString().equals("")){
			return warningMessage;
		}else{
			return stringBuilder.toString();
		}
	}
}
