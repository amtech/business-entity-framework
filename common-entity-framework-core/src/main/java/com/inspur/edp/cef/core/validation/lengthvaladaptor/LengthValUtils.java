/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.core.validation.lengthvaladaptor;

import com.inspur.edp.cef.core.validation.requiredvaladaptor.RequiredStringValAdaptor;
import com.inspur.edp.cef.spi.entity.info.CefDataTypeInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.AssocationPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.ObjectType;
import com.inspur.edp.cef.spi.validation.IRuntimeValidationAssembler;

public class LengthValUtils {
  public static void addLengthVals(
      IRuntimeValidationAssembler validationAssembler, CefDataTypeInfo dataTypeInfo)
  {
    addLengthVals(validationAssembler,dataTypeInfo,0);
  }

  public static void addLengthVals(
      IRuntimeValidationAssembler validationAssembler, CefDataTypeInfo dataTypeInfo,int cefVersion)
  {
    int index =0;
    for(DataTypePropertyInfo propertyInfo:dataTypeInfo.getPropertyInfos().values())
    {
      if(propertyInfo.getObjectType()== ObjectType.Association)
      {
        validationAssembler.getValidations().add(index++,new newAssociationLengthValAdaptor(propertyInfo.getPropertyName(),propertyInfo.getLength(),propertyInfo.getPrecision(),cefVersion));
        continue;
      }
      if(propertyInfo.getObjectType() == ObjectType.Normal) {
        switch (propertyInfo.getFieldType()) {
          case String:
          case Text:
            validationAssembler.getValidations().add(index++,
                new StringLengthValAdaptor(propertyInfo.getPropertyName(), propertyInfo.getLength(),
                    propertyInfo.getPrecision(),cefVersion));
            break;
          case Decimal:
            validationAssembler.getValidations().add(index++,
                new DecimalLengthValAdaptor(propertyInfo.getPropertyName(),
                    propertyInfo.getLength(), propertyInfo.getPrecision(),cefVersion));
            break;
        }
      }
    }
  }
}
