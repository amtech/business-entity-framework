/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.core.validation.requiredvaladaptor;

import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.spi.validation.IEntityRTValidationAssembler;
import com.inspur.edp.cef.spi.validation.IValidation;

public class EntityValidationExecutor
{
	private IEntityRTValidationAssembler privateAssembler;
	protected final IEntityRTValidationAssembler getAssembler()
	{
		return privateAssembler;
	}

	private ICefEntityContext privateEntityContext;
	protected final ICefEntityContext getEntityContext()
	{
		return privateEntityContext;
	}

	public EntityValidationExecutor(IEntityRTValidationAssembler assembler, ICefEntityContext entityCtx)
	{
		DataValidator.checkForNullReference(assembler, "assembler");
		DataValidator.checkForNullReference(entityCtx, "entityCtx");

		privateAssembler = assembler;
		privateEntityContext = entityCtx;
	}

	private IChangeDetail privateChange;
	protected final IChangeDetail getChange()
	{
		return privateChange;
	}
	protected final void setChange(IChangeDetail value)
	{
		privateChange = value;
	}

	private ICefValidationContext privateContext;
	protected final ICefValidationContext getContext()
	{
		return privateContext;
	}
	protected final void setContext(ICefValidationContext value)
	{
		privateContext = value;
	}

	public final void execute()
	{
		setChange(getChangeset());
		if (getChange() == null)
		{
			return;
		}
		setContext(GetContext());

		executeBelongings();

		executeSelf();

		executeChilds();
	}

	private void executeBelongings()
	{
		java.util.List<IValidation> items = getAssembler().getBelongingValidations();
		if (items == null || items.isEmpty())
		{
			return;
		}

		executeCore(getChange(), items);
	}

	private void executeSelf()
	{
		java.util.List<IValidation> items = getAssembler().getValidations();
		if (items == null || items.isEmpty())
		{
			return;
		}

		executeCore(getChange(), items);
	}

	private void executeChilds()
	{
		java.util.List<IValidation> items = getAssembler().getChildAssemblers();
		if (items == null || items.isEmpty())
		{
			return;
		}

		executeCore(getChange(), items);
	}

	private void executeCore(IChangeDetail change, Iterable<IValidation> items)
	{
		for (IValidation item : items)
		{
			if (item.canExecute(change))
			{
				item.execute(getContext(), change);
			}
		}
	}

	protected ICefValidationContext GetContext(){return  getAssembler().getValidationContext(getEntityContext());}

	protected IChangeDetail getChangeset()
	{
		IChangeDetail change =	getAssembler().getChangeset(getEntityContext());
		if(change==null)
			return null;
		//2020年06月20日新增类型变更集clone较慢, 去掉clone
		return change;
	}
}
