/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.be;

import com.inspur.edp.bef.api.parameter.clone.CloneParameter;
import com.inspur.edp.bef.api.parameter.retrieve.RespectiveRetrieveResult;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveChildResult;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.cef.api.action.EditResult;
import com.inspur.edp.cef.api.dataType.entity.ICefRootEntity;
import com.inspur.edp.cef.entity.entity.IEntityData;

import java.util.ArrayList;
import java.util.Date;

/** 业务实体接口 */
public interface IBusinessEntity extends ICefRootEntity, IBENodeEntity {
  /** 获取业务类型  */
  String getBEType();

  String getVersionControlPropertyName();
  /** 获取业务实体的上下文信息接口  */
  IBEContext getBEContext();

  /**
   * 以默认加锁的方式，检索当前实体数据
   *
   * @return 检索的返回结果
   */
  RespectiveRetrieveResult retrieve();

  EditResult edit();

  /**
   * 检索当前实体数据，可指定是否使用加锁方式
   *
   * @param para 检索参数
   * @return 检索的返回结果
   */
  RespectiveRetrieveResult retrieve(RetrieveParam para);

  /** 检索子表实体数据 */
  RespectiveRetrieveResult retrieveChild(
      java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, RetrieveParam para);

  /**
   *
   * @param nodeCodes
   * @param hierachyIdList
   * @param para
   * @return
   */
  RetrieveChildResult retrieveChild(
      java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, ArrayList<String> ids,RetrieveParam para);
  /**
   * 批量检索实体数据，可指定是否使用加锁方式
   *
   * @param para 检索参数
   */
  void retrieveWithScope(RetrieveParam para);

  /**
   * 实体数据批量加锁
   */
  void addLockWithScope();

  /**
   * 保存当前实体数据的变更集到数据库
   *
   * @return 执行是否成功
   */
  boolean save();

  /** 将实体数据的删除变更集提交到内存 */
  void delete();

  /**
   * 删除实体数据上指定ID的从(从)表数据，将删除变更集提交到内存。若需要将该删除变更集提交到数据库，需要在调用DeleteChild方法后，再调用Save方法。 1.
   * 删除从表数据时，nodeCodes包含从表实体编号，hierachyIdList包含主表数据的唯一标识，ids包含要删除的从表数据的唯一标识 2.
   * 删除从(从)表数据时，nodeCodes包含从从表所属的从表的实体编号、以及从从表的实体编号；hierachyIdList包含从从表数据所属的主表数据的唯一标识，
   * 以及所属的从表数据的唯一标识，ids包含要删除的从从表数据的唯一标识
   *
   * @param nodeCodes 要删除数据的从(从)表的实体编号
   * @param hierachyIdList 要删除数据的从(从)表的所属实体数据的唯一标识
   * @param ids 要删除的从(从)表数据的唯一标识集合
   */
  void deleteChild(
      java.util.List<String> nodeCodes,
      java.util.List<String> hierachyIdList,
      java.util.List<String> ids);

  /** 清除当前实体数据的变更集 */
  void clearChangeset();

  /** 清除当前实体数据的三级缓存 */
  void clearBuffer();

  /** 批量删除操作 */
  void deleteWithScope();

  // IRootAccessor CreateReadonlyAccessor(IEntityData entityDef);

  // IRootAccessor CreateReadonlyAccessorByHightVersion(IRootAccessor entityDef);

  // IRootAccessor CreateAccessor(IEntityData entityDef);

  /** 新增主表业务实体数据，实体数据中带有默认值 */
  void retrieveDefault();

  /** 新增主表业务实体数据，实体数据中带有默认值和传入的默认值 */
  void retrieveDefault(java.util.Map<String, Object> defaultValues);

  /**
   * 复制指定数据
   * @param cloneDataID
   * @param cloneParameter 复制参数
   */
  void clone(String cloneDataID, CloneParameter cloneParameter);

  void clone(String cloneDataID, String dataId, CloneParameter cloneParameter);

  /**
   * 新增业务实体的从(从)表数据 1. 新增从表数据时，nodeCodes包含从表的实体编号，hierachyIdList包含主表数据的唯一标识； 2.
   * 该方法不只支持新增从表数据，同样的也可以新增从从表数据。新增从从表数据时， nodeCodes中要包含从从表所属的从表的实体编号，以及从从表的实体编号；
   * hierachyIdList列表包括对应主表数据的唯一标识、子表数据的唯一标识。
   *
   * @param nodeCodes 要新增数据的从(从)表的实体编号
   * @param hierachyIdList 新增从(从)表数据的所属实体数据的唯一标识
   * @return 新增的从(从)表实体数据
   */
  IEntityData retrieveDefaultChild(
      java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList);

  /**
   * 新增业务实体的从(从)表数据 1. 新增从表数据时，nodeCodes包含从表的实体编号，hierachyIdList包含主表数据的唯一标识； 2.
   * 该方法不只支持新增从表数据，同样的也可以新增从从表数据。新增从从表数据时， nodeCodes中要包含从从表所属的从表的实体编号，以及从从表的实体编号；
   * hierachyIdList列表包括对应主表数据的唯一标识、子表数据的唯一标识。
   *
   * @param nodeCodes 要新增数据的从(从)表的实体编号
   * @param hierachyIdList 新增从(从)表数据的所属实体数据的唯一标识
   * @param dataID 从(从)表新增数据使用的Id
   * @return 新增的从(从)表实体数据
   */
  IEntityData retrieveDefaultChild(
      java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, String dataID);

  /**
   * 新增业务实体的从(从)表数据 1. 新增从表数据时，nodeCodes包含从表的实体编号，hierachyIdList包含主表数据的唯一标识； 2.
   * 该方法不只支持新增从表数据，同样的也可以新增从从表数据。新增从从表数据时， nodeCodes中要包含从从表所属的从表的实体编号，以及从从表的实体编号；
   * hierachyIdList列表包括对应主表数据的唯一标识、子表数据的唯一标识。
   *
   * @param nodeCodes 要新增数据的从(从)表的实体编号
   * @param hierachyIdList 新增从(从)表数据的所属实体数据的唯一标识
   * @param defaultValues 从(从)表新增数据使用的默认值, 其中不能包含主键
   * @return 新增的从(从)表实体数据
   */
  IEntityData retrieveDefaultChild(
      java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, java.util.Map<String, Object> defaultValues);

  /**
   * 创建一个新的实体对象
   *
   * @return
   */
  IEntityData createObjectData();

  /**
   * 创建子节点数据行
   *
   * @param childCode 子节点编号
   * @param dataID 行ID
   * @return 实体数据
   */

  IEntityData createChildData(String childCode, String dataID);

  Date getVersionControlPropValue();
  void setVersionControlPropValue(Date value);
}
