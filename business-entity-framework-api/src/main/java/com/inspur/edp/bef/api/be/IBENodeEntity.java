/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.be;

import com.inspur.edp.bef.api.action.assembler.IAbstractActionAssemblerFactory;
import com.inspur.edp.cef.api.dataType.entity.ICefEntity;
import com.inspur.edp.cef.entity.repository.DataSaveResult;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;

/**
 BE子节点接口
 
*/
public interface IBENodeEntity extends ICefEntity
{
	/** 
	 获取自定义动作组装器的工厂
	 
	 @return 
	*/
	IAbstractActionAssemblerFactory getAssemblerFactory();

	/** 
	 创建子节点BE实体数据
	 
	 @param code 子节点编号
	 @param id 实体数据的唯一标识
	 @return 
	*/
	IBENodeEntity getChildBEEntity(String code, String id);

	/** 
	 创建一个空的实体数据
	 
	 @param id 实体数据的唯一标识
	 @return 实体数据
	*/
	IEntityData createEntityData(String id);

	IEntityData createChildAccessor(String childCode, IEntityData data);



		//region 联动计算
	/** 
	 加载后联动计算, 主要用于给虚拟字段赋值
	*/
	void afterLoadingDeterminate();

	//取消联动计算, 场景如: 新增时使用了的编号取消时释放掉;
	void onCancelDeterminate();

	void beforeSaveDeterminate(IChangeDetail change);

	void afterSaveValidate(DataSaveResult saveResult);

	/**
	 * 解析性实体动作
	 * @param actionCode 动作编号
	 * @param pars 实体动作参数
	 * @return
	 */
	Object executeBizAction(String actionCode, Object... pars);
	//endregion
}
