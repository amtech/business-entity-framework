/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.parameter.retrieve;

import com.inspur.edp.cef.entity.entity.IEntityData;

public class RetrieveChildResult {
  private java.util.HashMap<String, IEntityData> dataDict;

  /**
   * 数据检索时获取到的实体数据字典，键为实体数据唯一标识，值为业务实体数据。
   */
  public final java.util.HashMap<String, IEntityData> getDatas() {
    if (dataDict == null) {
      dataDict = new java.util.LinkedHashMap<String, IEntityData>();
    }
    return dataDict;
  }

  public final void setDatas(java.util.HashMap<String, IEntityData> value) {
    dataDict = value;
  }

  /** 加锁是否成功 */
  private boolean privateLockFailed;

  public final boolean getLockFailed() {
    return privateLockFailed;
  }

  public final void setLockFailed(boolean value) {
    privateLockFailed = value;
  }
}
