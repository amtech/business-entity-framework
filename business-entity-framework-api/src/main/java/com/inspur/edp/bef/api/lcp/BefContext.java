/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.lcp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;

/**
 * bef的上下文
 */
public class BefContext implements Serializable {

//  private BefStatus befStatus;
  private AuthInfo authInfo;
  private HashMap<String, Object> parameter;
  /**
   * 获取当前Session的SessionId
   */
  private String privateCurrentSessionId;
  private String privateCurrentOperationType;

  /**
   * 是否启用最后修改人字段的修改
   */
  private static final String Key_EnableLastModifiedBy = "timeStampLastModifiedBy";

  public boolean isEnableTimeStampLastModifiedBy() {
    return parameter.containsKey(Key_EnableLastModifiedBy);

  }

  public void setEnableTimeStampLastModifiedBy(boolean value) {
    if(value)
      parameter.put(Key_EnableLastModifiedBy, "t");
    else
      parameter.remove(Key_EnableLastModifiedBy);
  }

  /**
   * bef的状态
   */
  @Deprecated
  public final BefStatus getBefStatus() {
    BefStatus befStatus = new BefStatus();
    befStatus.setIsBeforeSaving(getIsBeforeSaving());
    return befStatus;
  }

  private static final String Key_IsBeforeSaving = "isBeforeSaving";
  public final boolean getIsBeforeSaving() {
    return parameter.containsKey(Key_IsBeforeSaving);
  }

  public final void setIsBeforeSaving(boolean value) {
    if(value)
      parameter.put(Key_IsBeforeSaving, "t");
    else
      parameter.remove(Key_IsBeforeSaving);
  }

  public final String getCurrentSessionId() {
    return privateCurrentSessionId;
  }

  public final void setCurrentSessionId(String value) {
    privateCurrentSessionId = value;
  }

  /**
   * 参数字典，可以包含年度表信息等
   */
  public final HashMap<String, Object> getParams() {
    if (parameter == null) {
      parameter = new HashMap<>();
    }
    return parameter;
  }

  /**
   * 权限信息类
   */
  public final AuthInfo getAuthInfo() {
    if (authInfo == null) {
      authInfo = new AuthInfo();
    }
    return authInfo;
  }

  public final String getCurrentOperationType() {
    return privateCurrentOperationType;
  }

  public final void setCurrentOperationType(String value) {
    privateCurrentOperationType = value;
  }


  public final void setVariable(String varName, Object value) {
    getParams().put(varName, value);
  }

  public final boolean containsVariable(String varName) {
    return getParams().containsKey(varName);
  }

  public final Object getVariable(String varName) {
    return getParams().get(varName);
  }
}
