/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.repository;

import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.repository.IRootRepository;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;

public interface IEntityRepository extends IRootRepository {

  // List<IEntityData> Query(EntityFilter filter, List<AuthorityInfo> authorities);

  // List<IEntityData> Retrieve(List<string> dataIds);

  /**
   * 获取保存方法sql
   *
   * @param changeDetails 变更集
   * @return
   */
  SqlContext getSaveSqlContext(java.util.ArrayList<IChangeDetail> changeDetails);

  /**
   * 执行Sql
   *
   * @param context
   */
  void execSqlStatement(SqlContext context);

  /**
   * 获取Join表表名
   *
   * @param nodeCode 节点编号
   * @param columns 被引用字段集合
   * @param keyColumnName 被关联字段名称
   * @param keyDbColumnName 被关联字段数据库名称
   * @param tableAlias 表别名集合 其他Repository关联该Repository对应实体时，使用本方法获取表名与字段名，用于构造join部分的内容
   * @return
   */
  String getTableNameByColumns(
      String nodeCode,
      java.util.HashMap<String, String> columns,
      String keyColumnName,
      RefObject<String> keyDbColumnName,
      java.util.ArrayList<String> tableAlias);

  /**
   * 获取主节点组装器
   *
   * @return
   */
  IDacAssembler getMainAssembler();

  void mergeChildData(
      String nodeCode,
      java.util.ArrayList<IEntityData> results,
      java.util.HashMap<String, java.util.ArrayList<IEntityData>> childInfo);

  void mergeChildData(
      String nodeCode,
      java.util.HashMap<String, java.util.ArrayList<IEntityData>> parentResults,
      java.util.HashMap<String, java.util.ArrayList<IEntityData>> childInfo);

  void initContext(java.util.HashMap<String, Object> envVars);
}
