/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.event.save;

import com.inspur.edp.bef.api.be.BEInfo;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import io.iec.edp.caf.commons.event.CAFEventArgs;

/** Bef保存事件参数基类，承载了Bef保存事件能够提供的所有的参数（包括BEType，数据主键列表、变更集列表、功能SessionID）。 */
// TODO: 事件基类未提供
public abstract class SaveEventArgs extends CAFEventArgs {
  private BEInfo beInfo;
  private java.util.ArrayList<IChangeDetail> changes;
  private String funcSessionID;
  private java.util.ArrayList<String> dataIDs;

  protected SaveEventArgs(
          BEInfo beInfo,
          java.util.ArrayList<String> dataIDs,
          java.util.ArrayList<IChangeDetail> changes,
          String funcSessionID) {
    this.beInfo = beInfo;
    this.dataIDs = dataIDs;
    this.changes = changes;
    this.funcSessionID = funcSessionID;
  }

  protected SaveEventArgs()
  {}
  /** BE类型等信息 */
  public BEInfo getBeInfo() {
    return beInfo;
  }

  /** BE类型，用来区分是哪个实体（比如报销单、销售订单...） */
  public String getBeType() {
    return beInfo ==null? null:beInfo.getBEType();
  }
  /** 变更集，提供当前操作期间的数据变化。 */
  public java.util.ArrayList<IChangeDetail> getChanges() {
    return changes;
  }
  /** 数据ID，所有变化的数据主键列表。 */
  public java.util.ArrayList<String> getDataIDs() {
    return dataIDs;
  }
  /** 功能SessionID，通过它进行Lcp调用。 */
  public String getFuncSessionID() {
    return funcSessionID;
  }

  protected void copyToNewArgs(SaveEventArgs args)
  {
    args.beInfo= (BEInfo) beInfo.clone();
    args.dataIDs=dataIDs;
    args.changes=changes;
    args.funcSessionID=funcSessionID;
  }
}
