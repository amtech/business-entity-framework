/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.lcp;

import com.inspur.edp.bef.api.BefRtBeanUtil;
import com.inspur.edp.bef.api.repository.IBefRepositoryFactory;
import com.inspur.edp.cef.api.repository.IRootRepository;

public final class LcpFactoryManagerUtils {

  @Deprecated
  public static IBefRepositoryFactory getBefRepositoryFactory() {
    return BefRtBeanUtil.getBefRepositoryFactory();
  }

  public static IRootRepository createRepository(String configId){
    return getBefRepositoryFactory().createRepository(configId);
  }

  @Deprecated
  public static ILcpFactory getBefLcpFactory() {
    return BefRtBeanUtil.getLcpFactory();
  }

  public static <T extends IStandardLcp> T createLcp(String configId){
    return (T)getBefLcpFactory().createLcp(configId);
  }
}
