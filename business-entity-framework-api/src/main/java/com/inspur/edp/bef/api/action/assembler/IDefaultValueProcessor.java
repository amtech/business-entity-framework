/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.action.assembler;

import com.inspur.edp.bef.entity.entityData.defaultValue.IDefaultValue;
import com.inspur.edp.cef.entity.entity.IEntityData;

/** 实体类默认值处理器 */
public interface IDefaultValueProcessor {
  /**
   * 执行赋默认值
   *
   * @param entityData
   */
  void setValue(String code, IEntityData entityData);

  /**
   * 将target中包含但当前类不包含的默认值合并到当前类的默认值中
   *
   * @param target
   */
  void merge(IDefaultValueProcessor target);

  java.util.Map<String, java.util.Map<String, IDefaultValue>> getDefaultValues();

  INodeDefaultValueProcessor getNodeDefaultValueProcessor(String nodeCode);
}
