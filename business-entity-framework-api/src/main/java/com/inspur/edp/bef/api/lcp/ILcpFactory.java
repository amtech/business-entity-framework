/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.lcp;

import com.inspur.edp.commonmodel.api.ICMManager;

/**
 * Lcp工厂接口，提供了创建Lcp的几种方式。
 * <p>此接口中的方法大部分需要提供configId参数。下面简单介绍一下configId：
 * <ul><li>configId是一个BE的唯一标识，除一些非常早期版本的BE外，一般为元数据命名空间+元数据编号的形式，如com.inspur.gs.xxx.xxx；
 * <li>BE元数据作为一个元数据，本身也有一个UUID形式的元数据ID，这个ID也是唯一的，configId与元数据ID的主要区别在于configId更具可读性，
 * 此接口中也有一些成对的方法，功能一致但参数分别为元数据Id和configId；
 * <li>如果你需要创建一个不是你开发的BE的Lcp方法时，需要提供的configId或元数据Id应向BE的开发人员索要；
 * <li>如果你是BE的开发人员，你被询问BE的configId是什么时，BE设计器上提供了属性显示当前BE的configId，你可以从中复制出来发给对方。</ul>
 * <p>下面是此接口的一个常见用法：
 * <blockquote><pre>
 *     IStandardLcp lcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcp("xxx");
 *     lcp.retrieve("xxx");
 * </pre></blockquote>
 * <p>如果此类的方法报错，大部分原因都是因为所调用的BE未正确部署，详细的排查方法可以<a href="https://open.inspuronline.com/iGIX/#/document/mddoc/igix-2103%2Fdev-guide-beta%2FFAQ%2Fdefault.md">点击这里</a>查看
 * */
public interface ILcpFactory {
  /**
   * 根据配置Id和sessionId创建Lcp
   * @deprecated 应使用 {@link #createLcp(String)}或 {@link #createLcpByBEId(String)}
   *
   * @param configId BE的配置标识
   * @param sessionId 功能sessionId
   * @return 本地消费代理接口
   *
   * @see #createLcp(String)
   * @see #createLcpByBEId(String)
   */
  IStandardLcp createLcp(String configId, String sessionId);

  /**
   * 根据配置Id创建CMManager。
   * <p>
   * CMManager是Lcp的一个子集，其上仅包含序列化和反序列化相关方法。这些方法不依赖于BefSession，因此可以在无BefSession时调用。
   * @param configID
   * @return
   */
  ICMManager createCMManager(String configID);

  /**
   * 根据配置Id创建Lcp。
   * <p>创建Lcp时，必须存在BefSession，否则会引发编号为{@code GSP_ADP_BEF_2010}的{@link com.inspur.edp.bef.api.exceptions.BefEngineException}。
   * 创建BefSession可以使用{@link com.inspur.edp.bef.api.services.IBefSessionManager}。
   * <p>如果运行时报错：{@code 找不到configId为xxx对应的程序集和元数据...}，那么需要检查BE的生成物是否正确部署到环境中。
   * <p>如果报错信息中configId代表的BE元数据不是由你开发，那么联系BE的开发人员检查BE的生成物是否正确部署到环境中。
   *
   * @param configId BE的配置标识
   * @return 本地消费代理接口
   *
   * @see com.inspur.edp.bef.api.services.IBefSessionManager
   */
  IStandardLcp createLcp(String configId);

  /**
   * 根据元数据ID创建Lcp。
   * <p>
   * 功能与{@link #createLcp(String)}一致，仅参数不同。
   * 
   * @param beId BE元数据ID
   * @return 本地消费代理接口
   * 
   * @see #createLcp(String)
   */
  IStandardLcp createLcpByBEId(String beId);

  @Deprecated
  IStandardLcp createStatelessLcp(String configId);


  /**
   * 根据维度信息获取扩展BE的Lcp对象，如果指定的维度无扩展BE，则依次找其上层公共BE，如果无公共BE，则直接找基础BE。
   *@param baseBeMetadataId  基础BE元数据ID;
   * @param firstDimensionValue 第一纬度的值，如果为公有则传空；
   * @param secondDimensionValue 第二维度的值，如果为公有则传空。另：如果第一个维度的值为空时，第二个维度值必须为空。
   */
  IStandardLcp createExtendLcpById(String baseBeMetadataId,String firstDimensionValue, String secondDimensionValue);
}
