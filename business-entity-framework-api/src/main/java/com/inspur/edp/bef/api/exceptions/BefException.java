/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.exceptions;

import com.inspur.edp.bef.entity.exception.BefExceptionBase;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;

// bdf显示给用户的异常, message需要国际化
public class BefException extends BefExceptionBase {

  public BefException(
      String exceptionCode,
      String exceptionMessage,
      RuntimeException innerException,
      ExceptionLevel level) {
    super(exceptionCode, exceptionMessage, innerException, level, true);
  }


  public BefException(
      String exceptionCode,
      String resourceFile,
      RuntimeException innerException,
      String[] exceptionParams,
      ExceptionLevel level,
      boolean isBizException) {
    super(exceptionCode, resourceFile, innerException, exceptionParams, level, isBizException);
  }


  public BefException(
      String exceptionCode,
      String resourceFile,
      String messageCode,
      RuntimeException innerException,
      String[] exceptionParams,
      ExceptionLevel level,
      boolean isBizException) {
    super(
        exceptionCode,
        resourceFile,
        messageCode,
        innerException,
        exceptionParams,
        level,
        isBizException);
  }
}
