/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.exceptions;

import com.inspur.edp.cef.core.i18n.I18nResourceUtil;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import java.util.HashMap;
import java.util.List;
import org.omg.CosNaming._BindingIteratorImplBase;

public class DataVersionMismatchException extends CAFRuntimeException {

  private static final String msg = "此数据在功能打开后被他人更改过。你需要放弃更改并尝试关闭并重新打开功能后重新修改。";// TODO: 临时,将来改成资源文件
  public static final String ExtensionMessageKey_Ids = "ids";
  public static final String ExtensionMessageKey_BEType = "type";

  public DataVersionMismatchException(String beType, List<String> ids) {
    super("pfcommon",
        "cef_exception.properties",
        "Gsp_Bef_LockVal_0004",
        null,
        null,
        ExceptionLevel.Warning,
        true);
  }

  private static HashMap<String, String> buildExt(String beType, List<String> ids) {
    HashMap<String, String> rez = new HashMap<>(2, 1);
    rez.put(ExtensionMessageKey_BEType, beType);
    String value;
    if(ids != null && !ids.isEmpty()) {
      if(ids.size() > 50) {
        value = "共" + ids.size() + "项";
      } else {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (String id : ids) {
          sb.append(id).append(", ");
        }
        sb.setLength(sb.length() - 1);
        sb.append("]");
        value = sb.toString();
      }
    } else {
      value = "";
    }

    rez.put(ExtensionMessageKey_Ids, value);
    return rez;
  }
}
