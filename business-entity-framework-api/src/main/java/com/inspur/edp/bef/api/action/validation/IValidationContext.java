/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.action.validation;

import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBENodeEntity;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.api.lcp.BefContext;
import com.inspur.edp.bef.api.lcp.IStandardLcp;
import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.MessageLevel;
import com.inspur.edp.cef.spi.entity.resourceInfo.ModelResInfo;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.variable.api.data.IVariableData;
import java.util.List;

/** 校验规则上下文接口，用于在校验规则构件代码中获取当前数据行等信息
 * <p>需要注意，校验规则中只能进行数据校验，不可进行数据修改。如需修改数据请在联动计算中进行。
 * <p>此接口与{@link com.inspur.edp.bef.api.action.determination.IDeterminationContext}完全一致，
 * 相关说明可参考{@code IDeterminationContext}。
 *
 * @see com.inspur.edp.bef.api.action.determination.IDeterminationContext
 * */
public interface IValidationContext extends ICefValidationContext {

  /** @see IDeterminationContext#getBENodeEntity() */
  IBENodeEntity getBENodeEntity();

  IBEManagerContext getBEManagerContext();

  /** @see IDeterminationContext#getBEContext() */
  IBENodeEntityContext getBEContext();

  /** @see IDeterminationContext#getRootChange() */
  @Deprecated
  IChangeDetail getRootChange();

  /** bef的上下文 */
  BefContext getBefContext();

  /** @see IDeterminationContext#getCurrentData() */
  IEntityData getCurrentData();

  /** @see IDeterminationContext#getOriginalData() */
  IEntityData getOriginalData();

  /** @see IDeterminationContext#getNodeCode() */
  String getNodeCode();

  /** @see IDeterminationContext#setNodeCode(String) */
  void setNodeCode(String value);

  /** @see IDeterminationContext#addMessage(IBizMessage) */
  void addMessage(IBizMessage msg);

  /** @see IDeterminationContext#createMessageWithLocation(MessageLevel, String, String...) () */
  IBizMessage createMessageWithLocation(MessageLevel level, String msg, String... msgPars);

  /** @see IDeterminationContext#createMessageWithLocation(MessageLevel, List, String, String...)  */
  IBizMessage createMessageWithLocation(
      MessageLevel level, java.util.List<String> columnNames, String msg, String... msgPars);

  /** @see IDeterminationContext#getLcp(String) */
  IStandardLcp getLcp(String config);

  /** @see IDeterminationContext#getVariables() */
  IVariableData getVariables();


  //region i18n
  /** @see IDeterminationContext#getModelResInfo() */
  ModelResInfo getModelResInfo();

  /** @see IDeterminationContext#getUniqueConstraintMessage(String) */
  String getUniqueConstraintMessage(String conCode);

  //endregion
}
