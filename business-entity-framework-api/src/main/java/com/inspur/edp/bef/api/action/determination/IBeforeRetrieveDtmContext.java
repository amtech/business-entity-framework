/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.action.determination;

import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.cef.entity.condition.EntityFilter;

import java.util.List;
import java.util.Map;

/**
 * 数据检索前联动计算上下文。
 * <p>检索前联动计算并非每次调用检索都会执行。对一条数据，第一次检索时，会先执行检索前联动再从持久化中取数，
 * 同一BefSession下再次对此数据执行检索将不再执行检索前联动。
 * <p>使用此上下文可以对数据检索所使用的参数{@link #getRetrieveParam()}进行控制。在数据检索前联动计算中设置的检索参数具有最高优先级，将会覆盖VO等其他途径设置的检索参数。
 * <p>注意，非业务逻辑中的固有检索参数，不应通过检索前联动计算实现。例如不同界面所需的不同参数，应在VO层处理。
 * <p>此类继承自{@link IDeterminationContext}，但只是结构上继承，其{@link IDeterminationContext#getCurrentData()}等方法不允许调用，
 * 这非常容易理解，因为检索前必然不存在数据。
 */
public interface IBeforeRetrieveDtmContext extends IDeterminationContext {

    /** 获取检索参数，可以通过设置其上属性来控制检索行为。
     *
     * @see RetrieveParam
     */
    RetrieveParam getRetrieveParam();

    Map<String, EntityFilter> getNodeFilters();

    /** 当前正在检索的数据主键值的列表 */
    List<String> getDataIds();

    //region i18n
    /**
     * 翻译后的实体名称
     *
     * @return
     */
    String getEntityI18nName(String nodeCode);
    /**
     * 翻译后的属性名称
     *
     * @param labelID
     * @return
     */
    String getPropertyI18nName(String nodeCode, String labelID);
    /**
     * 翻译后的关联带出字段名称
     *
     * @param labelID
     * @param refLabelID
     * @return
     */
    String getRefPropertyI18nName(String nodeCode, String labelID, String refLabelID);
    /**
     * 翻译后的枚举显示值
     *
     * @param labelID
     * @param enumKey
     * @return
     */
    String getEnumValueI18nDisplayName(String nodeCode, String labelID, String enumKey);
    /**
     * 翻译后的唯一性约束提示信息
     *
     * @param conCode
     * @return
     */
    String getUniqueConstraintMessage(String nodeCode, String conCode);

    //endregion
}
