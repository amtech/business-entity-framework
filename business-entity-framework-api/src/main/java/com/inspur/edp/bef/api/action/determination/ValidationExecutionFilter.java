/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.api.action.determination;

import com.inspur.edp.cef.entity.changeset.ChangeType;

/** 校验规则执行过滤器 */

public final class ValidationExecutionFilter {
  /** 获取或设置变更集类型  */
  private ChangeType privateChangeType;

  public ChangeType getChangeType() {
    return privateChangeType;
  }

  public void setChangeType(ChangeType value) {
    privateChangeType = value;
  }

  // public string[] Elements { get; set; }

  //// NodeCode(只能是直接下级表), nodeCode下的字段列表
  // public IDictionary<string, string[]> ChildElements { get; set; }
}
