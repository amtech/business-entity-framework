/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.udt.spi.Validation;

import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.validation.AbstractValidationAction;
import com.inspur.edp.udt.api.Validation.IValidationContext;

public abstract class AbstractValidation extends AbstractValidationAction
{
//C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing via the 'new' keyword:
//ORIGINAL LINE: public new IValidationContext Context => (IValidationContext)super.Context;
	public IValidationContext getUdtValContext()
	{return (IValidationContext)super.getContext();}

	/** 
	 新实例初始化AbstractValidation具有给定的校验规则上下文，以及实体数据变更集
	 
	 @param context 校验规则上下文
	*/
	protected AbstractValidation(IValidationContext context, IChangeDetail change)
	{
		super(context, change);
	}
}
