/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.temptable;

import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.action.assembler.IMgrActionAssemblerFactory;
import com.inspur.edp.bef.api.lcp.BefTempTableContext;
import com.inspur.edp.bef.core.be.BEManagerContext;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import io.iec.edp.caf.databaseobject.api.entity.TempTableContext;
import java.util.Map;

public class DropTempTableMgrAction extends AbstractManagerAction<VoidActionResult> {

  private BefTempTableContext tempTableContext;

  public DropTempTableMgrAction(BefTempTableContext tempTableContext) {
    this.tempTableContext = tempTableContext;
    super.isReadOnly = true;
  }

  @Override
  public void execute() {
    for(Map.Entry<String, TempTableContext> entry:tempTableContext.getDboContexts().entrySet())
    {
      getBEManagerContext().getBEManager().getRepository().dropTempTable(entry.getKey(),entry.getValue());
    }
  }
}
