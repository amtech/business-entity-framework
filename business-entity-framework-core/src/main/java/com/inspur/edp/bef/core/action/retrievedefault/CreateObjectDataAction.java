/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrievedefault;

import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.spi.action.AbstractAction;
import com.inspur.edp.cef.entity.entity.IEntityData;

public class CreateObjectDataAction extends AbstractAction<IEntityData> {
  public CreateObjectDataAction(IBEContext beContext) {
    super(beContext);
  }

  @Override
  public void execute() {
    IEntityData data = ActionUtil.getRootEntity(this).createEntityData(this.getBEContext().getID());
    setResult(
        (IEntityData) ActionUtil.getBEContext(this).getAccessorCreator().createAccessor(data));
  }
}
