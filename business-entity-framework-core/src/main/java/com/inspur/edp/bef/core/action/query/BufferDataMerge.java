/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.query;

import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.entity.IEntityData;
import java.util.List;
import java.util.Optional;

// 主表
public class BufferDataMerge {
  private EntityFilter filter;
  private IBEManagerContext mgrContext;
  private java.util.List<IEntityData> dataList;

  public BufferDataMerge(
      EntityFilter filter, IBEManagerContext mgrContext, java.util.List<IEntityData> dataList) {
    this.filter = filter;
    this.mgrContext = mgrContext;
    this.dataList = dataList;
  }

  public final void merge() {
List<IBusinessEntity> entities = mgrContext.getAllEntities();
    if (entities == null || entities.size() == 0) {
      return;
    }

    PaginationDataMerge beMerger = new PaginationDataMerge(filter, dataList);
for (IBusinessEntity item : entities) {
      IChangeDetail change = beMerger.getOwnChange(item);
      if (change == null) {
        continue;
      }
      beMerger.merge(
          change.getChangeType() == ChangeType.Deleted
              ? item.getBEContext().getOriginalData()
              : item.getBEContext().getCurrentData(),
          Optional.of(change.getChangeType()));
    }
  }
}
