/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.determination.basetypes.childtrans;

import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.api.be.IBENodeEntity;
import com.inspur.edp.bef.spi.action.determination.IDeterminationAdaptor;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.Util;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IKey;
import com.inspur.edp.cef.spi.determination.IDetermination;
import java.util.Iterator;

public abstract class BefChildTransDtmAdapter<TIterator> implements IDetermination {

  private final String name;
  private final String childCode;
  private final ChildTransIterator<TIterator> iteratorContainer;

  protected BefChildTransDtmAdapter(String name, String childCode, ChildTransIterator<TIterator> iterator) {
    this.name = name;
    this.childCode = childCode;
    this.iteratorContainer = iterator;
  }

  @Override
  public String getName() {
    return name;
  }

  protected String getChildCode() {
    return childCode;
  }

  @Override
  public abstract boolean canExecute(IChangeDetail iChangeDetail);

  @Override
  public final void execute(ICefDeterminationContext dtmContext, IChangeDetail change) {
    com.inspur.edp.bef.api.action.determination.IDeterminationContext beCtx = (com.inspur.edp.bef.api.action.determination.IDeterminationContext) dtmContext;
    Iterator<TIterator> iterator = iteratorContainer.getIterator(beCtx, change, getChildCode());
    while(iterator.hasNext()){
      TIterator item = iterator.next();
      IBENodeEntity itemEntity = beCtx.getBENodeEntity()
          .getChildBEEntity(childCode, ((IKey) item).getID());
      doExecute(itemEntity, item);
    }
  }

  protected abstract void doExecute(IBENodeEntity itemEntity, TIterator item);

  protected static final ChildTransIterator<IChangeDetail> ChildChangeTransIterator = new ChildChangeTransIterator();

  protected static final ChildTransIterator<IEntityData> ChildDataTransIterator = new ChildDataTransIterator();

  protected static final ChildTransIterator<IChangeDetail> ChildChangeTransWithDeletedIterator = new ChildChangeWithDeletedTransIterator();

  private static class ChildChangeTransIterator implements ChildTransIterator<IChangeDetail> {

    @Override
    public Iterator<IChangeDetail> getIterator(IDeterminationContext dtmCtx,
        IChangeDetail change, String childCode) {
      return Util.getChildChanges(change, childCode).iterator();
    }
  }

  private static class ChildChangeWithDeletedTransIterator implements ChildTransIterator<IChangeDetail> {

    @Override
    public Iterator<IChangeDetail> getIterator(IDeterminationContext dtmCtx,
        IChangeDetail change, String childCode) {
      return com.inspur.edp.bef.api.changeset.Util.getChildChanges(change, childCode, dtmCtx)
          .iterator();
    }
  }

  private static class ChildDataTransIterator implements ChildTransIterator<IEntityData> {

    @Override
    public Iterator<IEntityData> getIterator(IDeterminationContext dtmCtx,
        IChangeDetail change, String childCode) {
      return ((IEntityData) dtmCtx.getData()).getChilds().get(childCode).iterator();
    }
  }

  private interface ChildTransIterator<T> {

    Iterator<T> getIterator(IDeterminationContext dtmCtx, IChangeDetail change, String childCode);
  }
}
