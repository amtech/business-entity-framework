/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.runtimeconfig;

import com.inspur.edp.bef.api.befruntimeconfig.RuntimeConService;
import com.inspur.edp.bef.entity.befruntimeconfig.GspBefRuntimeConfig;
import io.iec.edp.caf.tenancy.api.context.MultiTenantContextInfo;
import io.iec.edp.caf.tenancy.core.context.MultiTenantContextHolder;

import java.util.concurrent.ConcurrentHashMap;

public class RuntimeConfigServiceImpl implements RuntimeConService {

    private static final ConcurrentHashMap<String, GspBefRuntimeConfig> configInfo = new ConcurrentHashMap<>();
    private final RuntimeConfigRepository repository;

    public RuntimeConfigServiceImpl(RuntimeConfigRepository repository) {
        this.repository = repository;
    }

    @Override
    public GspBefRuntimeConfig getRuntimeConfigById(String id){

        if(id == null || "".equals(id))
            return null;
        if (!configInfo.containsKey(id)) {
            GspBefRuntimeConfig config = repository.findById(id).orElse(null);
            if (config == null)
                return null;
            configInfo.put(id, config);
        }
        return configInfo.get(id);

    }

    @Override
    public void saveRuntimeConfig(GspBefRuntimeConfig config){
        repository.save(config);

    }

    @Override
    public void deleteRuntimeConfigById(String id){
        if (id == null || "".equals(id))
            return;
        if (configInfo.containsKey(id)) {
            configInfo.remove(id);
        }
        GspBefRuntimeConfig config = repository.findById(id).orElse(null);
        if (config != null)
            repository.deleteById(id);
    }

    @Override
    public boolean isUseIgnoreLock(){
        GspBefRuntimeConfig config;
        try {
            config = this.getRuntimeConfigById("lock-ignore-config");
        }catch (Exception e){
            config = new GspBefRuntimeConfig();
            config.setId("lock-ignore-config");
            config.setRuntimeConfig("false");
            configInfo.put("lock-ignore-config",config);
            return false;
        }
        if(config != null) {
            return config.getRuntimeConfig().equals("true");
        }else {
            config = new GspBefRuntimeConfig();
            config.setId("lock-ignore-config");
            config.setRuntimeConfig("false");
            configInfo.put("lock-ignore-config",config);
            return false;
        }
    }

    private void connectMasterDb(){
        MultiTenantContextInfo contextInfo = new MultiTenantContextInfo();
        contextInfo.setMasterDb(true);
        MultiTenantContextHolder.set(contextInfo);
    }
}
