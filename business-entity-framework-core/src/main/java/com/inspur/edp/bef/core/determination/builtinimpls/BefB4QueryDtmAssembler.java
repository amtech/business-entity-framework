/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.determination.builtinimpls;

import com.inspur.edp.bef.core.determination.AbstractB4QueryDtmAssembler;
import com.inspur.edp.cef.spi.determination.IDetermination;
import java.util.ArrayList;
import java.util.List;

public class BefB4QueryDtmAssembler extends AbstractB4QueryDtmAssembler {

  private List<IDetermination> determinations;
  @Override
  public List<IDetermination> getDeterminations() {
    if(determinations==null)
      determinations=new ArrayList<>();
    return determinations;
  }

  public void addDetermination(IDetermination determination)
  {getDeterminations().add(determination);
  }
}
