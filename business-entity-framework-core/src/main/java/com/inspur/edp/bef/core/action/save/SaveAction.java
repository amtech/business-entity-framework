/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.save;


import com.inspur.edp.bef.api.action.assembler.IBEActionAssembler;
import com.inspur.edp.bef.core.action.base.ActionFactory;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.be.BENodeEntity;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.spi.action.AbstractAction;
import com.inspur.edp.bef.spi.action.IIgnoreDetermination;
import com.inspur.edp.bef.spi.entity.CodeRuleInfo;
import com.inspur.edp.cef.api.message.MessageLevel;
import com.inspur.edp.cef.entity.changeset.ChangeType;

public class SaveAction extends AbstractAction<Boolean> implements IIgnoreDetermination
{
	@Override
	public void execute()
	{
//		ActionFactory actionFac = new ActionFactory();
//		ActionUtil.getRootEntity(this).beforeSaveDeterminate();
//		AssignCode(ActionUtil.getRootEntity(this));
//		//ActionUtil.getRootEntity(this).executeAction(actionFac.getBeforeCheckDtmAction(
//		//    new CoreDeterminContext(Context)));
//		//TODO: zj生成编号的时机扩展性实现一下,BEContext上生成一个时机属性进行判断?讨论
//		//TODO: zj aftermodify时机的dtm不能选择有保存时编号规则的字段;
//		//actionFac.getAdjustNumberAction(new AdjustNumberActionContext(Context));//保存时生成编号
//		ActionUtil.getRootEntity(this).beforeSaveValidate();
//		Result = ActionUtil.getRootEntity(this).BEContext.Response.MessageCollector.getMaxLevel() < MessageLevel.Error;
//		if (!Result)
//		{
//			return;
//		}
//		SaveWithScope();
//		//var validationAction = actionFac.getValidationAction(Context);//Validation
//		//Result = ActionUtil.getRootEntity(this).executeAction(validationAction);
//		//if (!Result)
//		//    return;
//		//SaveWithScope();
	}

//	@Override
//	protected IBEActionAssembler getAssembler()
//	{
//		return GetAssemblerFactory().getSaveActionAssembler(CoreBEContext);
//	}
//
//	private void saveWithScope()
//	{
//		CoreBEContext.addScopeParameter(new SaveScopeNodeParameter(CoreBEContext));
//	}
//
//	private void assignCode(BENodeEntity be)
//	{
//		if (Context.TransactionalChange == null)
//		{
//			return;
//		}
//		if (!(Context.TransactionalChange.getChangeType() == ChangeType.Added))
//		{
//			return;
//		}
//		var data = CoreBEContext.CurrentData;
//		java.util.HashMap<String, CodeRuleInfo> codeInfo = be.getBeforeSaveCodeRule();
//		if (codeInfo == null)
//		{
//			return;
//		}
//		for (var key : codeInfo.keySet())
//		{
//			var currentCodeValue = data.getValue(key);
//			if (currentCodeValue != null && currentCodeValue instanceof String && ! (String)((currentCodeValue instanceof String) ? currentCodeValue : null).equals(""))
//			{
//				continue;
//			}
//			String code = GetCode(codeInfo[key].CodeRuleId);
//			data.setValue(key, code);
//		}
//	}
//
//	private String getCode(String ruleId)
//	{
//		ICodeRuleService service = ServiceManager.<ICodeRuleService>GetService();
//		return service.generate(ruleId, null);
//	}
}
