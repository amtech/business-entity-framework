/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.determination;

import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.core.determination.EntityDeterminationExecutor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

public class BefB4SaveAdditionalDtmExecutor extends EntityDeterminationExecutor
{

		//region Ctor
	public BefB4SaveAdditionalDtmExecutor(IEntityRTDtmAssembler assembler, IBENodeEntityContext entityCtx)
	{
		super(assembler, entityCtx);
	}

		//endregion


		//region Execute
	private IBENodeEntityContext innerGetEntityContext(){return (IBENodeEntityContext)super.getEntityContext();}
	@Override
	public void execute() {
		setChange(getChangeset());
		if (getChange() == null) {
			return;
		}

		setContext(GetContext());
		getRootContext().acceptTempChange();
		executeBelongingDeterminations();
		executeDeterminations();
		executeChildDeterminations();
		getRootContext().mergeIntoInnerChange();
		getRootContext().acceptListenerChange();
	}

	private CoreBEContext rootContext;
	private CoreBEContext getRootContext() {
		if (rootContext == null) {
			IBENodeEntityContext current = innerGetEntityContext();
			while (!(current instanceof CoreBEContext) && current != null) {
				current = current.getParentContext();
			}
			rootContext = (CoreBEContext)current;
		}
		return rootContext;
	}

	@Override
	protected IChangeDetail getChangeset()
	{
		IChangeDetail tempChange =  innerGetEntityContext().getCurrentTemplateChange();
		if(tempChange == null){
			return null;
		}
		return tempChange.clone();
	}

	@Override
	protected ICefDeterminationContext GetContext() {
		return new BeforeSaveDtmContext((IBENodeEntityContext) getEntityContext());
	}
		//endregion
}
