//package com.inspur.edp.bef.core.action.save;
//
//import com.inspur.edp.cef.spi.scope.AbstractCefScopeExtension;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Scope;
//import org.springframework.context.annotation.ScopedProxyMode;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//@EnableTransactionManagement
//@Configuration
//public class BefSaveActionServiceAutoConfiguration {
//  @Bean("PlatformCommon_Bef_SaveScopeNodeParameter")
//  @Scope(value="prototype")//, proxyMode= ScopedProxyMode.TARGET_CLASS
//  public AbstractCefScopeExtension getSaveExt() {
//    return new SaveWithScopeExtension();
//  }
//}
