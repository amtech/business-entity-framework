/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.determination;

import com.inspur.edp.bef.api.action.determination.DtmExecutionHistory;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.core.determination.EntityDeterminationExecutor;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

public class RootEntityAfterModifyDtmExecutor extends EntityDeterminationExecutor
{
	private DtmExecutionHistory execHistory;
	public RootEntityAfterModifyDtmExecutor(IEntityRTDtmAssembler assembler, ICefEntityContext entityCtx)
	{
		super(assembler, entityCtx);
		execHistory = new DtmExecutionHistory();
	}

	@Override
	public void execute()
	{
		CoreBEContext beCtx = (CoreBEContext) getEntityContext();
		setContext(GetContext());
		setChange(beCtx.getCurrentTemplateChange());
		while (getChange() != null)
		{
			execHistory.beginGroup("AfterModify");

			executeBelongingDeterminations();
			executeDeterminations();
			executeChildDeterminations();

			beCtx.acceptTempChange();
			setChange(beCtx.getListenerChange());
			beCtx.mergeIntoInnerChange();
			beCtx.acceptListenerChange();
		}
	}
///#endregion
}
//    internal class RootAfterModifyDtmExecutor : EntityDeterminationExecutor
//    {
//        private readonly ICoreBEContext rootEntityContext;
//        private readonly DtmExecutorContext executorContext;

//        protected IChangeDetail changeset;

//        public RootAfterModifyDtmExecutor(IEntityRTDtmAssembler assembler, ICoreBEContext rootEntityContext) 
//            : base(assembler, rootEntityContext)
//        {
//            this.rootEntityContext = rootEntityContext;
//            executorContext = new DtmExecutorContext(rootEntityContext);
//        }

//        //public AfterModifyDtmExecutor(IRuntimeDtmAssembler ass, IBENodeEntityContext context)
//        //    : base(ass, context)
//        //{
//        //    this.ass = ass;
//        //    this.context = context;
//        //}

//        public override void Execute()
//        {
//            rootEntityContext.AcceptListenerChange();
//            changeset = rootEntityContext.CurrentTemplateChange;
//            while (changeset != null)
//            {
//                executorContext.History.BeginGroup("AfterModify");
//                //rootEntityContext.AcceptTempChange();
//                base.Execute();
//                rootEntityContext.AcceptListenerChange();
//                changeset = rootEntityContext.CurrentTemplateChange;
//            }
//            //do
//            //{
//            //    executorContext.History.BeginGroup("AfterModify");
//            //    rootEntityContext.AcceptListenerChange();
//            //    changeset = rootEntityContext.CurrentTemplateChange;
//            //    rootEntityContext.AcceptTempChange();
//            //    base.Execute();
//            //} while (changeset != null);
//        }

//        protected override IChangeDetail GetChangeset()
//        {
//            return changeset;
//        }
//    }
