//package com.inspur.edp.bef.core.action;
//
//import com.inspur.edp.bef.core.action.retrieve.AddLockExtension;
//import com.inspur.edp.bef.core.action.retrieve.CheckVersionExtension;
//import com.inspur.edp.bef.core.action.retrieve.RetrieveExtension;
//import com.inspur.edp.bef.core.action.save.SaveWithScopeExtension;
//import com.inspur.edp.cef.spi.scope.AbstractCefScopeExtension;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Scope;
//
//@Configuration
//public class BefExtendServiceAutoConfiguration {
//  @Bean("PlatformCommon_Bef_CheckVersionScopeNodeParameter")
//  @Scope(value="prototype")
//  public AbstractCefScopeExtension getCheckVersionExtension() {
//    return new CheckVersionExtension();
//  }
//
//  @Bean("PlatformCommon_Bef_AddLockScopeNodeParameter")
//  @Scope(value="prototype")
//  public AbstractCefScopeExtension getAddLockExtension() {
//    return new AddLockExtension();
//  }
//
//  @Bean("PlatformCommon_Bef_RetrieveScopeNodeParameter")
//  @Scope(value="prototype")
//  public AbstractCefScopeExtension getRetrieveExtension() {
//    return new RetrieveExtension();
//  }
//}
