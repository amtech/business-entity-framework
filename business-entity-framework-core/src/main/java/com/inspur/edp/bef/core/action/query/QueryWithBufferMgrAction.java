/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.query;

import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.core.DotNetToJavaStringHelper;
import com.inspur.edp.bef.core.be.BEManagerContext;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.entity.IEntityData;
import java.util.List;

public class QueryWithBufferMgrAction extends QueryManagerAction
{
	public QueryWithBufferMgrAction(BEManagerContext managerContext, String nodeCode, EntityFilter filter)
	{
		super(managerContext, nodeCode, filter);
	}

	@Override
	public void execute()
	{
		super.execute();
		mergeFromBuffer();
	}

	private void mergeFromBuffer()
	{
List<IBusinessEntity> cachedData = getBEManagerContext().getAllEntities();
		if (cachedData == null || cachedData.size() == 0)
		{
			return;
		}

		if (getResult() == null)
		{
			setResult(new java.util.ArrayList<IEntityData>());
		}

		if (DotNetToJavaStringHelper.isNullOrEmpty(nodeCode))
		{
			new BufferDataMerge(getFinalFilter(), getBEManagerContext(), getResult()).merge();
		}
		else
		{
			new ChildBufferDataMerge(getFinalFilter(), nodeCode, getBEManagerContext(), getResult()).merge();
		}
	}
}
