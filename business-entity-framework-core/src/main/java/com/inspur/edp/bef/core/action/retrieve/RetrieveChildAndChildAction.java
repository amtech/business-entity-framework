/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrieve;

import com.inspur.edp.bef.api.action.assembler.IBEActionAssembler;
import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.parameter.retrieve.RespectiveRetrieveResult;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveChildResult;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveResult;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.spi.action.AbstractAction;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import java.util.ArrayList;
import java.util.HashMap;

public class RetrieveChildAndChildAction extends AbstractAction<RetrieveChildResult> {

  public RetrieveChildAndChildAction(java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, ArrayList<String> ids,RetrieveParam par)
  {
    this.nodeCodes = nodeCodes;
    this.hierachyIdList = hierachyIdList;
    this.ids=ids;
    this.par = par;
  }

  //endregion

  private java.util.List<String> nodeCodes;
  private java.util.List<String> hierachyIdList;
  private ArrayList<String> ids;
  private RetrieveParam par;

  @Override
  public void execute()
  {
    if(ids==null){
      throw new RuntimeException("检索参数id列表不能为空，请重新输入！");
    }
    AuthorityUtil.checkDataAuthority((IBEContext) this.getBEContext());

    CoreBEContext beCtx = ActionUtil.getBEContext(this);
    IEntityData rootData = RetrieveUtils.getData(beCtx, par.getCacheType());
    IEntityDataCollection childCollection = rootData != null ? rootData.getChilds().get(nodeCodes.get(0)) : null;
    for (int i = 1; i < nodeCodes.size(); i++)
    {
      IEntityData data = childCollection.tryGet(hierachyIdList.get(i));
      childCollection = data != null? data.getChilds().get(nodeCodes.get(i)): null;
      if (childCollection == null) {
        break;
      }
    }

    RetrieveChildResult tempVar = new RetrieveChildResult();
    HashMap<String,IEntityData> map=new HashMap<>();
    if(childCollection != null) {

      for(String item:ids){
        if(childCollection.tryGet(item)==null){
          throw  new RuntimeException("该"+item+"对应的数据不存在！");
        }
        map.put(item,childCollection.tryGet(item));
        tempVar.setDatas(map);
      }
    }

      tempVar.setLockFailed(par.getNeedLock() && !beCtx.isLocked());
      setResult(tempVar);
  }

@Override
  protected IBEActionAssembler getAssembler() { return null; }
}
