/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.be;

import com.inspur.edp.bef.api.be.IBEManager;
import com.inspur.edp.cef.api.dataType.entity.ICefRootEntity;
import com.inspur.edp.cef.core.rootmanager.BaseRootEntityCacheManager;
import java.util.HashMap;

public final class BizEntityCacheManager extends BaseRootEntityCacheManager {
  private java.util.HashMap<String, ICefRootEntity> dict;

  public BizEntityCacheManager(IBEManager rootMgr, java.util.HashMap<String, ICefRootEntity> dict) {
    super(rootMgr.getBEType(), rootMgr, null);
    this.dict = dict;
  }

  public IBEManager getBEManager() {
    return (IBEManager) rootMgr;
  }

  public void setBEManager(IBEManager value) {
    if (rootMgr != null) {
      throw new RuntimeException();
    }
    rootMgr = value;
  }

  @Override
  protected HashMap<String, ICefRootEntity> getDicBE() {
    return dict;
  }

  // TODO: 兼容be变更, 1906 response挪到sessionItem后去掉
  public java.util.HashSet<String> getDataIds() {
    return getDicBE() == null
        ? new java.util.HashSet<String>()
        : new java.util.HashSet<String>(getDicBE().keySet());
  }
}
