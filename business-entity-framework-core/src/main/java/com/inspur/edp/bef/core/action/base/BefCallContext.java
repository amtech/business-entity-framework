/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.base;

import com.inspur.edp.bef.api.action.IBefCallContext;
import com.inspur.edp.cef.api.RefObject;
import java.util.HashMap;
import java.util.Objects;

public class BefCallContext implements IBefCallContext {

  private HashMap<String, Object> store = new HashMap<>();

  @Override
  public <T> T getData(String name) {
    Objects.requireNonNull(name, "name");
    if (!store.containsKey(name)) {
      throw new IllegalArgumentException("CallContext项不存在" + name);
    }
    return (T) store.get(name);
  }

  @Override
  public void setData(String name, Object value) {
    Objects.requireNonNull(name, "name");
    store.put(name, value);
  }

  @Override
  public <T> boolean tryGetData(String name, RefObject<T> value) {
    Objects.requireNonNull(name, "name");
    Objects.requireNonNull(value, "value");

    if (!store.containsKey(name)) {
      return false;
    }
    value.argvalue = (T) store.get(name);
    return true;
  }

  @Override
  public boolean containsKey(String name) {
    Objects.requireNonNull(name, "name");
    return store.containsKey(name);
  }

  public void clear() {
    store.clear();
  }

  //region transaction
//  @Override
//  public void commit(IBefTransactionItem upper) {
//    store = ((BefCallContext) upper).store;
//  }
//
//  @Override
//  public void rollBack(IBefTransactionItem upper) {
//  }
//
//  @Override
//  public IBefTransactionItem begin() {
//    BefCallContext rez = new BefCallContext();
//    rez.store = new HashMap<>(store);
//    return rez;
//  }
  //endregion
}
