/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.lcp;

import com.inspur.edp.bef.api.lcp.ILcpFactory;
import com.inspur.edp.bef.api.lcp.IStandardLcp;
import com.inspur.edp.bef.api.lcp.StatelessLcpExtend;
import com.inspur.edp.bef.api.services.IBefSessionManager;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.function.Consumer;
import java.util.function.Function;

public class StatelessLcpImpl implements StatelessLcpExtend {

  @Override
  public <T extends IStandardLcp> void execute(String configId, Consumer<T> lcpConsumer) {
    execute(configId, (T lcp) -> {
      lcpConsumer.accept(lcp);
      return null;
    });
  }

  @Override
  public <T extends IStandardLcp, TResult> TResult execute(String configId,
      Function<T, TResult> lcpConsumer) {
    IBefSessionManager sessionMgr = SpringBeanUtils.getBean(IBefSessionManager.class);
    sessionMgr.createSession();
    T lcp = (T) ((ILcpFactory) SpringBeanUtils.getBean(ILcpFactory.class)).createLcp(configId);

    try {
      return lcpConsumer.apply(lcp);
    } finally {
      sessionMgr.closeCurrentSession();
    }
  }
}
