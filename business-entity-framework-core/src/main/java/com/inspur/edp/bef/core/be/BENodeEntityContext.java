/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.be;


import com.inspur.edp.bef.api.action.IBefCallContext;
import com.inspur.edp.bef.api.be.BufferClearingType;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.api.lcp.IStandardLcp;
import com.inspur.edp.bef.core.DotNetToJavaStringHelper;
import com.inspur.edp.bef.core.LcpUtil;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.action.base.BEEntityActionExecutor;
import com.inspur.edp.bef.core.determination.DeterminationContext;
import com.inspur.edp.bef.core.scope.BefScopeStack;
import com.inspur.edp.bef.core.session.BEFuncSessionBase;
import com.inspur.edp.cef.api.dataType.action.IDataTypeActionExecutor;
import com.inspur.edp.cef.entity.changeset.AddChangeDetail;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.spi.entity.resourceInfo.EntityResInfo;
import com.inspur.edp.cef.api.scope.ICefScopeNodeParameter;
import com.inspur.edp.cef.entity.changeset.ChangeDetailMerger;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.entity.CefEntityContext;
import com.inspur.edp.cef.spi.entity.resourceInfo.ModelResInfo;
import com.inspur.edp.cef.variable.api.data.IVariableData;
import java.util.Objects;

public class BENodeEntityContext extends CefEntityContext implements IBENodeEntityContext
{
	private String privateID;
	@Override
	public String getID()
	{
		return privateID;
	}
	public void setID(String value)
	{
		privateID = value;
	}

	private String privateCode;
	@Override
	public String getCode()
	{
		return privateCode;
	}
	public void setCode(String value)
	{
		privateCode = value;
	}

	private IChangeDetail privateCurrentTemplateChange;
	@Override
	public final IChangeDetail getCurrentTemplateChange()
	{
		return privateCurrentTemplateChange;
	}
	public final void setCurrentTemplateChange(IChangeDetail value)
	{
		privateCurrentTemplateChange = value;
	}

	 @Override
	 public IChangeDetail getCurrentChange() {return super.getChange();}
	 public void setCurrentChange(IChangeDetail value) {super.setChange(value);}

	 @Override
	 public IEntityData getCurrentData(){return super.getData();}
	 public void setCurrentData(IEntityData value){super.setData(value);}

	private IChangeDetail privateTransactionalChange;
	@Override
	public final IChangeDetail getTransactionalChange()
	{
		return privateTransactionalChange;
	}
	public final void setTransactionalChange(IChangeDetail value)
	{
		privateTransactionalChange = value;
	}

	public IChangeDetail getCurrentTransactionalChange() {
		IChangeDetail currentChange = getCurrentChange();
		if (currentChange == null) {
			return getTransactionalChange();
		}
		IChangeDetail transactionalChange = getTransactionalChange();
		if (transactionalChange == null) {
			return currentChange;
		}
		if(transactionalChange.getChangeType() == ChangeType.Added
				&& currentChange.getChangeType() != ChangeType.Deleted){
			return new AddChangeDetail(getCurrentData());
		}
		ActionUtil.getRootBEContext(this).suspendChangeListener();
		try {
			return ChangeDetailMerger
					.mergeChangeDetail(currentChange, transactionalChange.clone());
		} finally {
			ActionUtil.getRootBEContext(this).resumeChangeListener();
		}
	}

	private IEntityData privateTransactionData;
	@Override
	public final IEntityData getTransactionData()
	{
		return privateTransactionData;
	}
	public final void setTransactionData(IEntityData value)
	{
		privateTransactionData = value;
	}

	private IEntityData privateOriginalData;
	@Override
	public final IEntityData getOriginalData()
	{
		return privateOriginalData;
	}
	public final void setOriginalData(IEntityData value)
	{
		privateOriginalData = value;
	}

	private IBENodeEntityContext privateParentContext;
	@Override
	public final IBENodeEntityContext getParentContext()
	{
		return privateParentContext;
	}
	public final void setParentContext(IBENodeEntityContext value)
	{
		privateParentContext = value;
	}

	@Override
	public final IStandardLcp getLcp(String config)
	{
		return LcpUtil.getLcp(config);
	}

	//public IStandardLcp getLcp(string config, bool newSession)
	//{
	//    return LcpUtil.getLcp(config, newSession);
	//}


	@Override
	public <T> IDataTypeActionExecutor<T> getActionExecutor() {
	return new BEEntityActionExecutor<T>(this);
	}

	private BEFuncSessionBase privateSessionItem;
	public final BEFuncSessionBase getSessionItem()
	{
		return privateSessionItem;
	}
	public final void setSessionItem(BEFuncSessionBase value)
	{
		privateSessionItem = value;
	}

	public IVariableData getVariables(){
		return DeterminationContext.getRootBEContext(this).getVariables();
	}

	@Override
	public final void addScopeParameter(ICefScopeNodeParameter parameter) {
		Objects.requireNonNull(parameter);
		getSessionItem().getFuncSession().getScopeManager().addScopeParameter(parameter);
	}

	 @Override
	 public BufferClearingType getBufferClearingType(){return getSessionItem().getBufferClearingType();}
	 @Override
	 public void setBufferClearingType(BufferClearingType value){getSessionItem().setBufferClearingType(value);}

		//region i18n
	private ModelResInfo privateModelResInfo;
	@Override
	public final ModelResInfo getModelResInfo()
	{
		return privateModelResInfo;
	}
	public final void setModelResInfo(ModelResInfo value)
	{
		privateModelResInfo = value;
	}

	public final EntityResInfo getEntityResourceInfos(String nodeCode)
	{
		if(getModelResInfo() == null) {
			return null;
		}
		return getModelResInfo().getCustomResource(nodeCode);
	}

	public final EntityResInfo getCurrentEntityResourceInfos()
	{
		return getEntityResourceInfos(getCode());
	}

	@Override
	public final String getEntityI18nName()
	{
		return getCurrentEntityResourceInfos().getDisplayName();
	}

	@Override
	public final String getPropertyI18nName(String labelID)
	{
		String result = getCurrentEntityResourceInfos().getPropertyDispalyName(labelID);
		if (DotNetToJavaStringHelper.isNullOrEmpty(result))
		{
			result = labelID;
		}
		return result;
	}

	@Override
	public final String getRefPropertyI18nName(String labelID, String refLabelID)
	{
		String result = getCurrentEntityResourceInfos().getAssoRefPropertyDisplay(labelID, refLabelID);
		if (DotNetToJavaStringHelper.isNullOrEmpty(result))
		{
			result = refLabelID;
		}
		return result;
	}

	@Override
	public final String getEnumValueI18nDisplayName(String labelID, String enumKey)
	{
		return getCurrentEntityResourceInfos().getEnumPropertyDispalyValue(labelID, enumKey);

	}

	@Override
	public final String getUniqueConstraintMessage(String conCode)
	{
		return getCurrentEntityResourceInfos().getUniqueConstraintMessage(conCode);
	}

	@Override
	public IBefCallContext getCallContext(){
		return getSessionItem().getCallContext();
	}
		//endregion
}
