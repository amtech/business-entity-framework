/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.validation.basetypes.childtrans;

import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.bef.api.be.IBENodeEntity;
import com.inspur.edp.bef.core.action.validation.AbstractValidationAdaptor;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public abstract class BeChildTransValAdapter extends AbstractValidationAdaptor {

  private final String name;
  private final String childCode;

  protected BeChildTransValAdapter(String name, String childCode) {
    this.name = name;
    this.childCode = childCode;
  }

  protected String getChildCode() {
    return childCode;
  }

  @Override
  public boolean canExecute(IChangeDetail change) {
    return com.inspur.edp.cef.entity.changeset.Util.containsChild(change, childCode);
  }

  @Override
  public void execute(ICefValidationContext iCefValidationContext, IChangeDetail iChangeDetail) {
    execute((IValidationContext) iCefValidationContext, iChangeDetail);
  }

  @Override
  public void execute(IValidationContext compContext, IChangeDetail iChangeDetail) {
    for (IChangeDetail item : com.inspur.edp.bef.api.changeset.Util
        .getChildChanges(iChangeDetail, childCode, (IValidationContext)compContext)) {
      IBENodeEntity itemEntity = compContext.getBENodeEntity()
          .getChildBEEntity(childCode, item.getDataID());
      doExecute(compContext, itemEntity);
    }
  }

  protected abstract void doExecute(IValidationContext compContext, IBENodeEntity itemEntity);
}
