/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.validation.builtinimpls.adaptors;

import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.determination.builtinimpls.predicate.ExecutePredicate;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.core.action.CompReflector;
import com.inspur.edp.cef.core.validation.builtinimpls.valadaptors.BaseValAdaptor;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.ChildElementsTuple;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.Util;
import java.util.List;
import java.util.Objects;

public class BefPredicateValAdaptor extends BaseValAdaptor {

  //TODO:改改基类
  private final Class type;
  private final ExecutePredicate predicate;

  public BefPredicateValAdaptor(Class type, ExecutePredicate predicate) {
    super(type, new CompReflector(type, IValidationContext.class, IChangeDetail.class));
    Objects.requireNonNull(predicate, "predicate");
    this.predicate = predicate;
    this.type = type;
  }

  @Override
  public boolean canExecute(IChangeDetail iChangeDetail) {
    return predicate.test(iChangeDetail);
  }

  @Override
  public void execute(ICefValidationContext ctx, IChangeDetail change) {
    String id = ActionUtil.getBizContextId();
    super.execute(ctx, change);
    ActionUtil.checkSessionChanged(id, null, ActionUtil.CATEGORY_VALIDATION, type);
  }
}
