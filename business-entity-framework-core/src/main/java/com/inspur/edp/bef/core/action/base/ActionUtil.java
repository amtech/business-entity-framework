/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.base;

import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.core.DebugAdapter;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.be.BEManagerContext;
import com.inspur.edp.bef.core.be.BENodeEntity;
import com.inspur.edp.bef.core.be.BusinessEntity;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.spi.action.AbstractAction;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.core.context.BizContext;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class ActionUtil {

  public static <TResult> BusinessEntity getRootEntity(AbstractAction<TResult> action) {
    return (BusinessEntity) action.getContext().getDataType();
  }

  public static <TResult> CoreBEContext getBEContext(AbstractAction<TResult> action) {
    return (CoreBEContext) action.getContext();
  }

  public static <TResult> BENodeEntity getBENodeEntity(AbstractAction<TResult> action) {
    return (BENodeEntity) action.getContext().getDataType();
  }

  public static <TResult> BEManager getBEManager(AbstractManagerAction<TResult> action) {
    return (BEManager) action.getBEManagerContext().getBEManager();
  }

  public static <TResult> BEManagerContext getBEManagerContext(
      AbstractManagerAction<TResult> action) {
    return (BEManagerContext) action.getBEManagerContext();
  }

  public static CoreBEContext getRootBEContext(IBENodeEntityContext context) {
    while (context != null && !(context instanceof CoreBEContext)) {
      context = context.getParentContext();
    }
    return (CoreBEContext) context;
  }

  public static final String CATEGORY_ENTITY_ACTION = "实体动作";
  public static final String CATEGORY_MANAGER_ACTION = "自定义动作";
  public static final String CATEGORY_DETERMINATION = "联动计算";
  public static final String CATEGORY_VALIDATION = "校验规则";

  public static void checkSessionChanged(String sessionId, String beType, String category,
      Object actionCode) {
    try {
      if (!sessionEquals(sessionId)) {
        DebugAdapter.trace("checkSessionChanged", "Error", null,
            "be[" + beType + "]上的" + category + "[" + actionCode + "]未正确创建或关闭BefSession");
      }
    }catch(Exception e) {
      log.error("bef内部异常checkSessionChanged", e);
    }
  }

  private final static boolean sessionEquals(String id) {
    String current = getBizContextId();
    if (id == null) {
      return current == null;
    } else if (current == null) {
      return false;
    } else {
      return id.equals(current);
    }
  }

  public static String getBizContextId() {
    BizContext bc = CAFContext.current.getBizContext();
    return bc != null ? bc.getId() : null;
  }

  private final static String Null_Param_Introduction = System.getProperties().getProperty("line.separator") + "此报错信息是因为调用本接口的程序中传参错误, 请根据报错堆栈联系调用方确认";
  public static void requireNonNull(Object value, String name) {
    Objects.requireNonNull(value, "参数"+name+"不能为null, "+ Null_Param_Introduction);
  }

  public static void requireNonNull(List list, String name){
    requireNonNull((Object)list, name);
    for(Object item : list) {
      if(item == null) {
        throw new IllegalArgumentException("集合类型的参数" + name + "中不允许包含值为null的项, 当前值为:"
            + DebugAdapter.buildString(list, true) + Null_Param_Introduction);
      }
    }
  }
}
