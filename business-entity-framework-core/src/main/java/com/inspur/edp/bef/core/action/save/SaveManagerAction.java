/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.save;

import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.core.be.BEContext;
import com.inspur.edp.bef.core.be.BEManagerContext;
import com.inspur.edp.bef.core.be.BusinessEntity;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.cef.api.message.MessageLevel;
import java.util.List;
import java.util.stream.Collectors;

public class SaveManagerAction extends AbstractManagerAction<Boolean>
{

		//region Consturctor
	public SaveManagerAction(IBEManagerContext managerContext)
	{
		super(managerContext);
	}


		//endregion


		//region 字段属性

		//endregion


		//region Execute
	@Override
	public void execute()
	{
//		//TODO gongbj 保存的流程还得再重新串串，保存前事件应该是在生成编号后执行
//		beforeSaveDeterminate();
//		additionalBeforeSaveDeterminate();
//		acceptCurrent();
//		beforeSaveCodeGenerate();
//		setResult(beforeSaveValidate());
//		if (getResult())
//		{
//			mergeChanges();
//			saveEntities();
//		}
	}
//	private void acceptCurrent()
//	{
//		for (IBusinessEntity entity : getBEManagerContext().getAllEntities())
//		{
//			((CoreBEContext)entity.getBEContext()).acceptChanges();
//		}
//	}
//	private static final int MaxAdditionalDtmRepeat = 99;
//	//使用CurrentTemplateChange循环触发保存前联动计算
//	private void additionalBeforeSaveDeterminate()
//	{
//		int repeatCount = 0;
//		List<IBusinessEntity> entities = getBEManagerContext().getAllEntities();
//		while (entities != null && !entities.isEmpty())
//		{
//			if (repeatCount++ > MaxAdditionalDtmRepeat)
//			{
//				throw new CircleDeterminationsFound("无");
//			}
//			for (IBusinessEntity entity : entities)
//			{
//				((BusinessEntity)entity).additionalBeforeSaveDeterminate();
//			}
//			entities = getBEManagerContext().getAllEntities().stream().filter(enty -> enty.getBEContext().getCurrentTemplateChange() != null).collect(
//					Collectors.toList());
//		}
//	}
//
//	private void beforeSaveDeterminate()
//	{
//		for (IBusinessEntity entity : getBEManagerContext().getAllEntities())
//		{
//			entity.beforeSaveDeterminate();
//		}
//	}
//
//	private void mergeChanges()
//	{
//		for (IBusinessEntity entity : getBEManagerContext().getAllEntities())
//		{
//			entity.BEContext.mergeIntoInnerChange();
//			entity.BEContext.acceptListenerChange();
//			entity.BEContext.acceptTempChange();
//		}
//	}
//
//	private void saveEntities()
//	{
//		for (IBusinessEntity entity : getBEManagerContext().getAllEntities())
//		{
//			entity.saveWithScope();
//		}
//	}
//
//	private boolean beforeSaveValidate()
//	{
//		boolean success = true;
//		for (IBusinessEntity entity : getBEManagerContext().getAllEntities())
//		{
//			entity.beforeSaveValidate();
//			success &= entity.BEContext.getResponse().MessageCollector.getMaxLevel() < MessageLevel.Error;
//		}
//
//		return success;
//	}
//
//	private void beforeSaveCodeGenerate()
//	{
//		for (IBusinessEntity entity : getBEManagerContext().getAllEntities())
//		{
//			entity.beforeSaveCodeGenerate();
//		}
//	}
//
//
//		//endregion
//
//
//		//region Assembly
//	@Override
//	protected IMgrActionAssembler GetMgrAssembler()
//	{
//		return getMgrActionAssemblerFactory().getSaveMgrActionAssembler(BEManagerContext);
//	}
//
//		//endregion
}
