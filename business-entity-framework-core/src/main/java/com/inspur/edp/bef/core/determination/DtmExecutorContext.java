/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.determination;

import com.inspur.edp.bef.api.action.determination.DtmExecutionHistory;
import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;

public class DtmExecutorContext /*implements IDtmExecutorContext*/
{
	private ICefEntityContext privateBeCtx;
	public final ICefEntityContext getBeCtx()
	{
		return privateBeCtx;
	}
	private void setBeCtx(ICefEntityContext value)
	{
		privateBeCtx = value;
	}

	public DtmExecutorContext(ICefEntityContext beCtx)
	{
		setBeCtx(beCtx);
	}

	private DtmExecutionHistory history;
	public DtmExecutionHistory getHistory(){return (history != null) ? history : (history = new DtmExecutionHistory());}

	private IDeterminationContext privateCompContext;
	public final IDeterminationContext getCompContext()
	{
		return privateCompContext;
	}
	public final void setCompContext(IDeterminationContext value)
	{
		privateCompContext = value;
	}
}
