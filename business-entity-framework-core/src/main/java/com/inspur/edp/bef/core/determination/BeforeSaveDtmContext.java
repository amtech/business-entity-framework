/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.determination;

import com.inspur.edp.bef.api.action.determination.IBeforeSaveDtmContext;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.condition.FilterCondition;
import java.util.ArrayList;
import java.util.List;

public class BeforeSaveDtmContext extends DeterminationContext implements IBeforeSaveDtmContext {

  //private BusinessActionContext tccCtx;
  private EntityFilter filter;

  public BeforeSaveDtmContext(IBENodeEntityContext beContext) {
    super(beContext);
  }

  @Override
  public void addDataSaveFilter(List<FilterCondition> condition) {
    if (filter == null) {
      filter = new EntityFilter();
    }
    filter.addFilterConditions(new ArrayList(condition));
  }

  public List<FilterCondition> getConditions() {
    return filter != null ? filter.getFilterConditions() : null;
  }

//  @Override
//  public BusinessActionContext getBusinessActionContext() {
//    return FuncSessionManager.getCurrentSession().getCallContext().getData("bef_core_tccCtx");
//  }
}
