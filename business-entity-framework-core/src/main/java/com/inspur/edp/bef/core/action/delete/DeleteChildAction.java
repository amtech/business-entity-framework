/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.delete;

import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.action.assembler.IBEActionAssembler;
import com.inspur.edp.bef.api.exceptions.BefException;
import com.inspur.edp.bef.api.exceptions.ErrorCodes;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import com.inspur.edp.bef.spi.action.AbstractAction;
import com.inspur.edp.cef.entity.changeset.DeleteChangeDetail;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DeleteChildAction extends AbstractAction<VoidActionResult>
{

		//region Constructor
	private java.util.List<String> nodeCodes;
	private java.util.List<String> hierachyIdList;
	private java.util.List<String> ids;

	public DeleteChildAction(java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, java.util.List<String> ids)
	{
		this.nodeCodes = nodeCodes;
		this.hierachyIdList = hierachyIdList;
		this.ids = ids;
	}

		//endregion

	@Override
	public void execute()
	{
		AuthorityUtil.checkDataAuthority(ActionUtil.getBEContext(this));
		//ModifyAction.demandPermission(Context);
		//LockUtils.checkLock(Context);
IEntityDataCollection childCollection = ActionUtil.getBEContext(this).getCurrentData().getChilds().get(nodeCodes.get(0));
		ModifyChangeDetail rootChange = new ModifyChangeDetail(hierachyIdList.get(0));
		ModifyChangeDetail change = rootChange;
		for (int i = 1; i < nodeCodes.size(); i++)
		{
			final int index = i;
			ModifyChangeDetail childChange = new ModifyChangeDetail(hierachyIdList.get(i));
			change.addChildChangeSet(nodeCodes.get(i - 1), childChange);
			change = childChange;
Optional<IEntityData> child = childCollection.stream().filter((IEntityData item) -> hierachyIdList.get(index).equals(item.getID())).findFirst();
			childCollection =child.get() != null ?child.get().getChilds().get(nodeCodes.get(i)) : null;
			//change.addChildChangeSet(nodeCodes)
		}
List<IEntityData> toDeleteList =
        childCollection == null
            ? null
            : childCollection.stream()
                .filter(item -> ids.contains(item.getID()))
                .collect(Collectors.toList());
		if (toDeleteList == null || toDeleteList.size() != ids.size())
		{
			throw new BefException(ErrorCodes.DeletingNonExistence, "要删除的数据不存在", null, ExceptionLevel.Error); //ids.except(toDeleteList.select(item => item.ID));
		}
for (IEntityData item : toDeleteList)
		{
			change.addChildChangeSet(nodeCodes.get(nodeCodes.size() - 1), new DeleteChangeDetail(item.getID()));
		}

		ActionUtil.getRootEntity(this).appendTempCurrentChange(rootChange);
		//BEContext.ChangeFromModify = rootChange;
		//CoreBEContext..acceptChangeFromModify();
	}

	@Override
	protected IBEActionAssembler getAssembler()
	{
		return GetAssemblerFactory().getDeleteChildActionAssembler(ActionUtil.getBEContext(this), nodeCodes, hierachyIdList, ids);
	}
}
