/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.session;

import io.iec.edp.caf.core.context.BizContext;
import io.iec.edp.caf.core.context.BizContextExpirationPolicy;
import io.iec.edp.caf.core.session.Session;

import java.time.ZonedDateTime;

public class BefBizContext extends BizContext {
	public BefBizContext(){
	}
	public BefBizContext(String id, ZonedDateTime creationDate, Session parent, BizContextExpirationPolicy expirationPolicy)
	{
		super(id, creationDate, parent,expirationPolicy);
	}

	public void setId(String id){
		this.id = id;
	}

	public void setCreationDate(ZonedDateTime creationDate){
		this.creationDate = creationDate;
	}
}
