/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.session.distributed.dac;

import com.inspur.edp.bef.core.session.FuncSession;
import com.inspur.edp.commonmodel.core.session.distributed.dac.FuncSessionIncrement;
import java.util.concurrent.locks.Lock;

public interface FuncSessionDac {

  Lock getFuncInstanceLock(String funcInstId);

  Lock getSessionLock(String session);

  Lock getLocalSessionLock(String session);

  boolean isLatest(String sessionId, int version);

  String getSessionListByToken(String token);

  void addSessionWithToken(String userSessionId, String token, String sessionId);
  void removeSessionWithToken(String userSessionId, String token, String sessionId);

//  void addSession(String userSessionId, String sessionId);

  void removeSession(String sessionId);

  FuncSession recover(String id, FuncSession existing);

  void update(FuncSession session, FuncSessionIncrement value);

//  void reset(FuncSession session);
}
