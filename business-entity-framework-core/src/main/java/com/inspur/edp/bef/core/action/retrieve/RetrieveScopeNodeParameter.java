/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrieve;

import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.scope.ScopeNodeParameter;
import com.inspur.edp.cef.entity.entity.IEntityData;
import java.util.function.BiConsumer;

public class RetrieveScopeNodeParameter extends ScopeNodeParameter
{


		//region Constructor
	public RetrieveScopeNodeParameter(IBEContext beContext, RetrieveParam par, BiConsumer<IBEContext, IEntityData> action)
	{
		super(beContext);
		this.action = action;
		privateParam = par;
	}


		//endregion Constructor


		//region 属性字段
	private RetrieveParam privateParam;
	public final RetrieveParam getParam()
	{
		return privateParam;
	}

	private BiConsumer<IBEContext, IEntityData> action;

	@Override
	public String getParameterType() {return "PlatformCommon_Bef_RetrieveScopeNodeParameter";}


		//endregion

	public final void executeAction(IBEContext beContext, IEntityData data)
	{
		action.accept(beContext, data);
	}
}
