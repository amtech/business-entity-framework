/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.validation;


import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.bef.spi.action.validation.IValidationAdaptor;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public abstract class AbstractValidationAdaptor implements IValidationAdaptor
{
	@Override
	public abstract boolean canExecute(IChangeDetail change);

	@Override
	public abstract void execute(IValidationContext compContext, IChangeDetail change);

//void IValidation.execute(ICefValidationContext context, IChangeDetail change) => Execute((IValidationContext)context, change);
}
