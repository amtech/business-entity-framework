/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrievedefault;

import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.cef.entity.entity.IEntityData;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.util.StringUtils;

public class MultiRetrieveDefaultMgrAction extends AbstractManagerAction<Map<String, IEntityData>> {

   //region Consturctor
   private boolean useID;
   private List<String> dataIDs;
   private java.util.Map<String, Object> defaultValues;

   public MultiRetrieveDefaultMgrAction(IBEManagerContext managerContext, List<String> dataIDs,
       java.util.Map<String, Object> defaultValues) {
      super(managerContext);
      ActionUtil.requireNonNull(dataIDs, "dataIDs");
      this.dataIDs = dataIDs;
      this.defaultValues = defaultValues;
   }

   //endregion

   //region Override

   @Override
   public final void execute() {
      getBEManagerContext().checkAuthority("RetrieveDefault");
//      AuthorityUtil.checkAuthority("");
//      FuncSessionManager.getCurrentSession().getBefContext()
//          .setCurrentOperationType("RetrieveDefault");

      Map<String, IEntityData> result = new HashMap<>(dataIDs.size(), 1);
      for (String id : dataIDs) {
         if (StringUtils.isEmpty(id)) {
            throw new RuntimeException("批量新增dataIDs中不允许存在空串");
         }
         IBusinessEntity be = getBEManagerContext().getEntity(id);
         be.retrieveDefault(defaultValues);
         IEntityData curr = be.getBEContext().getCurrentData();
         if (result.put(id, curr) != null) {
            throw new RuntimeException("批量新增时存在重复的Id:" + id);
         }
      }

      setResult(result);
   }

   //TODO:
   @Override
   protected final IMgrActionAssembler getMgrAssembler() {
      return getMgrActionAssemblerFactory()
          .getRetrieveDefaultMgrActionAssembler(getBEManagerContext());
   }

   //endregion
}
