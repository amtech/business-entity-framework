/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.udt.core.Udt;


import com.inspur.edp.cef.api.dataType.action.IDataTypeActionExecutor;
import com.inspur.edp.cef.api.dataType.base.ICefDataType;
import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObject;
import com.inspur.edp.cef.spi.entity.resourceInfo.ValueObjModelResInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.ValueObjResInfo;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IValueObjData;
import com.inspur.edp.udt.api.Udt.ITemplateValues;
import com.inspur.edp.udt.api.Udt.IUdtContext;

public final class UdtContext implements IUdtContext {

  public UdtContext() {
    privateTemplateValues = new TemplateValues();
  }

  private IValueObjData privateData;

  public IValueObjData getValuObjData() {
    return privateData;
  }

  public void setValueObjData(IValueObjData value) {
    privateData = value;
  }

  private ICefValueObject privateDataType;

  public ICefValueObject getValueObjDataType() {
    return privateDataType;
  }

  public void setValueObjDataType(ICefValueObject value) {
    privateDataType = value;
  }

  //	public ICefData getData()
//	{
//		return getValuObjData();
//	}
//	 void setData(ICefData value)
//	 ICefDataType getDataType()
//	 void setDataType(ICefDataType value)
  private ITemplateValues privateTemplateValues;

  public ITemplateValues getTemplateValues() {
    return privateTemplateValues;
  }

  private <T> IDataTypeActionExecutor<T> GetActionExecutor() {
    throw new UnsupportedOperationException();
  }

  ///#region i18n

  private String nodeCode = "RootNode";
  private ValueObjModelResInfo privateModelResourceInfos;

  public ValueObjModelResInfo getModelResInfo() {
    return privateModelResourceInfos;
  }

  public void setModelResInfo(ValueObjModelResInfo value) {
    privateModelResourceInfos = value;
  }

  public ValueObjResInfo GetCurrentEntityResourceInfos() {
    ValueObjModelResInfo modelResourceInfos = getModelResInfo();
    return modelResourceInfos == null ? null
        : modelResourceInfos.getCustomResource(modelResourceInfos.getRootNodeCode());
  }

  public String GetEntityI18nName() {
    if (GetCurrentEntityResourceInfos() != null) {
      return GetCurrentEntityResourceInfos().getDisplayName();
    }
    return null;
  }

  public String GetPropertyI18nName(String labelID) {
//C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
    String result = null;
    if (GetCurrentEntityResourceInfos() != null) {
      result = GetCurrentEntityResourceInfos().getPropertyDispalyName(labelID);
    }
    if (result == "" || result == null) {
      result = labelID;
    }
    return result;
  }

  public String GetRefPropertyI18nName(String labelID, String refLabelID) {
    if (GetCurrentEntityResourceInfos() == null) {
      return null;
    }
    return GetCurrentEntityResourceInfos().getAssoRefPropertyDisplay(labelID, refLabelID);
  }

  public String GetEnumValueI18nDisplayName(String labelID, String enumKey) {
    if (GetCurrentEntityResourceInfos() == null) {
      return null;
    }
    return GetCurrentEntityResourceInfos().getEnumPropertyDispalyValue(labelID, enumKey);
  }

  public IValueObjData getData() {
    return getValuObjData();
  }

  public void setData(ICefData iCefData) {
    setValueObjData((IValueObjData) iCefData);
  }

  public void setData(IValueObjData iValueObjData) {
    setValueObjData(iValueObjData);
  }

  public ICefValueObject getDataType() {
    return getValueObjDataType();
  }

  public void setDataType(ICefDataType iCefDataType) {
    setValueObjDataType((ICefValueObject) iCefDataType);
  }

  public <T> IDataTypeActionExecutor<T> getActionExecutor() {
    throw new UnsupportedOperationException();
  }

  public void setDataType(ICefValueObject iCefValueObject) {
    setValueObjDataType((ICefValueObject) iCefValueObject);
  }


  ///#endregion
}
