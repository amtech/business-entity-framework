/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.udt.core.Determination;

import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.spi.entity.resourceInfo.ValueObjModelResInfo;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.udt.api.Determination.IDeterminationContext;
import com.inspur.edp.udt.api.Udt.ITemplateValues;
import com.inspur.edp.udt.api.Udt.IUdtContext;
import com.inspur.edp.udt.api.Udt.IUnifiedDataType;
import com.inspur.edp.udt.core.Udt.UdtContext;
import com.inspur.edp.udt.entity.IUdtData;

public class UdtDeterminationContext implements IDeterminationContext
{
	private final UdtContext nodeContext;
	private ICefDeterminationContext parentCtx;

	public UdtDeterminationContext(IUdtContext nodeContext, ICefDeterminationContext parentCtx) {
		this(nodeContext);
		this.parentCtx = parentCtx;
	}

	public UdtDeterminationContext(IUdtContext nodeContext) {
		this.nodeContext = (UdtContext)nodeContext;
	}

	public IUdtData getUdtData(){
		return (IUdtData)nodeContext.getData();
	}

	public ITemplateValues getTemplateValues(){return  nodeContext.getTemplateValues();}

	public ValueObjModelResInfo getModelResInfo() {return nodeContext.getModelResInfo();}

	public ICefData getData() {
		return getUdtData();
	}

	public String getEntityI18nName()
	{
		return nodeContext.GetEntityI18nName();
	}

	public String getPropertyI18nName(String labelID) {
		return nodeContext.GetPropertyI18nName(labelID);
	}

	public  String getRefPropertyI18nName(String labelID, String refLabelID) {
		return nodeContext.GetRefPropertyI18nName(labelID, refLabelID);
	}

	public  String getEnumValueI18nDisplayName(String labelID, String enumKey) {
		return nodeContext.GetEnumValueI18nDisplayName(labelID, enumKey);
	}

	@Override
	public ICefDeterminationContext getParentContext() {
		return parentCtx;
	}

	@Override
	public String getPropertyName() {
		return ((IUnifiedDataType)nodeContext.getValueObjDataType()).getPropertyName();
	}
}
