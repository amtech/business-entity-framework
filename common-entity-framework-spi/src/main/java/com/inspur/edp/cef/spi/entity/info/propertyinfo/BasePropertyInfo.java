/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.info.propertyinfo;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.api.manager.serialize.CefSerializeContext;
import com.inspur.edp.cef.api.manager.serialize.NestedSerializeContext;
import com.inspur.edp.cef.entity.entity.IEntityData;

public abstract class BasePropertyInfo {

    public abstract void write(JsonGenerator writer, String propertyName, Object value, SerializerProvider serializer, CefSerializeContext serContext);

    public abstract void writeChange(JsonGenerator writer, String propertyName, Object value, SerializerProvider serializer, CefSerializeContext serContext);

    public abstract Object read(JsonParser reader, String propertyName, DeserializationContext serializer, CefSerializeContext serContext);

    public abstract Object read(JsonNode node, String propertyName, CefSerializeContext serContext);


    public abstract Object readChange(JsonParser reader, String propertyName, DeserializationContext serializer, CefSerializeContext serContext);

    public abstract Object readChange(JsonNode node, String propertyName, CefSerializeContext serContext);

    public abstract void setValue(Object data, String propertyName, Object value, CefSerializeContext serContext);

    public abstract Object createValue();

    public Object createChange(){
        return null;
    }

    public String getPropValue(Object value){
        return "";
    }
}
