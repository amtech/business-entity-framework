/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.resourceInfo;

import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.entity.entity.FieldType;
import com.inspur.edp.cef.spi.entity.PropertyDefaultValue;
import com.inspur.edp.cef.spi.entity.info.EnumValueInfo;

import com.inspur.edp.cef.spi.entity.info.I18nEnumValueInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.AssocationPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.BasePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.ComplexUdtPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.ObjectType;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.SimpleAssoUdtPropertyInfo;
import java.util.ArrayList;
import java.util.List;

public abstract class DataTypeResInfo {


	/**
	 * 节点名称
	 *
	 * @return
	 */
	public String getDisplayName() {
		return null;
	}

	/**
	 * 字段名称
	 *
	 * @return
	 */
	public String getPropertyDispalyName(String labelId) {
		return null;
	}

	/**
	 * 枚举显示值
	 *
	 * @return
	 */
	public String getEnumPropertyDispalyValue(String labelId, String enumKey) {
		return null;
	}

	public String getAssoRefEnumPropertyDisplayValue(String labelId,String refEleCode,String enumKey)
	{
		throw new RuntimeException("此特性在2011盘之后支持。");
	}

	/**
	 * 枚举信息
	 *
	 * @return
	 */
	public EnumValueInfo getPropEnumInfo(String labelId, String enumKey) {
		return null;
	}

	/**
	 * 枚举信息
	 *
	 * @return
	 */
	public List<EnumValueInfo> getPropEnumInfos(String labelId) {
		return null;
	}

	/**
	 * 关联带出属性显示值
	 *
	 * @return
	 */
	public String getAssoRefPropertyDisplay(String labelId, String refEleCode) {
		return null;
	}

	/**
	 * 当前语言下枚举显示值
	 */
	public java.util.List<I18nEnumValueInfo> getI18nPropEnumInfos(java.lang.String propName) {
		java.util.List<EnumValueInfo> infos = this.getPropEnumInfos(propName);
		if (infos == null || infos.isEmpty()) {
			return new ArrayList<>();
		}
		List<I18nEnumValueInfo> i18nInfos = new ArrayList<>();
		infos.forEach(info -> {
			String currentName = getEnumPropertyDispalyValue(propName, info.getEnumCode());
			I18nEnumValueInfo i18nInfo = new I18nEnumValueInfo(info, currentName);
			i18nInfos.add(i18nInfo);
		});
		return i18nInfos;
	}
	protected  void addPropertyInfo(DataTypePropertyInfo propertyInfo) {

	}

	

}
