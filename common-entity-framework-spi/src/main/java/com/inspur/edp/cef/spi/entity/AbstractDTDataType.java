/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.spi.entity;

import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.spi.entityaction.CefDataTypeAction;
import com.inspur.edp.cef.api.dataType.action.IDataTypeActionExecutor;
import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;

public abstract class AbstractDTDataType
{
	private ICefDataTypeContext privateContext;
	public  ICefDataTypeContext getContext()
	{
		return privateContext;
	}
	public final void setContext(ICefDataTypeContext value)
	{
		privateContext = value;
	}

	protected <TResult> TResult execute(CefDataTypeAction<TResult> action) {
		DataValidator.checkForNullReference(action, "action");


		IDataTypeActionExecutor<TResult> executor = getContext().<TResult>getActionExecutor();
		executor.setAction(action);
		return executor.execute();
		//return new EntityActionExecutor<TResult>
		//{
		//    Action = action,
		//    Context = Context
		//}.execute();
	}
}
