/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.resourceInfo;

import java.util.Map;

public abstract class EntityResInfo extends DataTypeResInfo {

  /**
   * 唯一性约束提示信息
   *
   * @param uniqueConCode 唯一性约束编号
   * @return
   */
  public String getUniqueConstraintMessage(String uniqueConCode) {
    return null;
  }


  public String getEntityCode() {
    return null;
  }


  public EntityResInfo getChildEntityResInfo(String entityCode) {
    throw new RuntimeException("需要子类重写该实现。");
  }

  public Map<String, EntityResInfo> getChildEntityResInfos() {
    throw new RuntimeException("需要子类重写该实现。");
  }

}
