/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.spi.event.query;

import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.authority.AuthorityInfo;
import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.dependenceTemp.Pagination;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.determination.AbstractDeterminationAction;

import java.util.ArrayList;
import java.util.List;

public interface ICefQueryEventListener {

    //联动计算记录类的位置
    public void beforeQuery(EntityFilter filter, ArrayList<AuthorityInfo> authorities);

    public void beforePageQuery(String execTableName,
                                String tableName,
                                String execColumnNames,
                                String pkField,
                                String execFilterCondition,
                                String execSortCondition,
                                ArrayList<DbParameter> parameters,
                                RefObject<Pagination> pagination
    );

    public void afterPageQuery(List<IEntityData> resultvar);

    public void beforeNoPageQuery(String tablename, String condition, String sort, String queryfields, String sql, ArrayList<DbParameter> parameter);

    public void afterNoPageQuery(List<IEntityData> resultSet);

    public void afterQuery(List<IEntityData> resultvar);

    public void queryUnnormalStop(Exception e);

    public void processRecordClassPath(AbstractDeterminationAction determination);
}
