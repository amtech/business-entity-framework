/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.spi.validation;

import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.api.validation.ICefValidationContext;

public interface IValidation
{
	/** 
	 判断校验规则是否需要触发的检查方法
	 
	 @param change 实体变更集
	 @return 判断结果<see cref="bool"/>
	*/
	boolean canExecute(IChangeDetail change);

	void execute(ICefValidationContext context, IChangeDetail change);
}
