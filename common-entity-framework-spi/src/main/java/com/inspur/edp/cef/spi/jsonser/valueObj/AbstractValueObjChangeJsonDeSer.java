/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.spi.jsonser.valueobj;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.cef.entity.changeset.AbstractModifyChangeDetail;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefChangeJsonDeserializer;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefDataSerItem;

import java.io.IOException;
import java.util.List;

public abstract class AbstractValueObjChangeJsonDeSer extends AbstractCefChangeJsonDeserializer {


    public AbstractValueObjChangeJsonDeSer(List<AbstractValueObjSerializer> deSerializers) {
        super(deSerializers);
    }


    @Override
    protected IEntityData createData()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    protected AbstractModifyChangeDetail createModifyChangeDetail(JsonParser p, DeserializationContext ctxt)throws IOException
    {return new ValueObjModifyChangeDetail();}
}
