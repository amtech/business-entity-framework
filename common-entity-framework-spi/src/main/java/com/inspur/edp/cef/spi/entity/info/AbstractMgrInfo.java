/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.info;

/** 
 模型层的信息
 
*/
public abstract class AbstractMgrInfo
{
	/** 
	 模型名称_资源项Key
	 
	*/
	public abstract String getDisplayValueKey();

	/** 
	 资源文件元数据ID
	 
	*/
	public abstract String getResourceMetaId();

	/** 
	 主节点编号
	 
	*/
	public abstract String getRootNodeCode();

	/** 
	 业务实体所在SU
	 
	*/
	public abstract String getCurrentSU();
}
