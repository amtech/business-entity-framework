/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.spi.jsonser.entity;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefDataSerItem;
import java.util.Map;

public abstract class AbstractEntitySerializerItem extends AbstractCefDataSerItem
{
	public abstract void writeEntityBasicInfo(JsonGenerator jsonGenerator, IEntityData data, SerializerProvider serializerProvider);

	@Override
	public  void writeEntityBasicInfo(JsonGenerator jsonGenerator, ICefData data, SerializerProvider serializerProvider)
{ writeEntityBasicInfo(jsonGenerator, (IEntityData)data, serializerProvider);}

	@Override
	public  boolean readEntityBasicInfo(JsonParser p, DeserializationContext ctxt, ICefData data, String propertyName)
	{return readEntityBasicInfo(p,ctxt, (IEntityData)data, propertyName);}

	///#endregion

	public abstract boolean readEntityBasicInfo(JsonParser p, DeserializationContext ctxt, IEntityData data, String propertyName);

	/**
	 * vo序列化集中，记录vo标签与前端名映射 <传到前端属性名,voLabelId> e.g.<name,Name>
	 */
	public Map<String, String> getTransferPropertyNameMap() {
		return null;
	}
}
