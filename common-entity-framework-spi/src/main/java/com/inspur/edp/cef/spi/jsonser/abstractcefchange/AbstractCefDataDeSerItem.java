//package com.inspur.edp.cef.spi.jsonser.abstractcefchange111;
//
//import com.fasterxml.jackson.core.JsonParser;
//import com.fasterxml.jackson.databind.DeserializationContext;
//import com.inspur.edp.cef.entity.changeset.IChangeDetail;
//import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
//import com.inspur.edp.cef.entity.entity.ICefData;
//import com.inspur.edp.cef.api.RefObject;
//
//import java.io.IOException;
//import java.sql.Date;
//
//public abstract class AbstractCefDataDeSerItem {
//    public Object readModifyPropertyValue(JsonParser p, DeserializationContext ctxt, String propertyName, RefObject<Boolean> hasRead)
//    {
//        hasRead.argvalue = false;
//        return null;
//    }
//    public Object readModifyPropertyValue(JsonParser p, DeserializationContext ctxt, RefObject<String> propertyName, RefObject<Boolean> hasRead)
//    {
//        hasRead.argvalue = false;
//        return null;
//    }
//    public abstract boolean readEntityBasicInfo(JsonParser p, DeserializationContext ctxt, ICefData data, String propertyName);
//
//    protected final String ReadString(JsonParser p) throws IOException {
//        return p.getValueAsString();
//
//    }
//
//    /**
//     读取整型数据
//
//     @param value 值
//     @return 得到的整数值
//     */
//    protected final int readInt(JsonParser p) throws IOException
//    {
//        return p.getValueAsInt();
//    }
//
//    /**
//     读取小数数据
//
//     @param value 值
//     @return 得到的小数值
//     */
//    protected final double readDecimal(JsonParser p) throws IOException
//    {
//        return p.getValueAsDouble();
//    }
//
//    /**
//     读取布尔数据
//
//     @param value 值
//     @return 得到的布尔值
//     */
//    protected final boolean readBool(JsonParser p) throws IOException
//    {
//        return p.getValueAsBoolean();
//    }
//    /**
//     读取日期数据
//
//     @param value 值
//     @return 得到的日期值
//     */
//    protected final java.util.Date readDateTime(JsonParser p) throws IOException
//    {
//        String strValue =p.getValueAsString();
//        if(strValue!=null&&strValue!="")
//            return Date.valueOf(strValue);
//        else {
//            return null;
//        }
//
//    }
//    /**
//     读取二进制数据
//
//     @param value 值
//     @return 得到的二进制数据值
//     */
//    protected final byte[] readBytes(JsonParser p) throws IOException
//    {
//        throw new UnsupportedOperationException();
//        //Jaav版临时屏蔽
////        return Convert.FromBase64String(String.valueOf(value));
//    }
//
//    /**
//     读取枚举数据
//
//     @param value 值
//     @return 得到的枚举值
//     */
//    protected final <T> T readEnum(JsonParser p) throws IOException
//    {
//        throw new UnsupportedOperationException();
//        //Java版临时屏蔽
////        return T.valueOf(String.valueOf(value));
//    }
//    /**
//     读取关联数据
//
//     @param value 值
//     @return 得到的关联值
//     */
//    protected final <T> T readAssociation(JsonParser p) throws IOException
//    {
//        throw new UnsupportedOperationException();
//        //Java版临时屏蔽
////        p.readValueAs(Class<T>);
////        return ((JToken)((value instanceof JToken) ? value : null)).<T>ToObject();
//    }
//
//
//    @Deprecated
//    protected final <T> T readAssociation(Object value)
//    {
//        throw new UnsupportedOperationException();
//        //Java版临时屏蔽
////        return Activator.<T>CreateInstance();
//    }
//
//    protected final java.util.HashMap<String, Object> readAssociationValue(JsonParser p, Object value, DeserializationContext ctxt) throws IOException {
//
//        throw new UnsupportedOperationException();
//        //Java版临时屏蔽
//        //        java.util.HashMap<String, Object> transValue = new java.util.HashMap<String, Object>();
////        if (p.getCurrentToken()== JsonToken.VALUE_NULL)
////        {
////            return transValue;
////        }
////
//////        reader.Read();
////        while (p.getCurrentToken() != JsonToken.END_OBJECT)
////        {
//////C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
////            String propName = p.getValueAsString();
////           //Java版临时屏蔽
////           // reader.Read();
//////C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
////            var changeValue = reader.getValue();
////            if (reader.TokenType == JsonToken.StartObject)
////            {
////                changeValue = JToken.ReadFrom(reader);
////                ReadObject(reader);
////            }
////            else
////            {
////                reader.Read();
////            }
////            //reader.Read();
////            transValue.put(propName, changeValue);
////        }
////
////        return transValue;
//    }
//
//    @Deprecated
//    protected final java.util.HashMap<String, Object> readAssociationValue(Object value)
//    {
//        return new java.util.HashMap<String, Object>();
//    }
//
//    protected final ICefData getUdtEntity(Object value, String configId)
//    {
//        throw new UnsupportedOperationException();
//        //Java版临时屏蔽
////        return UdtManagerUtil.getUdtFactory().CreateManager(configId).Deserialize(String.valueOf((JObject)((value instanceof JObject) ? value : null)));
//    }
//
//    //此方法不再使用, 兼容已生成dll暂时保留
//    protected final <T> T readNestedValue(String nestedConfigId, Object value)
//    {
//        throw new UnsupportedOperationException();
//        //Java版临时屏蔽
////        Object tempVar = UdtManagerUtil.getUdtFactory().CreateManager(nestedConfigId);
//////C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
////        var val = (ICefValueObjManager)((tempVar instanceof ICefValueObjManager) ? tempVar : null);
////        return (T)val.CreateDataType();
//    }
//
//    protected final <T> T readNestedValue(String nestedConfigId, JsonParser p, DeserializationContext ctxt)throws IOException
//    {
//        throw new UnsupportedOperationException();
//        //Java版临时屏蔽
////        Object tempVar = UdtManagerUtil.getUdtFactory().CreateManager(nestedConfigId);
//////C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
////        var val = (ICefValueObjManager)((tempVar instanceof ICefValueObjManager) ? tempVar : null);
////        return (T)val.Deserialize(String.valueOf(JObject.Load(reader)));
//    }
//
//    @Deprecated
//    protected final IChangeDetail readNestedChange(String nestedConfigId, Object value)
//    {
//        return new ValueObjModifyChangeDetail();
//    }
//
//    protected final IChangeDetail readNestedChange(String nestedConfigId, JsonParser p, DeserializationContext ctxt)
//    {
//        throw new UnsupportedOperationException();
//        //Java版临时屏蔽
////        Object tempVar = UdtManagerUtil.getUdtFactory().CreateManager(nestedConfigId);
//////C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
////        var val = (ICefValueObjManager)((tempVar instanceof ICefValueObjManager) ? tempVar : null);
////        return val.DeserializeChange(String.valueOf(JObject.Load(reader)));
//    }
//
//    //动态属性data反序列化, 动态属性字段
////         设计时生成代码如:
////         * case "DynamicProp1":
////         *     ReadDynamicPropInfo(reader, salesOrder.DynamicProp1, "DynamicProp1", staticSerItems, serializer);
////         *     return true;
////
//    //Java版临时屏蔽
////    protected final void ReadDynamicPropSetValue(Object value, IDynamicPropSet data, String dynPropSetPropName, IDynamicPropSerItem serItem, JsonReader reader, JsonSerializer serializer)
////    {
////        DataValidator.CheckForNullReference(serItem, "serItem");
////
////        reader.Read();
////        while (reader.TokenType != JsonToken.EndObject)
////        {
////            String propName = serItem.GetActualItemName((String)reader.getValue());
////            reader.Read();
////            data.SetValue(propName, serItem.ReadItemValue(propName, reader, serializer));
////            reader.Read();
////        }
////    }
//
//    //动态属性变更集反序列化, 动态属性字段
////         设计时生成代码如:
////         * case "DynamicProp1":
////         *     hasRead = true;
////         *     return ReadDynamicPropSetChange(propertyValue, propName, staticSerItems, serializer);
////
////C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
////Java版临时屏蔽
////    protected IChangeDetail ReadDynamicPropSetChange(Object propertyValue, String dynPropSetPropName, IDynamicPropSerItem serItem, JsonSerializer serializer) => ReadDynamicPropSetChange(propertyValue, dynPropSetPropName, serItem, null, serializer);
////Java版临时屏蔽
////    protected final IChangeDetail ReadDynamicPropSetChange(Object propertyValue, String dynPropSetPropName, IDynamicPropSerItem serItem, JsonReader reader, JsonSerializer serializer)
////    {
////        DataValidator.CheckForNullReference(serItem, "serItem");
////
//////C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
////        var objReader = ((JObject)((propertyValue instanceof JObject) ? propertyValue : null)).CreateReader();
////        objReader.Read();
////        while (!String.equals(CamelConst.ChangeInfo, String.valueOf(objReader.getValue()), StringComparison.InvariantCultureIgnoreCase))
////        {
////            objReader.Read();
////        }
////        ValueObjModifyChangeDetail changeDetail = new ValueObjModifyChangeDetail();
////
////        objReader.Read();
////        objReader.Read();
////
////
////        while (objReader.TokenType == JsonToken.PropertyName)
////        {
////            String propName = serItem.GetActualItemName((String)objReader.getValue());
////            objReader.Read();
////            changeDetail[propName] = serItem.ReadItemChange(propName, objReader, serializer);
////            objReader.Read();
////        }
////
////        return changeDetail;
////    }
//}
