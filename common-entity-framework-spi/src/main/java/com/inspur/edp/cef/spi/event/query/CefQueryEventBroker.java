/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.spi.event.query;

import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.authority.AuthorityInfo;
import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.dependenceTemp.Pagination;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.determination.AbstractDeterminationAction;

import java.util.ArrayList;
import java.util.List;

public class CefQueryEventBroker {
    private static List<ICefQueryEventListener> eventLs = new ArrayList<ICefQueryEventListener>() {{
        //add(getCefQueryEventListener("com.inspur.edp.cef.debugtool.listener.impl.query.CefQueryEventListener"));

    }};

    private static ICefQueryEventListener getCefQueryEventListener(String className){
        try {
            Class cls=Class.forName(className);
            ICefQueryEventListener listener= (ICefQueryEventListener) cls.newInstance();
            return listener;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("listener实例初始化失败",e);
        }
    }


    public static final void fireBeforeQuery(EntityFilter filter, ArrayList<AuthorityInfo> authorities) {
        for (ICefQueryEventListener el : eventLs) {
            el.beforeQuery(filter, authorities);
        }
    }
    public static final void fireBeforePageQuery(String execTableName,
                                                String tableName,
                                                String execColumnNames,
                                                String pkField,
                                                String execFilterCondition,
                                                String execSortCondition,
                                                ArrayList<DbParameter> parameters,
                                                RefObject<Pagination> pagination
                                                ) {
        for (ICefQueryEventListener el : eventLs) {
            el.beforePageQuery(execTableName, tableName, execColumnNames, pkField, execFilterCondition, execSortCondition, parameters, pagination);
        }
    }
    public static final void fireAfterPageQuery(List<IEntityData> resultvar) {
        for (ICefQueryEventListener el : eventLs) {
            el.afterPageQuery(resultvar);
        }
    }

    public static final void firebeforeNoPageQuery(String tablename,String condition,String sort, String queryfields,String sql, ArrayList<DbParameter> parameter) {
        for (ICefQueryEventListener el : eventLs) {
            el.beforeNoPageQuery(tablename,condition,sort,queryfields,sql,parameter);
        }
    }

    public static final void fireafterNoPageQuery(List<IEntityData> resultset) {
        for (ICefQueryEventListener el : eventLs) {
            el.afterNoPageQuery(resultset);
        }
    }

    public static final void fireafterQuery(List<IEntityData> resultvar) {
        for (ICefQueryEventListener el : eventLs) {
            el.afterQuery(resultvar);
        }
    }

    public static final void fireQueryUnnormalStop(Exception e) {
        for (ICefQueryEventListener el : eventLs) {
            el.queryUnnormalStop(e);
        }
    }


    public static final void fireProcessRecordClassPath(AbstractDeterminationAction determination) {
        for (ICefQueryEventListener el : eventLs) {
            el.processRecordClassPath(determination);
        }
    }

}
