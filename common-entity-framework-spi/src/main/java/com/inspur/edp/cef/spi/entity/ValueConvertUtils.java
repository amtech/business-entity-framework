/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.spi.entity;

public class ValueConvertUtils {

    public static Object toEnum(Class enumClass, Object value){

        if(value == null || "".equals(value))
            return null;
        if(value.getClass().isEnum())
            return value;
        if(value instanceof String){
            return Enum.valueOf(enumClass, value.toString());
        }
        throw new RuntimeException("枚举"+enumClass.getName()+"不支持类型为"+value.getClass().getName()+"值为"+value.toString()+"的值");

    }

    //增加泛型减少生成代码量??
    public static <T> T convertToEnum(Class<T> enumClass, Object value){
       return (T)toEnum(enumClass, value);
    }
}
