/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.spi.jsonser.valueobj;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IValueObjData;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.ValueObjResInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefValueObjResInfo;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefDataSerItem;

public class ValueObjDataSerializerItem extends
    com.inspur.edp.cef.spi.jsonser.valueobj.AbstractValueObjSerializer {

  private CefValueObjResInfo valueObjResInfo;

  public ValueObjDataSerializerItem(CefValueObjResInfo valueObjResInfo)
  {
    this.valueObjResInfo = valueObjResInfo;
  }

  @Override
  public void writeEntityBasicInfo(JsonGenerator jsonGenerator, IValueObjData data,
      SerializerProvider serializerProvider) {
    for(DataTypePropertyInfo propertyInfoInfo :valueObjResInfo.getEntityTypeInfo().getPropertyInfos().values()){
      propertyInfoInfo.write(jsonGenerator, data.getValue(propertyInfoInfo.getPropertyName()), serializerProvider, getCefSerializeContext());
    }
  }

//  @Override
//  public void writeEntityBasicInfo(JsonGenerator jsonGenerator, ICefData data,
//      SerializerProvider serializerProvider) {
//    for(DataTypePropertyInfo propertyInfoInfo :valueObjResInfo.getEntityTypeInfo().getPropertyInfos().values()){
//      propertyInfoInfo.write(jsonGenerator, data, serializerProvider);
//    }
//  }

  @Override
  public boolean readEntityBasicInfo(JsonParser p, DeserializationContext ctxt, IValueObjData data,
      String propertyName) {
    if(!valueObjResInfo.getEntityTypeInfo().containsPropertyInfo(propertyName))
      return false;
    DataTypePropertyInfo info = valueObjResInfo.getEntityTypeInfo().getPropertyInfo(propertyName);
    Object value = info.read(p, ctxt, getCefSerializeContext());
    data.setValue(info.getPropertyName(), value);
    return true;
  }

  @Override
  public Object readModifyPropertyValue(JsonParser reader, DeserializationContext serializer, RefObject<String> propertyName, RefObject<Boolean> hasRead){
    if(!valueObjResInfo.getEntityTypeInfo().containsPropertyInfo(propertyName.argvalue))
      return null;
    DataTypePropertyInfo info = valueObjResInfo.getEntityTypeInfo().getPropertyInfo(propertyName.argvalue);
    Object value = info.readChange(reader, serializer, getCefSerializeContext());
    propertyName.argvalue = info.getPropertyName();
    hasRead.argvalue = true;
    return value;
  }

  @Override
  public boolean writeModifyPropertyJson(JsonGenerator jsonGenerator, String propertyName,
      Object value, SerializerProvider serializerProvider) {
    if(!valueObjResInfo.getEntityTypeInfo().containsPropertyInfo(propertyName))
      return false;
    DataTypePropertyInfo info = valueObjResInfo.getEntityTypeInfo().getPropertyInfo(propertyName);
    info.writeChange(jsonGenerator, value, serializerProvider, getCefSerializeContext());
    return true;
  }

//  @Override
//  public boolean readEntityBasicInfo(JsonParser p, DeserializationContext ctxt, ICefData data,
//      String propertyName) {
//    if(!valueObjResInfo.getEntityTypeInfo().containsPropertyInfo(propertyName))
//      return false;
//    DataTypePropertyInfo info = valueObjResInfo.getEntityTypeInfo().getPropertyInfo(propertyName);
//    Object value = info.read(p, ctxt);
//    data.setValue(info.getPropertyName(), value);
//    return true;
//  }
}
