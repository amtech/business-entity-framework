/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.spi.entity;

import com.inspur.edp.cef.entity.entity.FieldType;

import java.lang.reflect.Type;

/**
 * 属性信息
 */
public abstract class AbstractPropertyInfo {

  public abstract String getName();

  //public abstract bool ReadOnly { get; }

  private boolean privateRequired;
  private boolean enableRtrim;

  public boolean getRequired() {
    return privateRequired;
  }

  private boolean privateRequiredAsConstraint;

  public boolean getRequiredAsConstraint() {
    return privateRequiredAsConstraint;
  }

  /**
   * 是否去空格约束
   */
  private boolean EnableRtrimAsConstraint;

  public boolean getEnableRtrimAsConstraint() {
    return EnableRtrimAsConstraint;
  }

  private boolean privateIsUdt;

  public boolean getIsUdt() {
    return privateIsUdt;
  }

  private String privateUdtConfigId;

  public String getUdtConfigId() {
    return privateUdtConfigId;
  }

  /**
   * 是否默认存null
   */
  private boolean isDefaultNull;

  public boolean getIsDefaultNull() {
    return isDefaultNull;
  }

  public void setIsDefaultNull(boolean value) {
    isDefaultNull = value;
  }

  /**
   * 长度
   */
  private int privateLength;

  public int getLength() {
    return privateLength;
  }

  public void setLength(int value) {
    privateLength = value;
  }

  /**
   * 精度
   */
  private int privatePrecision;

  public int getPrecision() {
    return privatePrecision;
  }

  public void setPrecision(int value) {
    privatePrecision = value;
  }

  /**
   * 类型
   */
  private FieldType privateType = FieldType.forValue(0);

  public FieldType getType() {
    return privateType;
  }

  private boolean privateIsAssociation;

  public boolean getIsAssociation() {
    return privateIsAssociation;
  }

  private AssociationInfo privateAssociationInfo;

  public AssociationInfo getAssociationInfo() {
    return privateAssociationInfo;
  }

  /**
   * 是否枚举
   */
  private boolean privateIsEnum;

  public boolean getIsEnum() {
    return privateIsEnum;
  }

  /**
   * 枚举信息
   */
  private java.util.HashMap<String, Class> privateEnumValueInfos;

  public java.util.HashMap<String, Class> getEnumValueInfos() {
    return privateEnumValueInfos;
  }

  /**
   * 关联信息
   */
  private java.lang.Class privateAssociationInfoType;

  public java.lang.Class getAssociationInfoType() {
    return privateAssociationInfoType;
  }

  /**
   * 属性_国际化项的前缀 e.g. (commonField.I18nResourcePrefix).Name
   */
  private String privateDisplayValueKey;

  public String getDisplayValueKey() {
    return privateDisplayValueKey;
  }

  private PropertyDefaultValue privateDefaultValue;

  public PropertyDefaultValue getDefaultValue() {
    return privateDefaultValue;
  }


  @Override
  public boolean equals(Object obj) {
    return (obj instanceof AbstractPropertyInfo && (((AbstractPropertyInfo) obj).getName()
        == getName()));
  }


  @Override
  public int hashCode() {
		if (getName() == null) {
			return 0;
		}
    return getName().hashCode();
  }

  /**
   * 去空格属性
   *
   * @return
   */
  public boolean getEnableRtrim() {
    return enableRtrim;
  }

  public void setEnableRtrim(boolean enableRtrim) {
    this.enableRtrim = enableRtrim;
  }
}
